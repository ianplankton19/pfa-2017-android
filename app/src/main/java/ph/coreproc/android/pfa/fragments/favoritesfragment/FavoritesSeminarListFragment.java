package ph.coreproc.android.pfa.fragments.favoritesfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.preferences.Preferences;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import database.db.seminar_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.SeminarDetailsActivity;
import ph.coreproc.android.pfa.adapters.favorites.FavoritesSeminarRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.Seminar;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;

/**
 * Created by IanBlanco on 4/6/2017.
 */

public class FavoritesSeminarListFragment extends Fragment implements FavoritesSeminarRecyclerViewAdapter.Callback{


    @Bind(R.id.rvFavorites)
    RecyclerView mRvFavoritesSeminar;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    FavoritesSeminarRecyclerViewAdapter mFavoritesSeminarRecyclerViewAdapter;
    private EventBus mEventBus;
    ArrayList<Seminar> mSeminarLocalArrayList;

    private seminar_local_tblDao mSeminar_local_tblDao;

    private Context mContext;
    private User mUser;

    public static FavoritesSeminarListFragment newInstance() {
        FavoritesSeminarListFragment fragment = new FavoritesSeminarListFragment();

        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        mSeminarLocalArrayList = DataUtil.getSeminarLocals(mSeminar_local_tblDao, mUser.id, mUser.role);
        mFavoritesSeminarRecyclerViewAdapter.update(mSeminarLocalArrayList);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_favorites_conference_display, null);
        ButterKnife.bind(this, view);

        mContext = getActivity();

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        mSeminar_local_tblDao = DataUtil.setUpSeminarDB(getActivity(),
                Prefs.DB_NAME);
        initialize();
        return view;
    }

    private void initialize() {
        mEventBus = EventBus.getDefault();
        mSeminarLocalArrayList = new ArrayList<>();
        mRvFavoritesSeminar.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFavoritesSeminarRecyclerViewAdapter = new FavoritesSeminarRecyclerViewAdapter(getContext(), mSeminarLocalArrayList, FavoritesSeminarListFragment.this);

        mRvFavoritesSeminar.setAdapter(mFavoritesSeminarRecyclerViewAdapter);

        getData();
    }

    private void getData() {

        mSeminarLocalArrayList = DataUtil.getSeminarLocals(mSeminar_local_tblDao, mUser.id, mUser.role);
        mFavoritesSeminarRecyclerViewAdapter.add(mSeminarLocalArrayList);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void intentToDetails(int id) {
        getActivity().startActivity(SeminarDetailsActivity.newIntent(getActivity(), "" + id, true));
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }
}
