package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import butterknife.Bind;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.utils.TextViewEx;

/**
 * Created by IanBlanco on 5/18/2017.
 */

public class AboutFapActivity extends BaseActivity {

    @Bind(R.id.tvOne)
    TextViewEx mTvOne;
    @Bind(R.id.tvTwo)
    TextViewEx mTvTwo;

    public static Intent newIntent(Context context) {

        Intent intent = new Intent(context, AboutFapActivity.class);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_about_fap;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialize();
    }

    private void initialize() {
        ((TextViewEx) mTvOne).setText(mTvOne.getText().toString(), true);
        ((TextViewEx) mTvTwo).setText(mTvTwo.getText().toString(), true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
