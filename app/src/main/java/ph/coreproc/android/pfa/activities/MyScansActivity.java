package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.preferences.Preferences;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import database.db.exhibitor_scan_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.favorites.FavoritesExhibitorRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.NetworkHelper;
import ph.coreproc.android.pfa.utils.Prefs;

/**
 * Created by IanBlanco on 5/8/2017.
 */

public class MyScansActivity extends BaseActivity implements FavoritesExhibitorRecyclerViewAdapter.Callback {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.exhibitorsListRecyclerView)
    RecyclerView mExhibitorsListRecyclerView;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;


    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    @Bind(R.id.activity_seminar_list)
    LinearLayout mActivitySeminarList;


    private User mUser;

    public static Intent newIntent(Context context) {

        Intent intent = new Intent(context, MyScansActivity.class);
        return intent;
    }


    private ArrayList<Exhibitor> mExhibitorLocals;
    private FavoritesExhibitorRecyclerViewAdapter mFavoritesExhibitorRecyclerViewAdapter;
    private exhibitor_scan_local_tblDao mExhibitor_scan_local_tblDao;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_scanned_exhibitors;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        mExhibitor_scan_local_tblDao = DataUtil.setUpExhibitorScannedLocal(mContext, Prefs.DB_NAME);
        initialize();
    }

    private void initialize() {
        mExhibitorLocals = new ArrayList<>(DataUtil.getExhibitorsScansLocals(mExhibitor_scan_local_tblDao, mUser.id, mUser.role));
        mFavoritesExhibitorRecyclerViewAdapter = new FavoritesExhibitorRecyclerViewAdapter(mContext, mExhibitorLocals, MyScansActivity.this);
        PFABase.setDefaultRecyclerView(mContext, mExhibitorsListRecyclerView, mFavoritesExhibitorRecyclerViewAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void intentToDetails(int id) {
        NetworkHelper networkHelper = new NetworkHelper(mContext);
        startActivity(ExhibitorDetailsActivity.newIntent(mContext, id, !networkHelper.isNetworkAvailable()));
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }
}
