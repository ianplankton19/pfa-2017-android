package ph.coreproc.android.pfa.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.ConferenceDetailsActivity;
import ph.coreproc.android.pfa.models.offlinemodels.OfflineExtension;

/**
 * Created by willm on 3/28/2017.
 */

public class Topics extends OfflineExtension implements Serializable {


    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("day")
    @Expose
    public String day;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("location")
    @Expose
    public String location;

    @SerializedName("starts_at")
    @Expose
    public String startsAt;

    @SerializedName("rating")
    @Expose
    public int rating;

    @SerializedName("ends_at")
    @Expose
    public String endsAt;

    @SerializedName("duration")
    @Expose
    public String duration;

    @SerializedName("subtopics")
    @Expose
    public SubTopic subTopic;

    @SerializedName("speakers")
    @Expose
    public SpeakersData speakersData;


    public class SpeakersData implements Serializable {

        @SerializedName("data")
        @Expose
        public ArrayList<Speaker> speakers;

    }


    public View getViewDisplay(final Context context, View.OnClickListener onClickListener, ArrayList<SubTopic.SubTopicField> subTopicItems, String day) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_topic_item, null);
        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        LinearLayout llSubTopicContainer = (LinearLayout) view.findViewById(R.id.llSubTopicContainer);
        view.setOnClickListener(onClickListener);
        tvName.setText(name);
        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(ConferenceDetailsActivity.newIntent(context, id + "" , false, true));
            }
        });


        for (int i = 0; i < subTopicItems.size(); i ++) {

            llSubTopicContainer.addView(subTopicItems.get(i).getView(context, onClickListener));

        }
        return view;
    }

    public View getView(Context context, View.OnClickListener onClickListener, boolean isLast) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_topic_speaker, null);
        RelativeLayout rlContainer = (RelativeLayout) view.findViewById(R.id.rlContainer);
        TextView tvConferenceNameTextView = (TextView) view.findViewById(R.id.conferenceNameTextView);
        TextView tvConferenceDescriptionTextView = (TextView) view.findViewById(R.id.conferenceDescriptionTextView);
        ImageView ivProceed = (ImageView) view.findViewById(R.id.proceed);
        if (isLast) {
            rlContainer.setBackground(context.getDrawable(R.drawable.footer_background));
        }
        rlContainer.setOnClickListener(onClickListener);

        tvConferenceNameTextView.setText(name);
        tvConferenceDescriptionTextView.setText(description);

        return view;
    }

}