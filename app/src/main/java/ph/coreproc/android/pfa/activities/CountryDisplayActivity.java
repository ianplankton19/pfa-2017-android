package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.CountriesAdapter;
import ph.coreproc.android.pfa.models.Country;

/**
 * Created by IanBlanco on 4/18/2017.
 */

public class CountryDisplayActivity extends BaseActivity implements CountriesAdapter.Callback{


    @Bind(R.id.rvRegions)
    RecyclerView mRvRegions;


    private ArrayList<Country> mCountryArrayList;
    private CountriesAdapter mCountriesAdapter;


    public static Intent newIntent(Context context){
        Intent intent = new Intent(context, CountryDisplayActivity.class);
        return  intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_region_dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        initialize();
    }

    private void initialize() {
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        mRvRegions.setLayoutManager(linearLayoutManager);
//
        mCountryArrayList = new ArrayList<>();
//
        mCountryArrayList = PFABase.getCountries(mContext);
        mCountriesAdapter = new CountriesAdapter(mContext, mCountryArrayList, CountryDisplayActivity.this);
        PFABase.setDefaultRecyclerView(mContext, mRvRegions, mCountriesAdapter);
//        mRegionAdapter = new RegionAdapter(mRegions, mContext, RegionDisplayActivity.this);
//        mRvRegions.setAdapter(mRegionAdapter);

    }

    @Override
    public void getPosition(Country country) {
        EventBus.getDefault().postSticky(country);
        finish();
    }
}
