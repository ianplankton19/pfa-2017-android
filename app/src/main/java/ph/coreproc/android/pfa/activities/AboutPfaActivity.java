package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.Bind;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.utils.TextViewEx;

/**
 * Created by IanBlanco on 5/18/2017.
 */

public class AboutPfaActivity extends BaseActivity {

    @Bind(R.id.tvOne)
    TextView mTvOne;

    @Bind(R.id.tvTwo)
    TextView mTvTwo;

    @Bind(R.id.tvThree)
    TextView mTvThree;

    @Bind(R.id.tvFour)
    TextView mTvFour;

    @Bind(R.id.tvFive)
    TextView mTvFive;

    @Bind(R.id.tvSix)
    TextView mTvSix;

    public static Intent newIntent(Context context) {

        Intent intent = new Intent(context, AboutPfaActivity.class);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_about_pfa;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();
    }

    private void initialize() {

        ((TextViewEx) mTvOne).setText(mTvOne.getText().toString(), true);
        ((TextViewEx) mTvTwo).setText(mTvTwo.getText().toString(), true);
        ((TextViewEx) mTvThree).setText(mTvThree.getText().toString(), true);
        ((TextViewEx) mTvFour).setText(mTvFour.getText().toString(), true);
        ((TextViewEx) mTvFive).setText(mTvFive.getText().toString(), true);
        ((TextViewEx) mTvSix).setText(mTvSix.getText().toString(), true);
//        LayoutParams params = (LayoutParams) mTvOne.getLayoutParams();
//        LayoutParams params2 = (LayoutParams) mTvTwo.getLayoutParams();
//        mTvOne.setHeight(params.WRAP_CONTENT);
//        mTvTwo.setHeight(params2.WRAP_CONTENT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
