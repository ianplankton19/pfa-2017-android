package ph.coreproc.android.pfa.adapters.eventschedule;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Event;

/**
 * Created by IanBlanco on 4/20/2017.
 */

public class EventScheduleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    private ArrayList<Event> mEventArrayList;


    public EventScheduleAdapter(Context context, ArrayList<Event> eventArrayList) {
        mContext = context;
        mEventArrayList = eventArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_event_schedule_item, parent, false);
        return new EventsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EventsViewHolder eventsViewHolder = (EventsViewHolder) holder;
        eventsViewHolder.mConferenceNameTextView.setText(mEventArrayList.get(position).topic.toUpperCase());
        eventsViewHolder.mConferenceTimeTextView.setText(mEventArrayList.get(position).date);
        eventsViewHolder.mTvDate.setText(Html.fromHtml("<b>Time: </b>" + mEventArrayList.get(position).time));
        eventsViewHolder.mTvLocation.setText(Html.fromHtml("<b>Location: </b>" + mEventArrayList.get(position).location));
    }

    @Override
    public int getItemCount() {
        return mEventArrayList.size();
    }

    static class EventsViewHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.conferenceNameTextView)
        TextView mConferenceNameTextView;

        @Bind(R.id.conferenceTimeTextView)
        TextView mConferenceTimeTextView;

        @Bind(R.id.tvDate)
        TextView mTvDate;

        @Bind(R.id.tvLocation)
        TextView mTvLocation;


        public EventsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
