package ph.coreproc.android.pfa;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import database.db.DaoMaster;
import database.db.DaoSession;
import database.db.conference_local_tbl;
import database.db.conference_local_tblDao;
import database.db.exhibitor_local_tbl;
import database.db.exhibitor_local_tblDao;
import database.db.exhibitor_scan_local_tbl;
import database.db.exhibitor_scan_local_tblDao;
import database.db.notes_local_tbl;
import database.db.notes_local_tblDao;
import database.db.seminar_local_tbl;
import database.db.seminar_local_tblDao;
import database.db.speaker_local_tbl;
import database.db.speaker_local_tblDao;
import database.db.user_scan_local_tbl;
import database.db.user_scan_local_tblDao;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.models.Seminar;
import ph.coreproc.android.pfa.models.Speaker;
import ph.coreproc.android.pfa.models.Topics;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.offlinemodels.Notes;
import ph.coreproc.android.pfa.utils.ModelUtil;

/**
 * Created by IanBlanco on 4/12/2017.
 */

public class DataUtil {

    public static DaoSession getDaoSession(Context context, String dbName) {
        DaoMaster.DevOpenHelper masterHelper = new DaoMaster.DevOpenHelper(context, dbName, null);

        // close database if opened previously
        // hoping to fix the issue of not opening database
        masterHelper.close();

        SQLiteDatabase db = masterHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession masterSession = daoMaster.newSession();

        return masterSession;
    }

    /**
     * For Conference Table
     *
     * @param items
     * @param singleItem
     * @return
     */

    public static Integer checkIfExist(ArrayList<Topics> items, Topics singleItem) {
        for (int i = 0; i < items.size(); i++) {
            if (singleItem.id == items.get(i).id && singleItem.name.equals(items.get(i).name)) {
                return i;
            }
        }
        return null;
    }

    public static conference_local_tblDao setUpConferenceLocal(Context context, String dbName) {

        conference_local_tblDao mConference_local_tblDao = setUpConferenceDB(context, dbName);

        return mConference_local_tblDao;
    }


    public static conference_local_tblDao setUpConferenceDB(Context context, String dbName) {

        return getDaoSession(context, dbName).getConference_local_tblDao();
    }


    public static ArrayList<Topics> getTopicLocals(conference_local_tblDao mConference_local_tblDao, int id, String userRole) {
        ArrayList<Topics> items = new ArrayList<>();
        List<conference_local_tbl> conference_local_tbls = mConference_local_tblDao
                .queryBuilder().orderDesc(conference_local_tblDao.Properties.Id).build().list();
        if (conference_local_tbls.size() > 0) {
            for (conference_local_tbl conference_local_tbl : conference_local_tbls) {
                String response = conference_local_tbl.getSave_string();
                Topics topics = ModelUtil.fromJson(Topics.class, response);
                if (topics.userId == id && topics.userRole.equals(userRole)) {
                    items.add(topics);
                }
            }
        }

        return items;
    }

    public static ArrayList<Long> getTopicIds(conference_local_tblDao mConference_local_tblDao) {
        ArrayList<Long> topicIds = new ArrayList<>();
        List<conference_local_tbl> conference_local_tbls = mConference_local_tblDao.queryBuilder().
                orderDesc(conference_local_tblDao.Properties.Id).build().list();
        if (conference_local_tbls.size() > 0) {
            for (conference_local_tbl conference_local_tbl : conference_local_tbls) {
                Long id = conference_local_tbl.getId();
                topicIds.add(id);
            }
        }

        return topicIds;
    }


    /**
     * For Seminar Table
     *
     * @param items
     * @param sigleItem
     * @return
     */

    public static Integer checkSeminarIfExist(ArrayList<Seminar> items, Seminar sigleItem) {
        for (int i = 0; i < items.size(); i++) {
            if (sigleItem.id == items.get(i).id && sigleItem.name.equals(items.get(i).name)) {
                return i;
            }
        }
        return null;
    }

    public static seminar_local_tblDao setUpSeminarLocal(Context context, String dbName) {

        seminar_local_tblDao mSeminar_local_tblDao = setUpSeminarDB(context, dbName);

        return mSeminar_local_tblDao;
    }

    public static seminar_local_tblDao setUpSeminarDB(Context context, String dbName) {

        return getDaoSession(context, dbName).getSeminar_local_tblDao();
    }


    public static ArrayList<Seminar> getSeminarLocals(seminar_local_tblDao mSeminar_local_tblDao, int id, String role) {
        ArrayList<Seminar> items = new ArrayList<>();
        List<seminar_local_tbl> seminar_local_tbls = mSeminar_local_tblDao.queryBuilder().
                orderDesc(seminar_local_tblDao.Properties.Id).build().list();
        if (seminar_local_tbls.size() > 0) {
            for (seminar_local_tbl seminar_local_tbl : seminar_local_tbls) {
                String response = seminar_local_tbl.getSave_string();
                Seminar seminarLocal = ModelUtil.fromJson(Seminar.class, response);
                if (seminarLocal.userId == id && seminarLocal.userRole.equals(role)) {
                    items.add(seminarLocal);
                }
            }
        }
        return items;
    }


    public static ArrayList<Long> getSeminarId(seminar_local_tblDao mSeminar_local_tblDao) {
        ArrayList<Long> seminarIds = new ArrayList<>();
        List<seminar_local_tbl> seminar_local_tbls = mSeminar_local_tblDao.queryBuilder().
                orderDesc(seminar_local_tblDao.Properties.Id).build().list();
        if (seminar_local_tbls.size() > 0) {
            for (seminar_local_tbl seminar_local_tbl : seminar_local_tbls) {
                Long id = seminar_local_tbl.getId();
                seminarIds.add(id);
            }
        }

        return seminarIds;
    }


    /**
     * For Speaker Table
     *
     * @param items
     * @param sigleItem
     * @return
     */

    public static Integer checkSpeakerIfExist(ArrayList<Speaker> items, Speaker sigleItem) {

        for (int i = 0; i < items.size(); i++) {
            if (sigleItem.id == items.get(i).id && sigleItem.speakerName.equals(items.get(i).speakerName)) {
                return i;
            }
        }

        return null;
    }

    public static speaker_local_tblDao setUpSpeakerLocal(Context context, String dbName) {

        speaker_local_tblDao mSpeaker_local_tblDao = setUpSpeakerDB(context, dbName);

        return mSpeaker_local_tblDao;
    }

    public static speaker_local_tblDao setUpSpeakerDB(Context context, String dbName) {

        return getDaoSession(context, dbName).getSpeaker_local_tblDao();
    }


    public static ArrayList<Speaker> getSpeakerLocal(speaker_local_tblDao mSpeaker_local_tblDao, int id, String role) {
        ArrayList<Speaker> items = new ArrayList<>();
        List<speaker_local_tbl> speaker_local_tbls = mSpeaker_local_tblDao.queryBuilder().
                orderDesc(speaker_local_tblDao.Properties.Id).build().list();
        if (speaker_local_tbls.size() > 0) {
            for (speaker_local_tbl speaker_local_tbl : speaker_local_tbls) {
                String response = speaker_local_tbl.getSave_string();
                Speaker speakerLocal = ModelUtil.fromJson(Speaker.class, response);
                if (speakerLocal.userId == id && speakerLocal.userRole.equals(role)) {
                    items.add(speakerLocal);
                }
            }
        }
        return items;
    }

    public static ArrayList<Long> getSpeakerId(speaker_local_tblDao mSpeaker_local_tblDao) {
        ArrayList<Long> speakerIds = new ArrayList<>();
        List<speaker_local_tbl> speaker_local_tbls = mSpeaker_local_tblDao.queryBuilder().
                orderDesc(speaker_local_tblDao.Properties.Id).build().list();
        if (speaker_local_tbls.size() > 0) {
            for (speaker_local_tbl speaker_local_tbl : speaker_local_tbls) {
                Long id = speaker_local_tbl.getId();
                speakerIds.add(id);
            }
        }

        return speakerIds;
    }


    /**
     * For Exhibitor Table
     *
     * @param items
     * @param singleItem
     * @return
     */

    public static Integer checkExhibitorIfExist(ArrayList<Exhibitor> items, Exhibitor singleItem) {
        for (int i = 0; i < items.size(); i++) {
            if (singleItem.id == items.get(i).id && singleItem.name.equals(items.get(i).name)) {
                return i;
            }
        }
        return null;
    }

    public static exhibitor_local_tblDao setUpExhibitorLocal(Context context, String dbName) {

        exhibitor_local_tblDao mExhibitor_local_tblDao = setUpExhibitorDB(context, dbName);

        return mExhibitor_local_tblDao;
    }

    public static exhibitor_local_tblDao setUpExhibitorDB(Context context, String dbName) {
        return getDaoSession(context, dbName).getExhibitor_local_tblDao();
    }


    public static ArrayList<Exhibitor> getExhibitorLocal(exhibitor_local_tblDao mExhibitor_local_tblDao, int id, String role) {
        ArrayList<Exhibitor> items = new ArrayList<>();
        List<exhibitor_local_tbl> exhibitor_local_tbls = mExhibitor_local_tblDao.queryBuilder().
                orderDesc(exhibitor_local_tblDao.Properties.Id).build().list();
        if (exhibitor_local_tbls.size() > 0) {
            for (exhibitor_local_tbl exhibitor_local_tbl : exhibitor_local_tbls) {
                String response = exhibitor_local_tbl.getSave_string();
                Exhibitor exhibitorLocal = ModelUtil.fromJson(Exhibitor.class, response);
                if (exhibitorLocal.userId == id && exhibitorLocal.userRole.equals(role)) {
                    items.add(exhibitorLocal);
                }
            }
        }
        return items;
    }

    public static ArrayList<Long> getExhibitorId(exhibitor_local_tblDao mExhibitor_local_tblDao) {
        ArrayList<Long> exhibitorIds = new ArrayList<>();
        List<exhibitor_local_tbl> exhibitor_local_tbls = mExhibitor_local_tblDao.queryBuilder().
                orderDesc(exhibitor_local_tblDao.Properties.Id).build().list();
        if (exhibitor_local_tbls.size() > 0) {
            for (exhibitor_local_tbl exhibitor_local_tbl : exhibitor_local_tbls) {
                Long id = exhibitor_local_tbl.getId();
                exhibitorIds.add(id);
            }
        }

        return exhibitorIds;
    }


    /**
     * For User Exhibitor
     */
    public static user_scan_local_tblDao setUpUserScannedLocal(Context context, String dbName) {

        user_scan_local_tblDao mUser_scan_local_tblDao = setUpUserScanneDB(context, dbName);

        return mUser_scan_local_tblDao;
    }

    public static user_scan_local_tblDao setUpUserScanneDB(Context context, String dbName) {

        return getDaoSession(context, dbName).getUser_scan_local_tblDao();

    }

    public static Integer checkScanUserIfExist(ArrayList<User> items, User item) {

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).id == item.id && items.get(i).userId == item.userId
                    && items.get(i).userRole.equals(item.userRole)) {
                return i;
            }
        }
        return null;
    }


    public static ArrayList<User> getUserScanned(user_scan_local_tblDao mUser_scan_local_tblDao, int id, String role) {
        ArrayList<User> userArrayList = new ArrayList<>();
        List<user_scan_local_tbl> userScanLocalTbls = mUser_scan_local_tblDao.queryBuilder().
                orderDesc(user_scan_local_tblDao.Properties.Id).list();
        if (userScanLocalTbls.size() > 0) {
            for (user_scan_local_tbl userScanLocalTbl : userScanLocalTbls) {
                String response = userScanLocalTbl.getSave_string();
                Log.i("tag", "scans : " + response);
                User user = ModelUtil.fromJson(User.class, response);
                if (user.userId == id && user.userRole.equals(role)) {
                    userArrayList.add(user);
                }
            }
        }
        return userArrayList;
    }



    /**
     * For Scanned Exhibitors
     */

    public static exhibitor_scan_local_tblDao setUpExhibitorScannedLocal(Context context, String dbName) {

        exhibitor_scan_local_tblDao mExhibitor_scan_local_tblDao = setUpExhibitorScannedDB(context, dbName);

        return mExhibitor_scan_local_tblDao;
    }

    public static exhibitor_scan_local_tblDao setUpExhibitorScannedDB(Context context, String dbName) {
        return getDaoSession(context, dbName).getExhibitor_scan_local_tblDao();
    }


    public static Exhibitor saveScannedExhibitorLocal(Exhibitor exhibitorLocal, exhibitor_scan_local_tblDao mExhibitor_scan_local_tblDao) {

        String response = ModelUtil.toJsonString(exhibitorLocal);

        ArrayList<Long> ids = new ArrayList<>();
        ArrayList<Exhibitor> items = new ArrayList<>();

        List<exhibitor_scan_local_tbl> exhibitor_scan_local_tbls = mExhibitor_scan_local_tblDao.queryBuilder().
                orderDesc(exhibitor_scan_local_tblDao.Properties.Id).build().list();


        if (exhibitor_scan_local_tbls.size() > 0) {

            for (exhibitor_scan_local_tbl exhibitorScanLocalTbl : exhibitor_scan_local_tbls) {
                String dataResponse = exhibitorScanLocalTbl.getSave_string();
                long id = exhibitorScanLocalTbl.getId();
                Exhibitor exhibitorLocal1 = ModelUtil.fromJson(Exhibitor.class, dataResponse);
                items.add(exhibitorLocal1);
                ids.add(id);
            }

            if (checkScanIfExist(items, exhibitorLocal) != null) {
//        Update Function
                String sameResponse = ModelUtil.toJsonString(items.get(checkScanIfExist(items, exhibitorLocal)));

                if (response.length() > sameResponse.length()) {
                    long updateId = ids.get(checkScanIfExist(items, exhibitorLocal));
                    exhibitor_scan_local_tbl exhibitorScanLocalTbl = mExhibitor_scan_local_tblDao.load(updateId);
                    mExhibitor_scan_local_tblDao.update(exhibitorScanLocalTbl);
                    exhibitorLocal = ModelUtil.fromJson(Exhibitor.class, response);
                } else {
                    exhibitorLocal = ModelUtil.fromJson(Exhibitor.class, sameResponse);
                }
//        Nothing to do

            } else {

                exhibitor_scan_local_tbl exhibitorScanLocalTbl = new exhibitor_scan_local_tbl(response);
                mExhibitor_scan_local_tblDao.insert(exhibitorScanLocalTbl);
                exhibitorLocal = ModelUtil.fromJson(Exhibitor.class, response);
            }
        } else {

//        if no one exists
            exhibitor_scan_local_tbl exhibitorScanLocalTbl = new exhibitor_scan_local_tbl(response);
            mExhibitor_scan_local_tblDao.insert(exhibitorScanLocalTbl);
            exhibitorLocal = ModelUtil.fromJson(Exhibitor.class, response);
        }
        return exhibitorLocal;
    }

    public static Integer checkScanIfExist(ArrayList<Exhibitor> items, Exhibitor item) {

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).id == item.id && items.get(i).userId == item.userId) {
                return i;
            }
        }
        return null;
    }

    public static ArrayList<Exhibitor> getExhibitorsScansLocals(exhibitor_scan_local_tblDao mExhibitor_scan_local_tblDao, int id, String role) {
        ArrayList<Exhibitor> exhibitorLocals = new ArrayList<>();
        List<exhibitor_scan_local_tbl> exhibitorScanLocalTbls = mExhibitor_scan_local_tblDao.queryBuilder().
                orderDesc(exhibitor_scan_local_tblDao.Properties.Id).list();
        if (exhibitorScanLocalTbls.size() > 0) {
            for (exhibitor_scan_local_tbl exhibitorScanLocalTbl : exhibitorScanLocalTbls) {
                String response = exhibitorScanLocalTbl.getSave_string();
                Log.i("tag", "scans : " + response);
                Exhibitor exhibitorLocal = ModelUtil.fromJson(Exhibitor.class, response);
                if (exhibitorLocal.userId == id && exhibitorLocal.userRole.equals(role)) {
                    exhibitorLocals.add(exhibitorLocal);
                }
            }
        }
        return exhibitorLocals;
    }


    /**
     * For Notes Table
     */

    public static boolean checkIfNoteExist(ArrayList<Notes> items, Notes singleItem) {
        boolean toReturn = false;
        for (Notes notes : items) {
            if (singleItem.id == notes.id && singleItem.note.equals(notes.note) && singleItem.noteName.equals(notes.noteName) &&
                    notes.userId == singleItem.userId && notes.userRole.equals(singleItem.userRole)) {
                toReturn = true;
            }
        }

        return toReturn;
    }


    public static notes_local_tblDao setUpNotesLocal(Context context, String dbName) {

        notes_local_tblDao mNotes_local_tblDao = setUpNotesDB(context, dbName);

        return mNotes_local_tblDao;
    }

    public static notes_local_tblDao setUpNotesDB(Context context, String dbName) {

        return getDaoSession(context, dbName).getNotes_local_tblDao();
    }

    public static ArrayList<Notes> getNotesLocal(notes_local_tblDao mNotes_local_tblDao, int id, String role) {
        ArrayList<Notes> items = new ArrayList<>();
        List<notes_local_tbl> notes_local_tbls = mNotes_local_tblDao.queryBuilder().
                orderDesc(notes_local_tblDao.Properties.Id).build().list();
        if (notes_local_tbls.size() > 0) {
            for (notes_local_tbl notes_local_tbl : notes_local_tbls) {
                String response = notes_local_tbl.getSave_string();
                Notes notes = ModelUtil.fromJson(Notes.class, response);
                if (notes.userId == id && notes.userRole.equals(role)) {
                    items.add(notes);
                }
            }
        }


        return items;
    }

    public static ArrayList<Long> getNoteIds(notes_local_tblDao mNotes_local_tblDao) {
        ArrayList<Long> ids = new ArrayList<>();
        List<notes_local_tbl> notes_local_tbls = mNotes_local_tblDao.queryBuilder().
                orderDesc(notes_local_tblDao.Properties.Id).build().list();
        if (notes_local_tbls.size() > 0) {
            for (notes_local_tbl notes_local_tbl : notes_local_tbls) {
                long response = notes_local_tbl.getId();
//                Notes notes = ModelUtil.fromJson(Notes.class, response);
                ids.add(response);
            }


        }
        return ids;
    }

}
