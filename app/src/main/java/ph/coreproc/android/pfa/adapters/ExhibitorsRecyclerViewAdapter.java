package ph.coreproc.android.pfa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Exhibitor;

/**
 * Created by willm on 3/27/2017.
 */

public class ExhibitorsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Exhibitor> items;
    ArrayList<Exhibitor> itemsCopy;
    Context mContext;

    public interface Callback extends DisplayNoDataInterface {
        void intentToDetails(int id);
    }

    Callback mCallback;

    public ExhibitorsRecyclerViewAdapter(Context context, ArrayList<Exhibitor> items, Callback callback) {
        mContext = context;
        this.items = items;
        this.itemsCopy = items;
        this.mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_exhibitor_item, parent, false);
        ExhibitorViewHolder exhibitorViewHolder = new ExhibitorViewHolder(view);
        return exhibitorViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ExhibitorViewHolder exhibitorViewHolder = (ExhibitorViewHolder) holder;
        final Exhibitor exhibitor = items.get(position);

        exhibitorViewHolder.exhibitorNameTextView.setText(exhibitor.franchiseName);
        exhibitorViewHolder.boothNumberTextView.setText("Booth " + exhibitor.boothNumber);
        exhibitorViewHolder.mRlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.intentToDetails(exhibitor.id);
            }
        });
        if (exhibitor.avatar != null) {
            Glide.with(mContext)
                    .load(exhibitor.avatar)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_delete_white)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Log.d("error loading image", "error" + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            exhibitorViewHolder.exhibitorImageView.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(exhibitorViewHolder.exhibitorImageView);
        }

    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults onReturn = new FilterResults();
                final ArrayList<Exhibitor> results = new ArrayList<>();

                items = new ArrayList<>(itemsCopy);

                if (constraint != null) {
                    if (items.size() > 0) {
                        for (final Exhibitor exhibitor : items) {
                            if (exhibitor.franchiseName.toLowerCase().contains(constraint.toString().toLowerCase()) ||
                                    exhibitor.name.toLowerCase().contains(constraint.toString().toLowerCase()))
                                results.add(exhibitor);
                            }
                        }
                        onReturn.values = results;
                    }

                    return onReturn;
                }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                items = (ArrayList<Exhibitor>) results.values;

                displayNoData(items);
//                arrangeArray();
                notifyDataSetChanged();
            }
        };
    }

    public void add(ArrayList<Exhibitor> items) {
        int previousDataSize = this.items.size();
        this.items.addAll(items);
        displayNoData(this.items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }

    //    for Search and sorting
    public void changeData(ArrayList<Exhibitor> items) {
        this.items = new ArrayList<>();
        this.items = items;
        displayNoData(this.items);
        notifyDataSetChanged();
    }

    private void displayNoData(ArrayList<Exhibitor> items) {

        if (items.size() > 0 == false) {
            mCallback.displayNoDataText();
            return;
        }
        mCallback.removeNoDataText();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ExhibitorViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.exhibitorImageView)
        ImageView exhibitorImageView;

        @Bind(R.id.exhibitorNameTextView)
        TextView exhibitorNameTextView;

        @Bind(R.id.boothNumberTextView)
        TextView boothNumberTextView;

        @Bind(R.id.rlContainer)
        RelativeLayout mRlContainer;

        public ExhibitorViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

