package ph.coreproc.android.pfa.models.offlinemodels;

        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

        import java.io.Serializable;

/**
 * Created by IanBlanco on 6/14/2017.
 */

public class OfflineExtension implements Serializable {

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("user_role")
    @Expose
    public String userRole;
}
