package ph.coreproc.android.pfa.adapters.favorites;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Seminar;

/**
 * Created by IanBlanco on 4/10/2017.
 */

public class FavoritesSeminarRecyclerViewAdapter extends RecyclerView.Adapter<FavoritesSeminarRecyclerViewAdapter.ViewHolder> {

    public interface Callback extends DisplayNoDataInterface{
        void intentToDetails(int id);
    }
    private Callback mCallback;
    private Context mContext;
    private ArrayList<Seminar> mSeminarLocalArrayList;

    public FavoritesSeminarRecyclerViewAdapter(Context context, ArrayList<Seminar> seminarLocalArrayList, Callback callback) {
        mContext = context;
        mSeminarLocalArrayList = seminarLocalArrayList;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_favorites, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Seminar seminarLocal = mSeminarLocalArrayList.get(position);
        holder.mTvName.setText(seminarLocal.name);
        holder.mTvBooth.setText(seminarLocal.location);
        holder.mLlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.intentToDetails(seminarLocal.id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mSeminarLocalArrayList.size();
    }

    public void add(ArrayList<Seminar> items){
        mSeminarLocalArrayList.addAll(items);
        displayNoData(mSeminarLocalArrayList);
        notifyDataSetChanged();
    }

    public void update(ArrayList<Seminar> items) {
        mSeminarLocalArrayList = items;
        displayNoData(mSeminarLocalArrayList);
        notifyDataSetChanged();
    }

    private void displayNoData(ArrayList<Seminar> items) {

        if (items.size() > 0 == false) {
            mCallback.displayNoDataText();
            return;
        }
        mCallback.removeNoDataText();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvName)
        TextView mTvName;

        @Bind(R.id.tvBooth)
        TextView mTvBooth;

        @Bind(R.id.llContainer)
        LinearLayout mLlContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
