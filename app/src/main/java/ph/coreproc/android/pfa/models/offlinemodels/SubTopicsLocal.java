package ph.coreproc.android.pfa.models.offlinemodels;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by IanBlanco on 4/5/2017.
 */

public class SubTopicsLocal implements Serializable {

    @SerializedName("data")
    @Expose
    public ArrayList<SubTopicFieldLocal> subTopics;

    private JsonObject data;

    public void setData(JsonObject data) {
        this.data = data;
    }


    public SubTopicsLocal(JsonArray jsonArray) {
        data = new JsonObject();
        data.add("data", jsonArray);
    }

//    public SubTopicsLocal(SubTopic subTopic){
//        this.subTopics = subTopic;
//    }

    public JsonObject getData() {
        return data;
    }


//    @SerializedName("search_item")
//    @Expose
//    public Speaker Speaker;


}
