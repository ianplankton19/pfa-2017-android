package ph.coreproc.android.pfa.fragments.conference;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.ConferenceDetailsActivity;
import ph.coreproc.android.pfa.adapters.conference.ConferenceListRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.Session;
import ph.coreproc.android.pfa.models.Topics;
import ph.coreproc.android.pfa.rest.ConferenceInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by willm on 3/24/2017.
 */

public class ConferenceFirstDayFragment extends Fragment implements ConferenceListRecyclerViewAdapter.Callback {

    private Context mContext;

    @Bind(R.id.conferenceListRecyclerView)
    RecyclerView conferenceListRecyclerView;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    ArrayList<Session> mSessionArrayList;
    private ConferenceListRecyclerViewAdapter mConferenceListRecyclerViewAdapter;

    public static ConferenceFirstDayFragment newInstance() {
        ConferenceFirstDayFragment conferenceFirstDayFragment = new ConferenceFirstDayFragment();

        return conferenceFirstDayFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_conference_list_fragment, container, false);
//        Bundle bundle = getArguments();
        ButterKnife.bind(this, view);
        mContext = getActivity();
        initialize();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser) {
                initialize();
            }

        }
    }

    private void initialize() {

        mSessionArrayList = new ArrayList<>();
        Log.d("iteems", "dumaan");

        mConferenceListRecyclerViewAdapter = new ConferenceListRecyclerViewAdapter(mContext, mSessionArrayList, ConferenceFirstDayFragment.this, "1");
        PFABase.setDefaultRecyclerView(mContext, conferenceListRecyclerView, mConferenceListRecyclerViewAdapter);

       getData();

    }

    private void getData() {

        final Dialog dialog = PFABase.loadDefaultProgressDialogCancellable(mContext, getActivity());
        dialog.show();

        ConferenceInterface conferenceInterface = KitchenRestClient.create(mContext, ConferenceInterface.class, true);
        Call<DataWrapper<ArrayList<Session>>> topicResponseCall = conferenceInterface.getConference(getString(R.string.get_conference_url));
        topicResponseCall.enqueue(new Callback<DataWrapper<ArrayList<Session>>>() {
            @Override
            public void onResponse(Call<DataWrapper<ArrayList<Session>>> call, Response<DataWrapper<ArrayList<Session>>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                mConferenceListRecyclerViewAdapter.add(response.body().getData());

            }

            @Override
            public void onFailure(Call<DataWrapper<ArrayList<Session>>> call, Throwable t) {
                dialog.dismiss();
            }
        });


    }


    @Override
    public void displayDetails(Topics topics) {
        startActivity(ConferenceDetailsActivity.newIntent(mContext, "" + topics.id, false, true));

    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }
}
