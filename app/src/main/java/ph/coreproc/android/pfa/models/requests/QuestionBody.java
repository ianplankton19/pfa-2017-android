package ph.coreproc.android.pfa.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 5/5/2017.
 */

public class QuestionBody implements Serializable {

    public QuestionBody(String question) {
        this.question = question;
    }

    @SerializedName("question")
    @Expose
    public String question;
}
