package ph.coreproc.android.pfa.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.KitchenRestClient;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import database.db.user_scan_local_tblDao;
import ph.coreproc.android.pfa.BuildConfig;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.AdsAdapter;
import ph.coreproc.android.pfa.adapters.RVDashBoardAdapter;
import ph.coreproc.android.pfa.models.Ads;
import ph.coreproc.android.pfa.models.DashBoardCategory;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.Version;
import ph.coreproc.android.pfa.rest.AdsInterface;
import ph.coreproc.android.pfa.rest.UserInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 3/27/2017.
 */

public class DashBoardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, RVDashBoardAdapter.Callback {




    public static Intent newIntent(Context context) {

        Intent intent = new Intent(context, DashBoardActivity.class);
        return intent;
    }


    @Bind(R.id.rvDashboardList)
    RecyclerView mRvDashboardList;

    @Bind(R.id.nsvParent)
    NestedScrollView mNsvParent;

    @Bind(R.id.nav_view)
    NavigationView mNavigationView;

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @Bind(R.id.view_pager)
    AutoScrollViewPager autoScrollViewPager;

    @Bind(R.id.ivPfaLogo)
    ImageView mIvPfaLogo;

    @Bind(R.id.btnAboutFap)
    Button mBtnAboutFap;

    @Bind(R.id.btnAboutPfa)
    Button mBtnAboutPfa;

    @Bind(R.id.tvVersion)
    TextView mTvVersion;

    private Toolbar mToolbar;

    private RVDashBoardAdapter mRVDashBoardAdapter;

    private Context mContext;
    private ArrayList<DashBoardCategory> mListCategory;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);

        mContext = this;
        getVersion();
        initialize();
    }

    private void initialize() {

        mTvVersion.setText(BuildConfig.VERSION_NAME);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        getUser();

        mNavigationView.getMenu().clear();
        if (user.role.equals("visitor")) {
            mNavigationView.inflateMenu(R.menu.activity_dashboard_menu);
        } else {
            mNavigationView.inflateMenu(R.menu.activity_dashboard_menu_exhibitor);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_drawer);

        initStaticData();
        mRvDashboardList.setNestedScrollingEnabled(false);
        mNsvParent.setSmoothScrollingEnabled(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        mRvDashboardList.setLayoutManager(gridLayoutManager);
        mRvDashboardList.setHasFixedSize(true);
        mRvDashboardList.setNestedScrollingEnabled(false);
        mRVDashBoardAdapter = new RVDashBoardAdapter(mContext, mListCategory, DashBoardActivity.this, user);
        mRvDashboardList.setAdapter(mRVDashBoardAdapter);

        mNavigationView.setNavigationItemSelectedListener(this);
        autoScrollViewPager.requestFocus();


        LinearLayout llAds = (LinearLayout) findViewById(R.id.llAds);
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = width * 180 / 728;
        Log.i("dimen", "width:" + width);
        Log.i("dimen", "height:" + height);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
        llAds.setLayoutParams(params);
        llAds.requestLayout();


    }

    private String getExhibitorScanIds() {
        user_scan_local_tblDao scanLocalData = DataUtil.setUpUserScannedLocal(mContext, Prefs.DB_NAME);
        ArrayList<User> userArraylist = new ArrayList<>(DataUtil.getUserScanned(scanLocalData, user.id, user.role));
        // Get all ids
        String ids = "";
        for (User user : userArraylist)
            ids += user.id + ",";

        // Call API here using ids
        return ids;
    }

    private void getUser() {

        String response = Preferences.getString(mContext, Prefs.USER);
        user = ModelUtil.fromJson(User.class, response);
//        try {
        setUpNavigationView(user);
//        } catch (Exception e) {
//
//        }

    }


    private void setAds(ArrayList<Ads> adsList) {
       /* for (Ads ads : adsList) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(mContext);
            defaultSliderView.image(ads.adBanner);
            defaultSliderView.setScaleType(BaseSliderView.ScaleType.Fit);

            mSlider.setDuration(8);
            mSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
            mSlider.addSlider(defaultSliderView);
        }*/


        if (adsList.size() == 0) {
            autoScrollViewPager.setVisibility(View.GONE);
        }

        AdsAdapter adsAdapter = new AdsAdapter(getSupportFragmentManager(), mContext, adsList);
        autoScrollViewPager.setAdapter(adsAdapter);

        autoScrollViewPager.setInterval(7000);
        autoScrollViewPager.startAutoScroll();
        autoScrollViewPager.setCurrentItem(0);

    }


    private void getAds() {

        AdsInterface adsInterface = KitchenRestClient.create(mContext, AdsInterface.class, true);
        Call<DataWrapper<ArrayList<Ads>>> adsCallResponse = adsInterface.getAds(mContext.getString(R.string.get_ads_url));

        adsCallResponse.enqueue(new Callback<DataWrapper<ArrayList<Ads>>>() {
            @Override
            public void onResponse(Call<DataWrapper<ArrayList<Ads>>> call, Response<DataWrapper<ArrayList<Ads>>> response) {

                if (!response.isSuccessful()) {
                    switch (response.code()) {
                        case 401:
                            User.logout(mContext);
                            return;
                    }
                }
                try {
                    setAds(response.body().getData());
                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Call<DataWrapper<ArrayList<Ads>>> call, Throwable t) {

            }
        });

    }

    @Override
    protected void onStop() {
        //mSlider.stopAutoCycle();
        super.onStop();
    }


    private void setUpNavigationView(User user) {

        View header = mNavigationView.getHeaderView(0);

        final ImageView userImageView = (ImageView) header.findViewById(R.id.userImageView);
        TextView nameTextView = (TextView) header.findViewById(R.id.nameTextView);
        TextView emailTextView = (TextView) header.findViewById(R.id.emailTextView);
        try {
            nameTextView.setText(user.expoVisitor.mExpoVisitorContent.fullName);
        } catch (Exception e) {
//            do nothing
        }
        if (user.logo != null) {
            Log.i("url", "url: " + user.logo);
            Glide.with(mContext)
                    .load(user.logo)
                    .placeholder(R.drawable.fap_logo_new)
                    .error(R.drawable.fap_logo_new)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Log.d("error loading image", "erro " + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            userImageView.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(userImageView);
        }
        emailTextView.setText(user.email);
        Log.i("testing", "this is testingaa: " + user.email);
    }


    private void initStaticData() {

        mListCategory = new ArrayList<>();
        String[] arrNames = {"My Expo QR Code", "Scan QR Code", "My Scans", "Event Schedule", "Conference", "Speakers", "Exhibitors", "Expo Map", "Seminars", "NxtGen", "CFE", "Favorites"};
        String[] arrResources = {"ic_logo_qr_code", "ic_logo_scan_qr_code", "ic_logo_about", "ic_logo_event_schedule", "ic_logo_conference", "ic_logo_speaker", "ic_logo_exhibitors",
                "ic_logo_expo_map", "ic_logo_seminar", "ic_logo_nextgen_icon", "ic_logo_cfe", "ic_logo_favorites"};

        for (int i = 0; i < arrNames.length; i++) {
            DashBoardCategory dashBoardCategory = new DashBoardCategory(arrNames[i], arrResources[i]);
            mListCategory.add(dashBoardCategory);
        }
    }

    @Override
    public void intentLocator(String category) {
        switch (category) {

            case "My Expo QR Code":
                if (!user.role.toLowerCase().equals("exhibitor"))
                    PFABase.showQrCode(mContext, user.qrCode);
                break;

            case "Scan QR Code":
                startActivity(QrCodeReaderActivity.newIntent(mContext));
                break;

            case "Speakers":
                startActivity(SpeakersListActivity.newIntent(mContext));
                break;

            case "Conference":
                startActivity(ConferenceListActivity.newIntent(mContext));
                break;

            case "Expo":

                break;

            case "Exhibitors":
                startActivity(ExhibitorsListActivity.newIntent(mContext));
                break;

            case "Expo Map":
                startActivity(ExpoMapActivity.newIntent(mContext));
                break;

            case "CFE":
                startActivity(CfeDetailsActivity.newIntent(mContext));
                break;

            case "Event Schedule":
                startActivity(EventScheduleListActivity.newIntent(mContext));
                break;

            case "Favorites":
                if (!user.role.toLowerCase().equals("exhibitor"))
                    startActivity(FavoritesListActivity.newIntent(mContext));
                break;

            case "Seminars":
                startActivity(SeminarListActivity.newIntent(mContext));
                break;

            case "My Scans":
                if (user.role.equals("visitor") || user.role.equals("conference")) {
                    startActivity(MyScansActivity.newIntent(mContext));
                    return;
                }
                startActivity(MyScansUserActivity.newIntent(mContext, false));
                break;

            case "NxtGen":
                startActivity(NxtGenDirectionActivity.newIntent(mContext));
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;

//            case R.id.menuItemDownloadConference:
//                // download conference here
//                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    --- Temporarily hide this for search ---
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }*/

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        item.setChecked(true);

        switch (id) {
            case R.id.navProfile:
                startActivity(UserInfoActivity.newIntent(mContext, "" + user.id, false));
                break;
            case R.id.navNotes:
                startActivity(NotesActivityListDisplay.newIntent(mContext));
                break;
            case R.id.navAboutFaphl:
                startActivity(AboutFapActivity.newIntent(mContext));
                break;
            case R.id.navWhoScannedMe:
                startActivity(MyScansUserActivity.newIntent(mContext, true));
                break;
            case R.id.navDownloads:
                Log.i("string", "stringaa" + getExhibitorScanIds());
                startActivity(SendEmailDisplayActivity.newIntent(mContext, getExhibitorScanIds(), "Exhibitor", 0 + ""));
                break;
            case R.id.navAboutPfa:
                startActivity(AboutPfaActivity.newIntent(mContext));
                break;
            case R.id.navSignOut:
                user.logout(mContext);
                startActivity(SplashScreenActivity.newIntent(mContext));
                break;

        }

        item.setChecked(false);
        mDrawerLayout.closeDrawers();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        getAds();
    }

    @OnClick({R.id.btnAboutFap, R.id.btnAboutPfa})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAboutFap:
                startActivity(AboutFapActivity.newIntent(mContext));
                break;
            case R.id.btnAboutPfa:
                startActivity(AboutPfaActivity.newIntent(mContext));
                break;
        }
    }


    private void getVersion() {
        UserInterface userInterface = KitchenRestClient.create(mContext, UserInterface.class, true);
        Call<Version> getAppVersion = userInterface.getVersion(mContext.getString(R.string.get_version));
        getAppVersion.enqueue(new Callback<Version>() {
            @Override
            public void onResponse(Call<Version> call, Response<Version> response) {
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }
                    return;
                }
                String version = response.body().current;
                checkVersion(version);
            }

            @Override
            public void onFailure(Call<Version> call, Throwable t) {

            }
        });
    }

    private void checkVersion(String version) {
        if (!version.equals(BuildConfig.VERSION_NAME)) {
            PFABase.dialogWithOnCLickCancelableNotCancelable(mContext,"Update Available", "Please update for the latest in Franchise Asia Philippines.", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                    finish();

                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

        }

    }

}
