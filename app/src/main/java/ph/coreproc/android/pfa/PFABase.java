package ph.coreproc.android.pfa;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ph.coreproc.android.pfa.models.Country;
import ph.coreproc.android.pfa.models.Event;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.models.Region;
import ph.coreproc.android.pfa.utils.ModelUtil;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by willm on 3/27/2017.
 */

public class PFABase {

    public static void setDefaultRecyclerView(Context context, RecyclerView recyclerView, RecyclerView.Adapter face) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(face);
        face.notifyDataSetChanged();
    }

    public static AlertDialog.Builder dialogPicker(Context context, String title, String[] items, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setItems(items, onClickListener);

        return builder;
    }


    public static void dialogWithOnClickCancelable(Context context, String title, DialogInterface.OnClickListener onClickListener) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(title);
            builder.setPositiveButton("OK", onClickListener);
            builder.create().show();
        } catch (Exception e) {
//            No Context available
        }
    }

    public static void dialogWithOnClick(Context context, String title, DialogInterface.OnClickListener onClickListener) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(title);
            builder.setCancelable(false);
            builder.setPositiveButton("OK", onClickListener);
            builder.create().show();
        } catch (Exception e) {
//            No Context available
        }
    }

    public static void dialogWithOnCLickCancelable(Context context, String title, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onCancel) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(title);
            builder.setPositiveButton("Yes", onClickListener);
            builder.setNegativeButton("No", onCancel);
            builder.create().show();
        } catch (Exception e) {
//            No Context available
        }
    }


    public static void dialogWithOnCLickCancelableNotCancelable(Context context, String title, String content,  DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onCancel) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setMessage(content);
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", onClickListener);
            builder.setNegativeButton("No", onCancel);
            builder.create().show();
        } catch (Exception e) {
//            No context available
        }
    }


    public static void showQrCode(Context context, String qrCode) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_qr_code, null);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        ImageView imageView = (ImageView) view.findViewById(R.id.qrCodeImageView);

        Glide.with(context)
                .load(qrCode)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .dontAnimate()
                .into(imageView);


        dialog.setView(view);
        dialog.create().show();
    }

    public static String getMetadataValue(Context context, String key) {
        ApplicationInfo app = null;
        try {
            app = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = app.metaData;
            return bundle.getString(key, "");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String removeEndSpacing(String item) {

        return item.replaceAll("\\s+$", "");

    }

    public static JsonObject loadJsonObjectFromAssets(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("" + fileName + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            return generateJsonObjectFromString(json);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static JsonObject generateJsonObjectFromString(String jsonString) {
        return new JsonParser().parse(jsonString).getAsJsonObject();
    }

    public static ArrayList<Region> getRegions(Context context) {
        ArrayList<Region> regions = new ArrayList<>();
        JsonArray jsonArray = loadJsonObjectFromAssets(context, "regions").get("regions").getAsJsonArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            regions.add(new Region(jsonArray.get(i).getAsJsonObject()));
        }

        return regions;
    }

    public static ArrayList<Country> getCountries(Context context) {
        ArrayList<Country> countries = new ArrayList<>();
        JsonArray jsonArray = loadJsonObjectFromAssets(context, "countries").get("countries").getAsJsonArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            countries.add(new Country(jsonArray.get(i).getAsJsonObject()));
        }

        return countries;
    }

    public static Exhibitor getExhibitorLocalByScan(Context context, int scannedId) {

        JsonArray jsonArray = loadJsonObjectFromAssets(context, "exhibitors").get("exhibitors").getAsJsonArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            Exhibitor exhibitorLocal = ModelUtil.fromJson(Exhibitor.class, jsonArray.get(i).getAsJsonObject().toString());
            if (exhibitorLocal.id == scannedId) {
                return exhibitorLocal;
            }
        }
        return null;
    }

    public static Exhibitor getSpecificExhibitor(Context context, int id) {
        JsonArray jsonArray = loadJsonObjectFromAssets(context, "exhibitors").get("exhibitors").getAsJsonArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            Exhibitor exhibitor = ModelUtil.fromJson(Exhibitor.class, jsonArray.get(i).getAsJsonObject().toString());
            if (exhibitor.id == id) {
                return exhibitor;
            }
        }
        return null;
    }

    public static ArrayList<Exhibitor> getExhibitorLocals(Context context, String investmentLevelSort, String sectorSort, int action) {
        ArrayList<Exhibitor> items = new ArrayList<>();
        JsonArray jsonArray = loadJsonObjectFromAssets(context, "exhibitors").get("exhibitors").getAsJsonArray();
        switch (action) {
            case 0: // no sorting
                for (int i = 0; i < jsonArray.size(); i++) {
                    Exhibitor exhibitor = ModelUtil.fromJson(Exhibitor.class, jsonArray.get(i).getAsJsonObject().toString());
                    items.add(exhibitor);

                }
                break;
            case 1: // sort by investment level
                for (int i = 0; i < jsonArray.size(); i++) {
                    Exhibitor exhibitor = ModelUtil.fromJson(Exhibitor.class, jsonArray.get(i).getAsJsonObject().toString());
                    try {
                        if (exhibitor.investmentLevel.toLowerCase().equals(investmentLevelSort.toLowerCase())) {
                            items.add(exhibitor);
                        }
                    }catch (Exception e) {
//                        do nothing
                    }
                }
                break;
            case 2: // sort by sector
                for (int i = 0; i < jsonArray.size(); i++) {
                    Exhibitor exhibitor = ModelUtil.fromJson(Exhibitor.class, jsonArray.get(i).getAsJsonObject().toString());
                    if (exhibitor.sector.toLowerCase().equals(sectorSort.toLowerCase())) {
                        items.add(exhibitor);
                    }
                }
                break;
        }
        return items;
    }


    public static ArrayList<Event> getCfeEvents(Context context) {
        ArrayList<Event> events = new ArrayList<>();
        JsonObject jsonObject = loadJsonObjectFromAssets(context, "events").get("events").getAsJsonObject();
        JsonArray jsonArray = jsonObject.get("cfe").getAsJsonArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            events.add(new Event(jsonArray.get(i).getAsJsonObject()));
        }

        return events;
    }

    public static ArrayList<Event> getConferenceEvents(Context context) {
        ArrayList<Event> events = new ArrayList<>();
        JsonObject jsonObject = loadJsonObjectFromAssets(context, "events").get("events").getAsJsonObject();
        JsonArray jsonArray = jsonObject.get("conference").getAsJsonArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            events.add(new Event(jsonArray.get(i).getAsJsonObject()));
        }

        return events;
    }

    public static ArrayList<Event> getExpoEvents(Context context) {
        ArrayList<Event> events = new ArrayList<>();
        JsonObject jsonObject = loadJsonObjectFromAssets(context, "events").get("events").getAsJsonObject();
        JsonArray jsonArray = jsonObject.get("expo").getAsJsonArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            events.add(new Event(jsonArray.get(i).getAsJsonObject()));
        }

        return events;
    }

    public static ArrayList<Event> getSeminarEvents(Context context) {
        ArrayList<Event> events = new ArrayList<>();
        JsonObject jsonObject = loadJsonObjectFromAssets(context, "events").get("events").getAsJsonObject();
        JsonArray jsonArray = jsonObject.get("seminars").getAsJsonArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            events.add(new Event(jsonArray.get(i).getAsJsonObject()));
        }

        return events;
    }

    public static Region getRegion(Context context, String id) {

        ArrayList<Region> regions = new ArrayList<>(getRegions(context));

        for (Region region : regions) {
            if (region.id.equalsIgnoreCase(id)) {
                return region;
            }
        }

        return null;
    }

    public static boolean isEmailValid(String email) {
        return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean validateInputs(ViewGroup mainView) {
        for (int i = 0; i < mainView.getChildCount(); i++) {
            View view = mainView.getChildAt(i);
            if (view instanceof EditText) {
                EditText editText = (EditText) view;

                // validate all input fields in view
                if (editText.getText().toString().isEmpty()) {
                    return false;
                }
            }
        }
        return true;

    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }


    public static boolean isUserLoggedIn(Context context) {
        return Preferences.getString(context, Preferences.API_KEY).length() != 0;
    }


    public static Dialog loadDefaultProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getString(R.string.progress_dialog_message_please_wait));
        return progressDialog;
    }

    public static Dialog loadDefaultProgressDialogCancellable(final Context context, final Activity activity) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getString(R.string.progress_dialog_message_please_wait));
        progressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK &&
                        event.getAction() == KeyEvent.ACTION_UP &&
                        !event.isCanceled()) {
                    dialog.cancel();
//                    showDialog(DIALOG_MENU);
                    activity.finish();
                    return true;
                }
                return false;
            }
        });

        return progressDialog;
    }

    public static String loadJsonStringFromAssets(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("json/" + fileName + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            return json;
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String loadStringFromAssets(Context context, String file) {
        String json = "";
        try {
            InputStream is = context.getAssets().open("rawstrings/" + file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            return json;
        } catch (IOException ex) {
            ex.printStackTrace();
            return "";
        }
    }


    public static String getTime(String getDate) {
        String dateString = getDate;
        Log.i("date", "getDate" + getDate);
        Date convertedDate = new Date();
        try {
            convertedDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return (new SimpleDateFormat("hh:mm a")).format(convertedDate);
    }

    public static String getDate(String getDate) {
        String dateString = getDate;
        Log.i("date", "getDate" + getDate);
        Date convertedDate = new Date();
        try {
            convertedDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String dateFormatted = (new SimpleDateFormat("yyyy-MM-dd")).format(convertedDate);

        String month = "";

        String[] dateExplode = dateFormatted.split("-");

        if (dateExplode[1].equals("01")) {
            month = "January";
        }

        if (dateExplode[1].equals("02")) {
            month = "February";
        }

        if (dateExplode[1].equals("03")) {
            month = "March";
        }

        if (dateExplode[1].equals("04")) {
            month = "April";
        }

        if (dateExplode[1].equals("05")) {
            month = "May";
        }

        if (dateExplode[1].equals("06")) {
            month = "June";
        }

        if (dateExplode[1].equals("07")) {
            month = "July";
        }

        if (dateExplode[1].equals("08")) {
            month = "August";
        }

        if (dateExplode[1].equals("09")) {
            month = "September";
        }

        if (dateExplode[1].equals("10")) {
            month = "October";
        }

        if (dateExplode[1].equals("11")) {
            month = "November";
        }

        if (dateExplode[1].equals("12")) {
            month = "December";
        }


        return month + " " + dateExplode[2] + ", " + dateExplode[0];
    }


}
