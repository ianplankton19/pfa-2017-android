package ph.coreproc.android.pfa.models;

import com.google.gson.JsonObject;

import java.io.Serializable;

/**
 * Created by IanBlanco on 4/17/2017.
 */

public class Country implements Serializable{

    public String id;
    public String name;
    public String code;
    public String latitude;
    public String longitude;
    public String createdAt;
    public String updateAt;

    public Country(JsonObject jsonObject){

        this.id = jsonObject.get("id").getAsString();
        this.name = jsonObject.get("name").getAsString();
        this.code = jsonObject.get("code").getAsString();
//        this.latitude = jsonObject.get("latitude").getAsString();
//        this.longitude = jsonObject.get("longitude").getAsString();
//        this.createdAt = jsonObject.get("created_at").getAsString();
//        this.updateAt = jsonObject.get("updated_at").getAsString();

    }

    public Country(String id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }


}
