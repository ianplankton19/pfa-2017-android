package ph.coreproc.android.pfa.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by IanBlanco on 6/1/2017.
 */

public class QrRequestId {

    public QrRequestId(int exhibitorId) {
        this.exhibitorId = exhibitorId;
    }

    @SerializedName("exhibitor_id")
    @Expose
    public int exhibitorId;
}
