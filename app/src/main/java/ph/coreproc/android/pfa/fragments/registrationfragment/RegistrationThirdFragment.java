package ph.coreproc.android.pfa.fragments.registrationfragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.CountryDisplayActivity;
import ph.coreproc.android.pfa.activities.RegionDisplayActivity;
import ph.coreproc.android.pfa.activities.RegistrationActivity;
import ph.coreproc.android.pfa.models.Country;
import ph.coreproc.android.pfa.models.Region;

/**
 * Created by IanBlanco on 3/29/2017.
 */

public class RegistrationThirdFragment extends Fragment {


    @Bind(R.id.etCountry)
    EditText mEtCountry;

    @Bind(R.id.etRegion)
    EditText mEtRegion;

    @Bind(R.id.etMobileNo)
    EditText mEtMobileNo;

    @Bind(R.id.tvRegionAsterisk)
    TextView mTvRegionAsterisk;

    private TextWatcher mTextWatcher;

    private EventBus mEventBus = EventBus.getDefault();
    RegistrationActivity mRegistrationActivity;


//    private int checkId = 0;


    public static RegistrationThirdFragment newInstance() {

        Bundle args = new Bundle();

        RegistrationThirdFragment fragment = new RegistrationThirdFragment();
//        fragment.setArguments(args);
        return fragment;
    }

    private Country mCountry;

//    Set to Philippines


    @Override
    public void onStart() {
        super.onStart();
        mEventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mEventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(Region region) {
        Log.i("daan", "daan");
        mEtRegion.setText(region.name);
        mRegistrationActivity.mRegionStr = region.id;
        mEventBus.removeStickyEvent(region);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(Country country) {
        mCountry = country;
        Log.i("daan", "daan_county");
        mEtCountry.setText(mCountry.name);
        mRegistrationActivity.mCountryStr = mCountry.id;
        mEventBus.removeStickyEvent(mCountry);
//        Check if Philippines
        boolean counrtyCheck = mCountry.id.equals("174");
        mEtRegion.setEnabled(counrtyCheck);
        mEtRegion.setAlpha(counrtyCheck ? (float) 1.0 : (float) 0.4);
        mTvRegionAsterisk.setVisibility(counrtyCheck ? View.VISIBLE : View.GONE);
        if (!counrtyCheck) {
            mEtRegion.setText("");
            mRegistrationActivity.mRegionStr = "";
        }

        mEtMobileNo.setText(counrtyCheck ? "63" : "");
        mEventBus.removeStickyEvent(mCountry);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.registration_third_page_new, container, false);

        mRegistrationActivity = (RegistrationActivity) getActivity();

        ButterKnife.bind(this, view);
        mCountry = new Country("174", "Philippines", "PH");
        initialize();
        return view;
    }

    public void initialize() {
        mEtCountry.setText(mCountry.name);
        mRegistrationActivity.mCountryStr = mCountry.id;
        mEtCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(CountryDisplayActivity.newIntent(getActivity()));
            }
        });

        mEtRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(RegionDisplayActivity.newIntent(getActivity()));
            }
        });

        mEtMobileNo.addTextChangedListener(setTextWatchers(mEtMobileNo));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private TextWatcher setTextWatchers(final EditText editText) {


        mTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                switch (editText.getId()) {


                    case R.id.etMobileNo:
                        mRegistrationActivity.mMobileNoStr = "" + s;
                        break;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        return mTextWatcher;
    }


}