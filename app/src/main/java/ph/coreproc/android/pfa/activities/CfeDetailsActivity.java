package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.bluejamesbond.text.style.JustifiedSpan;
import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.conference.SpeakerListRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.Speaker;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.rest.SpeakerInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 4/18/2017.
 */

public class CfeDetailsActivity extends BaseActivity implements SpeakerListRecyclerViewAdapter.Callback {

    @Bind(R.id.tvDetails)
    TextView mTvDetails;

    @Bind(R.id.tvSpeaker)
    TextView mTvSpeaker;

    @Bind(R.id.rvSpeaker)
    RecyclerView mRvSpeaker;

    @Bind(R.id.wbOne)
    WebView mWbOne;


    private SpeakerListRecyclerViewAdapter mSpeakerListRecyclerViewAdapter;
    private ArrayList<Speaker> mSpeakerArrayList;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, CfeDetailsActivity.class);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_cfe_details;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initialize() {
        String strCfeDetails =getResources().getString(R.string.cfe_details) ;

        mSpeakerArrayList = new ArrayList<>();
        Spannable sb = new SpannableString(strCfeDetails);
        sb.setSpan(new StyleSpan(Typeface.BOLD), 4, 41, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new JustifiedSpan().getTextAlignment(), 0, strCfeDetails.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvDetails.setText(sb);
        // ((TextViewEx) mTvDetails).setText(mTvDetails.getText().toString(), true);

        mWbOne.getSettings().setDefaultFixedFontSize((int) getResources().getDimension(R.dimen.adapter_text_title));
        mWbOne.loadData(strCfeDetails, "text/html","utf-8");

        mRvSpeaker.setLayoutManager(new LinearLayoutManager(this));
        mSpeakerListRecyclerViewAdapter = new SpeakerListRecyclerViewAdapter(mContext, mSpeakerArrayList, CfeDetailsActivity.this);
        mRvSpeaker.setAdapter(mSpeakerListRecyclerViewAdapter);
        getCfeSpeaker();
    }


    private void getCfeSpeaker() {

        final Dialog dialog = PFABase.loadDefaultProgressDialogCancellable(mContext, this);
        dialog.show();

        SpeakerInterface speakerInterface = KitchenRestClient.create(mContext, SpeakerInterface.class, true);
        Call<DataWrapper<ArrayList<Speaker>>> speakerArraylistCall = speakerInterface.getSpeakers(mContext.getString(R.string.get_speakers_url));
        speakerArraylistCall.enqueue(new Callback<DataWrapper<ArrayList<Speaker>>>() {
            @Override
            public void onResponse(Call<DataWrapper<ArrayList<Speaker>>> call, Response<DataWrapper<ArrayList<Speaker>>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

//                for (Speaker search_item : response.body().getData()) {
//
//                    if (search_item.speakerName.contains("CFE")) {
//                        mSpeakerArrayList.add(search_item);
//                    }
//                }

                for (int i = 0; i < response.body().getData().size(); i++) {
                    if (response.body().getData().get(i).speakerName.contains("SIMNICK")) {
                        mSpeakerArrayList.add(response.body().getData().get(i));
                    }
                }
                Log.i("list", "List: " + mSpeakerArrayList.size());
                mSpeakerListRecyclerViewAdapter.changeData(mSpeakerArrayList);

            }

            @Override
            public void onFailure(Call<DataWrapper<ArrayList<Speaker>>> call, Throwable t) {
                dialog.dismiss();
//                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                mTvSpeaker.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void intentSpeakerDetails(String id) {
        startActivity(SpeakerDetailsActivity.newIntent(mContext, id, false));
    }
}
