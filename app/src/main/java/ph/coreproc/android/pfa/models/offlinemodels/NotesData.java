package ph.coreproc.android.pfa.models.offlinemodels;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by IanBlanco on 4/6/2017.
 */

public class NotesData implements Serializable {

    @SerializedName("data")
    @Expose
    public ArrayList<Notes> notesData;

    private JsonObject data;

    public ArrayList<Notes> getNotesData() {
        return notesData;
    }

    public void setNotesData(ArrayList<Notes> notesData) {
        this.notesData = notesData;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }


    public NotesData(JsonArray jsonArray) {
        data = new JsonObject();
        data.add("data", jsonArray);
    }

    public JsonObject getData() {
        return data;
    }
}
