package ph.coreproc.android.pfa.models;

import com.google.gson.JsonObject;

import java.io.Serializable;

/**
 * Created by IanBlanco on 4/19/2017.
 */

public class Event implements Serializable {

        public String date;
        public String topic;
        public String time;
//        public ArrayList<Speaker> speakers;
        public String location;

    public Event(JsonObject jsonObject) {
        this.date = jsonObject.get("date").getAsString();
        this.topic = jsonObject.get("topic").getAsString();
        this.time = jsonObject.get("time").getAsString();
        this.location = jsonObject.get("location").getAsString();
    }
}


