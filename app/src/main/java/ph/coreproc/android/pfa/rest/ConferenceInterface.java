package ph.coreproc.android.pfa.rest;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.models.Session;
import ph.coreproc.android.pfa.models.Topics;
import ph.coreproc.android.pfa.models.requests.CommentBody;
import ph.coreproc.android.pfa.models.requests.DownloadBody;
import ph.coreproc.android.pfa.models.requests.QuestionBody;
import ph.coreproc.android.pfa.models.requests.RatingBody;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by IanBlanco on 4/5/2017.
 */

public interface ConferenceInterface {

    @GET
    Call<DataWrapper<ArrayList<Session>>> getConference(@Url String url);

    @GET("/api/v1/conferences/{id}")
    Call<DataWrapper<Topics>> getConferenceDetails(@Path("id") String id);

    @POST("/api/v1/conferences/{id}/comments")
    Call<ResponseBody> postConferenceComment(@Path("id") String id, @Body CommentBody commentBody);

    @POST("/api/v1/conferences/{id}/ratings")
    Call<ResponseBody> postConferenceRating(@Path("id") String id, @Body RatingBody ratingBody);

    @POST("/api/v1/conferences/{id}/questions")
    Call<ResponseBody> postConferenceQuestion(@Path("id") String id, @Body QuestionBody questionBody);

    @POST("/api/v1/conferences/{id}/presentation/download")
    Call<ResponseBody> postConferenceDownload(@Path("id") String id, @Body DownloadBody downloadBody);

}
