package ph.coreproc.android.pfa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Region;

/**
 * Created by IanBlanco on 4/4/2017.
 */

public class RegionAdapter extends RecyclerView.Adapter<RegionAdapter.ViewHolder> {

    public interface Callback{

        void getPosition(Region region);
    }


    private Callback mCallback;
    private List<Region> mRegions;
    private Context mContext;

    public RegionAdapter(List<Region> regions, Context context, Callback callback) {
        mRegions = regions;
        mContext = context;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_region, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Region region = mRegions.get(position);
        holder.mRegionName.setText(region.name);
        holder.mLlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.getPosition(region);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mRegions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.regionName)
        TextView mRegionName;

        @Bind(R.id.llContainer)
        LinearLayout mLlContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
