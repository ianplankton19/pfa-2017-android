package ph.coreproc.android.pfa.models;

/**
 * Created by IanBlanco on 3/27/2017.
 */

public class DashBoardCategory {

    private String categoryName;

    private String categoryResource;

    public DashBoardCategory(String categoryName, String categoryResource) {
        this.categoryName = categoryName;
        this.categoryResource = categoryResource;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getCategoryResource() {
        return categoryResource;
    }
}

