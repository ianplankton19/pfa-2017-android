package ph.coreproc.android.pfa.models.offlinemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 4/6/2017.
 */

public class Notes extends OfflineExtension implements Serializable {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("noteName")
    @Expose
    public String noteName;

    @SerializedName("note")
    @Expose
    public String note;

    @SerializedName("category")
    @Expose
    public String category;

    @SerializedName("is_checked")
    @Expose
    public boolean isChecked = false;

    @SerializedName("is_showed")
    @Expose
    public boolean isShowed = false;



    public Notes(int id, String note, String noteName, String category, int userid, String role) {
        this.id = id;
        this.note = note;
        this.noteName = noteName;
        this.category = category;
        this.userId = userid;
        this.userRole = role;
    }

//    public JsonObject getJsonObject() {
//
//        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("id", this.id);
//        jsonObject.addProperty("noteName", this.noteName);
//        jsonObject.addProperty("note", this.note);
//        jsonObject.addProperty("category", this.category);
//        return jsonObject;
//    }
}
