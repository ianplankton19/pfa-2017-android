package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.SpeakersRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.Speaker;
import ph.coreproc.android.pfa.rest.SpeakerInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpeakersListActivity extends BaseActivity implements SpeakersRecyclerViewAdapter.Callback {


    SearchView searchView;
    SearchView.OnQueryTextListener queryTextListener;


    @Bind(R.id.speakersListRecyclerView)
    RecyclerView speakersListRecyclerView;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    @Bind(R.id.tabLayout)
    TabLayout mTabLayout;

    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    private TextWatcher mTextWatcher;
    private MenuItem searchItem;

    private SpeakersRecyclerViewAdapter mSpeakersRecyclerViewAdapter;
    private ArrayList<Speaker> mSpeakerArrayList;
    private ArrayList<Speaker> mModeratorArrayList;
    private ArrayList<Speaker> mFacilitatorArrayList;


    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, SpeakersListActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    private void initialize() {

        mSpeakerArrayList = new ArrayList<>();
        mModeratorArrayList = new ArrayList<>();
        mFacilitatorArrayList = new ArrayList<>();


        mSpeakersRecyclerViewAdapter = new SpeakersRecyclerViewAdapter(mContext, mSpeakerArrayList, SpeakersListActivity.this, this);
        PFABase.setDefaultRecyclerView(mContext, speakersListRecyclerView, mSpeakersRecyclerViewAdapter);

        setUpTabLayout();
        getSpeakers();
    }

    private void setUpTabLayout() {

        mTabLayout.setTabTextColors(ContextCompat.getColor(mContext, R.color.divider_conference), Color.WHITE);
        mTabLayout.addTab(mTabLayout.newTab().setText("SPEAKERS"));
        mTabLayout.addTab(mTabLayout.newTab().setText("FACILITATORS"));
        mTabLayout.addTab(mTabLayout.newTab().setText("MODERATORS"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:
                        mSpeakersRecyclerViewAdapter.changeData(mSpeakerArrayList);
                        break;

                    case 1:
                        mSpeakersRecyclerViewAdapter.changeData(mFacilitatorArrayList);
                        break;
                    case 2:
                        mSpeakersRecyclerViewAdapter.changeData(mModeratorArrayList);
                        break;

                }

                searchView.onActionViewCollapsed();


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                searchView.onActionViewCollapsed();
//                mSpeakersRecyclerViewAdapter.changeData(filterSpeakers(tab.getText().toString()));

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
//                searchView.onActionViewCollapsed();
//                mSpeakersRecyclerViewAdapter.getFilter().filter("");
//                mSpeakersRecyclerViewAdapter.changeData(filterSpeakers(tab.getText().toString()));

            }
        });

    }

    private ArrayList<Speaker> filterSpeakers(String role) {

        ArrayList<Speaker> temp = new ArrayList<>();
        for (Speaker speaker : mSpeakerArrayList) {
            if (role.toLowerCase().contains(speaker.role)) {
                temp.add(speaker);
            }
        }

        return temp;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_speakers_list;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_item, menu);
        searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) SpeakersListActivity.this.getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchItem.getActionView();
//        if (searchItem != null) {
//            KitchenUiUtils.showAlertDialog(mContext, "dumaan");
//        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    // Toast like print
                    mSpeakersRecyclerViewAdapter.getFilter().filter(newText);
                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
//        getMenuInflater().inflate(R.menu.search_item, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
                searchView.setOnQueryTextListener(queryTextListener);
                // Not implemented here


                return false;
            case android.R.id.home:
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                    return false;
                }
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void getSpeakers() {

        final Dialog dialog = PFABase.loadDefaultProgressDialogCancellable(mContext, this);
        dialog.show();
        SpeakerInterface speakerInterface = KitchenRestClient.create(mContext, SpeakerInterface.class, true);
        Call<DataWrapper<ArrayList<Speaker>>> speakersResponseCall = speakerInterface.getSpeakers(getString(R.string.get_speakers_url));
        speakersResponseCall.enqueue(new Callback<DataWrapper<ArrayList<Speaker>>>() {
            @Override
            public void onResponse(Call<DataWrapper<ArrayList<Speaker>>> call, Response<DataWrapper<ArrayList<Speaker>>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }
                sortData(response.body().getData());

            }

            @Override
            public void onFailure(Call<DataWrapper<ArrayList<Speaker>>> call, Throwable t) {
                dialog.dismiss();
                PFABase.dialogWithOnClick(mContext, getString(R.string.dialog_error_global), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        });

    }

    private void sortData(ArrayList<Speaker> items) {

        for (Speaker speaker : items) {
        try {
            if (speaker.role.toLowerCase().contains("speaker")) {
                mSpeakerArrayList.add(speaker);
            }

            if (speaker.role.toLowerCase().contains("moderator")) {
                mModeratorArrayList.add(speaker);
            }

            if (speaker.role.toLowerCase().contains("facilitator")) {
                mFacilitatorArrayList.add(speaker);
            }
        } catch (Exception e) {
//            no role in data
        }
        }

        mSpeakersRecyclerViewAdapter.changeData(mSpeakerArrayList);

    }

    @Override
    public void intentToDetails(int id) {
        startActivity(SpeakerDetailsActivity.newIntent(mContext, "" + id, false));
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
//        mBtnReload.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
//        mBtnReload.setVisibility(View.VISIBLE);
    }
}
