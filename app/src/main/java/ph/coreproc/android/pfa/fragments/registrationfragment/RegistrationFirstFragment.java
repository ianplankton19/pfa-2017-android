package ph.coreproc.android.pfa.fragments.registrationfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.RegistrationActivity;

/**
 * Created by IanBlanco on 3/29/2017.
 */

public class RegistrationFirstFragment extends Fragment {

    @Bind(R.id.etEmail)
    EditText mEtEmail;

    @Bind(R.id.etPassword)
    EditText mEtPassword;

    RegistrationActivity mRegistrationActivity;

    TextWatcher mTextWatcher;



    public static RegistrationFirstFragment newInstance() {

        Bundle args = new Bundle();

        RegistrationFirstFragment fragment = new RegistrationFirstFragment();
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.registration_first_page_new, container, false);
        ButterKnife.bind(this, view);
        mRegistrationActivity = (RegistrationActivity) getActivity();
        initialize();

        return view;

    }

    private void initialize() {

//        setTextWatchers();

        mEtEmail.addTextChangedListener(setTextWatchers(mEtEmail));
        mEtPassword.addTextChangedListener(setTextWatchers(mEtPassword));

    }


    private TextWatcher setTextWatchers(final EditText editText) {


        mTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                switch (editText.getId()) {

                    case R.id.etEmail:

                        mRegistrationActivity.mEmailStr = "" + s;

//                        Log.i("tag", "email:" + mEmailStr);
                        break;

                    case R.id.etPassword:

                        mRegistrationActivity.mPasswordStr = "" + s;
//                        Log.i("tag", "password:" + mPasswordStr);
                        break;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        return mTextWatcher;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
