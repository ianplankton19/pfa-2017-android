package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.eventschedule.EventScheduleViewPagerAdapter;
import ph.coreproc.android.pfa.fragments.eventschedule.CfeFragment;
import ph.coreproc.android.pfa.fragments.eventschedule.ConferenceFragment;
import ph.coreproc.android.pfa.fragments.eventschedule.ExpoFragment;
import ph.coreproc.android.pfa.fragments.eventschedule.SeminarFragment;

/**
 * Created by IanBlanco on 4/19/2017.
 */

public class EventScheduleListActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.eventScheduleTabLayout)
    TabLayout mEventScheduleTabLayout;

    @Bind(R.id.eventScheduleListViewPager)
    ViewPager mEventScheduleListViewPager;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, EventScheduleListActivity.class);

        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_event_schedule;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        initialize();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void initialize() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setViewPager();
        mEventScheduleTabLayout.setupWithViewPager(mEventScheduleListViewPager);
    }

    private void setViewPager() {
        EventScheduleViewPagerAdapter adapter = new EventScheduleViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(CfeFragment.newInstance(), "CFE");
        adapter.addFragment(ConferenceFragment.newInstance(), "CONFERENCE");
        adapter.addFragment(ExpoFragment.newInstance(), "EXPO");
        adapter.addFragment(SeminarFragment.newInstance(), "SEMINARS");
        mEventScheduleListViewPager.setAdapter(adapter);
        mEventScheduleListViewPager.setOffscreenPageLimit(adapter.getCount());
    }
}
