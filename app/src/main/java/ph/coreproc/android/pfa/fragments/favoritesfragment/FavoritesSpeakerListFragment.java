package ph.coreproc.android.pfa.fragments.favoritesfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.preferences.Preferences;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import database.db.speaker_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.SpeakerDetailsActivity;
import ph.coreproc.android.pfa.adapters.favorites.FavoriteSpeakerRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.Speaker;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;

/**
 * Created by IanBlanco on 4/12/2017.
 */

public class FavoritesSpeakerListFragment extends Fragment implements FavoriteSpeakerRecyclerViewAdapter.Callback{

    @Bind(R.id.rvFavorites)
    RecyclerView mRvFavorites;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    private speaker_local_tblDao mSpeaker_local_tblDao;

    private ArrayList<Speaker> mSpeakerLocals;

    private Context mContext;
    private FavoriteSpeakerRecyclerViewAdapter mFavoriteSpeakerRecyclerViewAdapter;

    private User mUser;


    public static FavoritesSpeakerListFragment newInstance(){

        FavoritesSpeakerListFragment favoritesSpeakerListFragment = new FavoritesSpeakerListFragment();

        return favoritesSpeakerListFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        mSpeakerLocals = DataUtil.getSpeakerLocal(mSpeaker_local_tblDao, mUser.id, mUser.role);
        mFavoriteSpeakerRecyclerViewAdapter.update(mSpeakerLocals);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_favorites_conference_display, null);
        ButterKnife.bind(this, view);
        mContext = getActivity();

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        mSpeaker_local_tblDao = DataUtil.setUpSpeakerLocal(mContext, Prefs.DB_NAME);
        initialize();
        return view;
    }

    private void initialize() {

        mSpeakerLocals = new ArrayList<>();
        mRvFavorites.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mFavoriteSpeakerRecyclerViewAdapter = new FavoriteSpeakerRecyclerViewAdapter(mContext, mSpeakerLocals, FavoritesSpeakerListFragment.this);
        mRvFavorites.setAdapter(mFavoriteSpeakerRecyclerViewAdapter);
        getData();
    }

    private void getData(){
        mSpeakerLocals = DataUtil.getSpeakerLocal(mSpeaker_local_tblDao, mUser.id, mUser.role);
        mFavoriteSpeakerRecyclerViewAdapter.add(mSpeakerLocals);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void intentToDetails(int id) {
        startActivity(SpeakerDetailsActivity.newIntent(mContext, "" + id, true));
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }
}
