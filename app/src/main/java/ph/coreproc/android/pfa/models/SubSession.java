package ph.coreproc.android.pfa.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import ph.coreproc.android.pfa.R;

/**
 * Created by IanBlanco on 5/3/2017.
 */

public class SubSession implements Serializable {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("session_name")
    @Expose
    public String sessionName;

    @SerializedName("start_time")
    @Expose
    public String startTime;

    @SerializedName("end_time")
    @Expose
    public String endTime;

    @SerializedName("topics")
    @Expose
    public TopicList topicList;


    public View getViewDisplay(Context context, View.OnClickListener onClickListener, ArrayList<Topics> topicsItems, String day) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_subsession_item, null);
        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        LinearLayout llSubSessionContainer = (LinearLayout) view.findViewById(R.id.llSubSessionContainer);
        LinearLayout llSubSessionContainerMain = (LinearLayout) view.findViewById(R.id.llSubSessionContainerMain);
        view.setOnClickListener(onClickListener);

        if (!arrCountChecker(topicsItems, day)) {
            llSubSessionContainerMain.setVisibility(View.GONE);
        }

        tvName.setText(sessionName);
        for (int i = 0; i < topicsItems.size(); i++) {
            if (topicsItems.get(i).day.equals(day)) {
                ArrayList<SubTopic.SubTopicField> itemsSubtopic = topicsItems.get(i).subTopic.subTopics;
                llSubSessionContainer.addView(topicsItems.get(i).getViewDisplay(context, onClickListener, itemsSubtopic, day));
            }
        }

        return view;
    }


    private boolean arrCountChecker(ArrayList<Topics> items, String day) {

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).day.equals(day)) {
                return true;
            }
        }
        return false;
    }

}
