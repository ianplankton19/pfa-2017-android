package ph.coreproc.android.pfa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 5/8/2017.
 */

public class Ads implements Serializable{

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("link")
    @Expose
    public String link;

    @SerializedName("ad_banner")
    @Expose
    public String adBanner;
}
