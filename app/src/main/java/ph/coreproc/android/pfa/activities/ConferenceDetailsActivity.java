package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;
import database.db.conference_local_tbl;
import database.db.conference_local_tblDao;
import de.hdodenhof.circleimageview.CircleImageView;
import io.techery.properratingbar.ProperRatingBar;
import io.techery.properratingbar.RatingListener;
import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.conference.SpeakerListRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.Speaker;
import ph.coreproc.android.pfa.models.Topics;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.offlinemodels.TopicLocal;
import ph.coreproc.android.pfa.models.requests.CommentBody;
import ph.coreproc.android.pfa.models.requests.RatingBody;
import ph.coreproc.android.pfa.rest.ConferenceInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import ph.coreproc.android.pfa.utils.UiUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConferenceDetailsActivity extends BaseActivity implements SpeakerListRecyclerViewAdapter.Callback {


    public static String ARGS_CONFERENCE_ID = "ARGS_CONFERENCE_ID";
    public static String ARGS_IS_LOCAL = "ARGS_IS_LOCAL";
    public static String ARGS_TO_DISPLAY = "ARGS_TO_DISPLAY";



    private String conferenceIdStr;
    private boolean mIsLocal;
    private boolean mDisplayText;

    private Topics mTopics;
    private RatingBody mRatingBody;

    @Bind(R.id.llDate)
    LinearLayout mLlDate;

    @Bind(R.id.llDay)
    LinearLayout mLlDay;

    @Bind(R.id.llTime)
    LinearLayout mLlTime;

    @Bind(R.id.llLocation)
    LinearLayout mLlLocation;

    @Bind(R.id.llDuration)
    LinearLayout mLlDuration;

    @Bind(R.id.llDescription)
    LinearLayout mLlDescription;

    @Bind(R.id.upperRatingBar)
    ProperRatingBar upperRatingBar;

    @Bind(R.id.conferenceTitleTextView)
    TextView mConferenceTitleTextView;

    @Bind(R.id.conferenceDateTextView)
    TextView mConferenceDateTextView;

    @Bind(R.id.conferenceLocationTextView)
    TextView mConferenceLocationTextView;

    @Bind(R.id.conferenceTimeTextView)
    TextView mConferenceTimeTextView;

    @Bind(R.id.conferenceDayTextView)
    TextView mConferenceDayTextView;

    @Bind(R.id.conferenceDurationTextView)
    TextView mConferenceDurationTextView;

    @Bind(R.id.conferenceDescriptionTextView)
    TextView mConferenceDescriptionTextView;

    @Bind(R.id.conferenceSpeakerImageView)
    CircleImageView mConferenceSpeakerImageView;

    @Bind(R.id.proceed)
    ImageView mProceed;

    @Bind(R.id.conferenceSpeakerTextView)
    TextView mConferenceSpeakerTextView;

    @Bind(R.id.favoriteButton)
    Button mFavoriteButton;

    @Bind(R.id.activity_conference_details)
    LinearLayout mActivityConferenceDetails;

    @Bind(R.id.fabNotes)
    FloatingActionButton mFabNotes;

    @Bind(R.id.rvSpeaker)
    RecyclerView mRvSpeaker;

    @Bind(R.id.llFooter)
    LinearLayout mLlFooter;

    @Bind(R.id.tvRate)
    TextView mTvRate;

    @Bind(R.id.submitButton)
    Button mSubmitButton;

    @Bind(R.id.etComment)
    EditText mEtComment;

    @Bind(R.id.createQuestion)
    Button mCreateQuestion;

    @Bind(R.id.tvSpeaker)
    TextView mTvSpeaker;


    //    Array for online
    private ArrayList<Speaker> mSpeakerArrayList;

    private ArrayList<Topics> mTopicsArrayList;
    private ArrayList<Topics> mTopicLocalArrayList = new ArrayList<>();
    private SpeakerListRecyclerViewAdapter mSpeakerListRecyclerViewAdapter;

    private conference_local_tbl mConference_local_tbl;
    private conference_local_tblDao mConference_local_tblDao;

    private User mUser;


    private Call<ResponseBody> mPostRateCall;

    public static Intent newIntent(Context context, String id, boolean isLocal, boolean displayText) {
        Intent intent = new Intent(context, ConferenceDetailsActivity.class);
        intent.putExtra(ARGS_CONFERENCE_ID, id);
        intent.putExtra(ARGS_IS_LOCAL, isLocal);
        intent.putExtra(ARGS_TO_DISPLAY, displayText);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        upperRatingBar.setListener(ratingListener);
        upperRatingBar.toggleClickable();
        mConference_local_tblDao = DataUtil.setUpConferenceLocal(mContext, Prefs.DB_NAME);
        initialize();
    }

    private void initialize() {


        final Bundle extras = getIntent().getExtras();
        conferenceIdStr = extras.getString(ARGS_CONFERENCE_ID);
        mIsLocal = extras.getBoolean(ARGS_IS_LOCAL);
        mDisplayText = extras.getBoolean(ARGS_TO_DISPLAY);

        if (!mIsLocal) {
            getData();
        } else {

//            setFieldLocal(getDataLocal());
            setFields(getDataLocal());
        }

        String response = Preferences.getString(mContext, Prefs.USER);
        User user = ModelUtil.fromJson(User.class, response);

        if (user.role.equals("exhibitor")) {
            mLlFooter.setVisibility(View.GONE);
        }

    }

//    private void setFieldLocal(TopicLocal topicLocal) {
//        mTopicLocal = topicLocal;
//
//        if (checkItemToLocal(mTopicLocal) != null) {
//            mFavoriteButton.setText("Unfavorite");
//        }
//
//        hideTextViews();
//        upperRatingBar.setRating(mTopicLocal.rating);
//        mConferenceTitleTextView.setText(mTopicLocal.name);
//        mConferenceDateTextView.setText(Html.fromHtml("<b>Date:</b> " + PFABase.getDate(mTopicLocal.startsAt)));
//        mConferenceDayTextView.setText(Html.fromHtml("<b>Day:</b> " + mTopicLocal.day));
//        mConferenceTimeTextView.setText(Html.fromHtml("<b>Time:</b> " + PFABase.getTime(mTopicLocal.startsAt)));
//        mConferenceDescriptionTextView.setText(Html.fromHtml("<b>Description:</b> " + mTopicLocal.description));
//        mConferenceLocationTextView.setText(Html.fromHtml("<b>Location:</b> " + mTopicLocal.location));
//        mConferenceDurationTextView.setText(Html.fromHtml("<b>Duration:</b> " + Integer.parseInt(mTopicLocal.duration) / 60 + "mins."));
////        mLlFooter.setVisibility(View.GONE);
//        mFabNotes.setVisibility(View.GONE);
//        mSpeakerLocalArrayList = new ArrayList<>();
//        for (SpeakerLocal speakerLocal : mTopicLocal.mSpeakerLocalData.speakers) {
//            mSpeakerLocalArrayList.add(speakerLocal);
//        }
//        mTvSpeaker.setVisibility(mTopicLocal.mSpeakerLocalData.speakers.size() == 0 ? View.GONE : View.VISIBLE);
//        mRvSpeaker.setLayoutManager(new LinearLayoutManager(this));
//        mFavoritesSpeakerRecyclerViewAdapter = new FavoritesSpeakerConfernceRecyclerViewAdapter(mContext, mSpeakerLocalArrayList);
//        mRvSpeaker.setAdapter(mFavoritesSpeakerRecyclerViewAdapter);
//    }


    private void setFields(Topics topics) {
        mTopics = topics;

        if (checkItemToLocal(mTopics) != null) {
            mFavoriteButton.setText("Unfavorite");
        }
        hideTextViews();
        upperRatingBar.setRating(mTopics.rating);
        mConferenceTitleTextView.setText(mTopics.name);
        if (!mDisplayText) {
            mLlDate.setVisibility(View.GONE);
            mLlDay.setVisibility(View.GONE);
            mLlTime.setVisibility(View.GONE);
            mLlDescription.setVisibility(View.GONE);
            mLlLocation.setVisibility(View.GONE);
            mLlDuration.setVisibility(View.GONE);
        }
        mConferenceDateTextView.setText(PFABase.getDate(mTopics.startsAt));
        mConferenceDayTextView.setText(mTopics.day);
        mConferenceTimeTextView.setText(PFABase.getTime(mTopics.startsAt));
        mConferenceDescriptionTextView.setText(mTopics.description);
        mConferenceLocationTextView.setText(mTopics.location);
        mConferenceDurationTextView.setText(Integer.parseInt(mTopics.duration) / 60 + " minutes");
        mTvRate.setText("Rate this Topic");
        mSpeakerArrayList = new ArrayList<>();

        mRvSpeaker.setLayoutManager(new LinearLayoutManager(this));

        mSpeakerListRecyclerViewAdapter = new SpeakerListRecyclerViewAdapter(mContext, mSpeakerArrayList, ConferenceDetailsActivity.this);
        mRvSpeaker.setAdapter(mSpeakerListRecyclerViewAdapter);

        getSpeakerData();
    }


    private void hideTextViews() {

        if (mTopics != null) {
            setIfVisible(mLlDate, PFABase.getDate(mTopics.startsAt));
            setIfVisible(mLlDay, mTopics.day);
            setIfVisible(mLlTime, PFABase.getTime(mTopics.startsAt));
            setIfVisible(mLlDescription, mTopics.description);
            setIfVisible(mLlLocation, mTopics.location);
            setIfVisible(mLlDuration, Integer.parseInt(mTopics.duration) / 60 + " minutes");
            return;
        }
//        setIfVisible(mConferenceDateTextView, PFABase.getDate(mTopicLocal.startsAt));
//        setIfVisible(mConferenceDayTextView, mTopicLocal.day);
//        setIfVisible(mConferenceTimeTextView, PFABase.getTime(mTopicLocal.startsAt));
//        setIfVisible(mConferenceDescriptionTextView, mTopicLocal.description);
//        setIfVisible(mConferenceLocationTextView, mTopicLocal.location);
//        setIfVisible(mConferenceDurationTextView, Integer.parseInt(mTopicLocal.duration) / 60 + " minutes");
    }


    private void setIfVisible(LinearLayout linearLayout, String text) {
        try {
            linearLayout.setVisibility(text.equals("") ? View.GONE : View.VISIBLE);
        } catch (Exception e) {
            linearLayout.setVisibility(View.GONE);
        }
    }


    private void getSpeakerData() {

        for (Speaker speaker : mTopics.speakersData.speakers) {
            if (speaker.role.toLowerCase().equals("speaker")) {
                mSpeakerArrayList.add(speaker);
            }
        }

        mTvSpeaker.setVisibility(mSpeakerArrayList.size() == 0 ? View.GONE : View.VISIBLE);
        mSpeakerListRecyclerViewAdapter.changeData(mSpeakerArrayList);

    }


    private Integer checkItemToLocal(Topics topics) {

        mTopicLocalArrayList = new ArrayList<>(DataUtil.getTopicLocals(mConference_local_tblDao, mUser.id, mUser.role));
//        TopicLocal topicLocalAdd = new TopicLocal(topics);

        Integer isExistedToLocal = DataUtil.checkIfExist(mTopicLocalArrayList, topics);

        return isExistedToLocal;
    }

//    private Integer checkItemToLocal(TopicLocal topicLocal) {
//
//        mTopicLocalArrayList = new ArrayList<>(DataUtil.getTopicLocals(mConference_local_tblDao));
//        Integer isExistedToLocal = DataUtil.checkIfExist(mTopicLocalArrayList, topicLocal);
//
//        return isExistedToLocal;
//    }


    @Override
    protected void onResume() {
        super.onResume();
        PFABase.hideSoftKeyboard(ConferenceDetailsActivity.this);
    }

    private void saveString(String saveString) {

        mConference_local_tbl = new conference_local_tbl(saveString);
        Log.i("tag", "results :" + saveString);
        mConference_local_tblDao.insert(mConference_local_tbl);
        mFavoriteButton.setText("Unfavorite");
//        Toast.makeText(mContext, "Saved to Favorites", Toast.LENGTH_SHORT).show();
    }

    private void deleteFavorite(int index) {
        ArrayList<Long> ids = DataUtil.getTopicIds(mConference_local_tblDao);

        mConference_local_tbl = mConference_local_tblDao.load(ids.get(index));
        mConference_local_tblDao.delete(mConference_local_tbl);
        mFavoriteButton.setText("Favorite");
    }


    private Topics getDataLocal() {

        mTopicLocalArrayList = new ArrayList<>(DataUtil.getTopicLocals(mConference_local_tblDao, mUser.id, mUser.role));
        Topics topicLocalToReturn = new Topics();
        for (Topics topics : mTopicLocalArrayList) {
            if (topics.id == Integer.parseInt(conferenceIdStr)) {
                topicLocalToReturn = topics;
            }
        }
        return topicLocalToReturn;
    }


    private void getData() {

        final Dialog dialog = PFABase.loadDefaultProgressDialogCancellable(mContext, this);
        dialog.show();


        ConferenceInterface conferenceInterface = KitchenRestClient.create(mContext, ConferenceInterface.class, true);
        Call<DataWrapper<Topics>> mCallTopicResponse = conferenceInterface.getConferenceDetails(conferenceIdStr);
        mCallTopicResponse.enqueue(new Callback<DataWrapper<Topics>>() {
            @Override
            public void onResponse(Call<DataWrapper<Topics>> call, Response<DataWrapper<Topics>> response) {

                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                setFields(response.body().getData());
            }

            @Override
            public void onFailure(Call<DataWrapper<Topics>> call, Throwable t) {
                dialog.dismiss();
//                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                PFABase.dialogWithOnClick(mContext, "No Internet connection, No data to display", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        });


    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_conference_details;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.menuItemDownloadConference:
                // download conference here
                Intent intent = SendEmailDisplayActivity.newIntent(mContext, "", "Visitor", conferenceIdStr);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.conference, menu);
        return true;
    }

    private RatingListener ratingListener = new RatingListener() {
        @Override
        public void onRatePicked(ProperRatingBar ratingBar) {
//            Toast.makeText(mContext, "" + ratingBar.getRating(), Toast.LENGTH_SHORT).show();
            mRatingBody = new RatingBody(ratingBar.getRating());

        }
    };

    private void rateFunction() {

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();

        ConferenceInterface conferenceInterface = KitchenRestClient.create(mContext, ConferenceInterface.class, true);

        mPostRateCall = conferenceInterface.postConferenceRating(conferenceIdStr, mRatingBody);
        mPostRateCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
//                        try {
                    upperRatingBar.setRating(mTopics.rating);
//                        } catch (Exception e) {
//                            upperRatingBar.setRating(mTopicLocal.rating);
//                        }
                    return;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                try {
                    upperRatingBar.setRating(mTopics.rating);
                } catch (Exception e) {
                    upperRatingBar.setRating(0);
                }
            }
        });

    }


    @Override
    public void intentSpeakerDetails(String id) {
        startActivity(SpeakerDetailsActivity.newIntent(mContext, id, false));
    }

    @OnClick({R.id.favoriteButton, R.id.tvCommentSubmit, R.id.tvRateSubmit, R.id.fabNotes, R.id.createQuestion})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.favoriteButton:


                mTopicLocalArrayList = new ArrayList<>(DataUtil.getTopicLocals(mConference_local_tblDao, mUser.id, mUser.role));
                TopicLocal topicLocalAdd;
//                if (mTopics != null) {
//                    topicLocalAdd = new TopicLocal(mTopics);
//                } else {
//                    topicLocalAdd = mTopicLocal;
//                }

                Integer isExistedToLocal = DataUtil.checkIfExist(mTopicLocalArrayList, mTopics);


                if (isExistedToLocal == null) {
//                    mTopicLocalArrayList.add(mTopics);
////                    Add to favorites
//
//                    JsonArray jsonArraySub = new JsonArray();
//                    JsonArray jsonArraySpeaker = new JsonArray();
////                        For SubTopic
//                    for (SubTopicFieldLocal subTopic : topicLocalAdd.subTopic.subTopics) {
//                        jsonArraySub.add(subTopic.getJsonObject());
//                    }
////                        For Speaker
//                    for (SpeakerLocal speaker : topicLocalAdd.mSpeakerLocalData.speakers) {
//                        jsonArraySpeaker.add(speaker.getData());
//                    }
//                    SpeakerLocalData speakerLocalData = new SpeakerLocalData(jsonArraySpeaker);
//                    SubTopicsLocal subTopicsLocal = new SubTopicsLocal(jsonArraySub);

//                    saveString(topicLocalAdd.getJsonObject(subTopicsLocal.getData(), speakerLocalData.getData()).toString());

                    mTopics.userId = mUser.id;
                    mTopics.userRole = mUser.role;
                    saveString(ModelUtil.toJsonString(mTopics));

                    return;
                }
                deleteFavorite(isExistedToLocal);

                break;

            case R.id.createQuestion:
                int idToPass;
                try {
                    idToPass = Integer.parseInt(conferenceIdStr);
                } catch (Exception e) {
                    idToPass = 0;
                }

                startActivity(QuestionDisplayActivity.newIntent(mContext, idToPass, "Conference"));

                break;

            case R.id.tvRateSubmit:
                rateFunction();
                break;

            case R.id.tvCommentSubmit:
                final String comment = mEtComment.getText().toString();
                if (comment.equals("")) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up first the comment box.");
                    return;
                }
                sendComment(comment);
                break;

            case R.id.fabNotes:

                startActivity(NotesActivity.newIntent(mContext,
                        mTopics.id, mTopics.name, "Conference"));

                break;
        }
    }


    private void sendComment(String comment) {

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();

        ConferenceInterface conferenceInterface = KitchenRestClient.create(mContext, ConferenceInterface.class, true);
        CommentBody commentBody = new CommentBody(comment);

        Call<ResponseBody> commentRequest = conferenceInterface.postConferenceComment(conferenceIdStr, commentBody);
        commentRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }
                mEtComment.setText("");
                UiUtil.showMessageDialog(getSupportFragmentManager(), "Comment Sent");


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
            }
        });

    }


}
