package ph.coreproc.android.pfa.rest.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 3/31/2017.
 */

public class DataWrapper<T> implements Serializable{

    @SerializedName("data")
    @Expose
    private T data;

    public T getData() {
        return data;
    }
}
