package ph.coreproc.android.pfa.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Country;
import ph.coreproc.android.pfa.models.Region;
import ph.coreproc.android.pfa.models.Registration;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.rest.UserInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import ph.coreproc.android.pfa.utils.UiUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 7/11/2017.
 */

public class RegistrationActivityNew extends BaseActivity {

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, RegistrationActivityNew.class);
        return intent;
    }

    @Bind(R.id.etEmail)
    EditText mEtEmail;

    @Bind(R.id.etPassword)
    EditText mEtPassword;

    @Bind(R.id.etFullname)
    EditText mEtFullname;

    @Bind(R.id.etCountry)
    EditText mEtCountry;

    @Bind(R.id.tvRegionAsterisk)
    TextView mTvRegionAsterisk;

    @Bind(R.id.etRegion)
    EditText mEtRegion;

    @Bind(R.id.etAreasOfInterest)
    EditText mEtAreasOfInterest;

    @Bind(R.id.etLookingForFranchise)
    EditText mEtLookingForFranchise;

    @Bind(R.id.btnCancel)
    Button mBtnCancel;

    @Bind(R.id.btnSignUp)
    Button mBtnSignUp;


    private Country mCountry;
    private EventBus mEventBus = EventBus.getDefault();

    public String mPasswordStr = "", mEmailStr = "";

    public String mFullnameStr = "", mAgeRangeStr = "", mGenderStr = "";


    public String mCountryStr = "", mRegionStr = "", mMobileNoStr = "";

    public String mCompanyStr = "", mPhoneStr = "", mPositionTitleStr = "", mOccupationStr = "";

    public String mHowDidYouLearnStr = "", mHowDidYouLearnOthersStr = "", mLookingForFranchiseStr = "",
            mAreaOfInterestStr = "", mPurposeOfVisitStr = "", mPurposeVisitOthersStr = "", mAreYouAnExistingStr = "";
    public Boolean mRepeatVisitBool, mAreYouInterestedBool;

    private boolean mCountryCheck;

    private final String[] mAreaOfInterestPicker = {"Appliances / Suppliers / Equipments", "Bank & Finance", "Beauty & Lifestyle Concepts",
            "Education / Training & Related Services", "Food & Beverages", "Franchise / Legal Consultancy", "Health & Wellness", "International Brands / Concepts",
            "IT / Telecom", "Petro Business & Related Products & Services", "Retail Concepts", "Tourism / Hospitality"};

    private final String[] mLookingForFranchisePicker = {"5 M and above", "3 M - 5 M", "1.5 M - 3 M", "500 K - 1.5 M", "500 K and Below"};

    @Override
    public void onStart() {
        super.onStart();
        mEventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mEventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(Region region) {
        Log.i("daan", "daan");
        mEtRegion.setText(region.name);
        mRegionStr = region.id;
        mEventBus.removeStickyEvent(region);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(Country country) {
        mCountry = country;
        Log.i("daan", "daan_county");
        mEtCountry.setText(mCountry.name);
        mCountryStr = mCountry.id;
        mEventBus.removeStickyEvent(mCountry);
//        Check if Philippines
        boolean counrtyCheck = mCountry.id.equals("174");
        mEtRegion.setEnabled(counrtyCheck);
        mEtRegion.setAlpha(counrtyCheck ? (float) 1.0 : (float) 0.4);
        mTvRegionAsterisk.setVisibility(counrtyCheck ? View.VISIBLE : View.GONE);
        if (!counrtyCheck) {
            mEtRegion.setText("");
            mRegionStr = "";
        }

        mEventBus.removeStickyEvent(mCountry);
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_registration_latest;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        mCountry = new Country("174", "Philippines", "PH");
        initialize();
    }

    private void initialize() {

        mEtCountry.setText(mCountry.name);
        mCountryStr = mCountry.id;

        mEtCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(CountryDisplayActivity.newIntent(mContext));
            }
        });

        mEtRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(RegionDisplayActivity.newIntent(mContext));
            }
        });

        mEtAreasOfInterest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(mContext, "Areas of Interest", mAreaOfInterestPicker, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEtAreasOfInterest.setText(mAreaOfInterestPicker[which]);
                        mAreaOfInterestStr = mAreaOfInterestPicker[which];
                    }
                }).show();
            }
        });

        mEtLookingForFranchise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(mContext, "Choose Franchise", mLookingForFranchisePicker, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        mEtLookingForFranchise.setText(mLookingForFranchisePicker[which]);
                        mLookingForFranchiseStr = mLookingForFranchisePicker[which];
                    }
                }).show();
            }
        });

    }


    @OnClick({R.id.btnCancel, R.id.btnSignUp})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCancel:
                finish();
                break;
            case R.id.btnSignUp:

                mEmailStr = mEtEmail.getText().toString();
                mPasswordStr = mEtPassword.getText().toString();
                mFullnameStr = mEtFullname.getText().toString();

                if (mEmailStr.trim().equals("")) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your email.");
                    return;
                }

                if (!PFABase.isEmailValid(mEmailStr.trim())) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Wrong format of email.");
                    return;
                }

                if (mPasswordStr.length() < 8) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Password length is minimum of 8.");
                    return;
                }

                if (mFullnameStr.equals("")) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your name.");
                    return;
                }

                if (mCountryStr.equals("")) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your Country.");
                    return;
                }

//                        Check if country is Philippines
                mCountryCheck = mCountryStr.equals("174");

                if (mCountryCheck) {
                    if (mRegionStr.equals("")) {
                        UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your region.");
                        return;
                    }
                }

                if (mAreaOfInterestStr.equals("")) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your area of interest.");
                    return;
                }

                if (mLookingForFranchiseStr.equals("")) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your desired franchise package.");
                    return;
                }

                mFullnameStr = PFABase.removeEndSpacing(mFullnameStr);
                mEmailStr = PFABase.removeEndSpacing(mEmailStr.trim());
                mCompanyStr = PFABase.removeEndSpacing(mCompanyStr);

                Registration registration = new Registration(mFullnameStr, mEmailStr, mPasswordStr, mRegionStr.equals("") ? "     " : mRegionStr,
                        mGenderStr.toLowerCase(), mCompanyStr, mMobileNoStr, mPhoneStr,
                        mAgeRangeStr, mOccupationStr, mPositionTitleStr, mPurposeOfVisitStr, mPurposeVisitOthersStr,
                        mAreaOfInterestStr, mLookingForFranchiseStr, mAreYouAnExistingStr, mRepeatVisitBool, mAreYouInterestedBool,
                        mHowDidYouLearnStr, mHowDidYouLearnOthersStr);

                final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
                dialog.show();

                UserInterface userInterface = KitchenRestClient.create(mContext, UserInterface.class, false);
                Call<DataWrapper<User>> userCall = userInterface.registerUser(getString(R.string.register_url), registration);

                userCall.enqueue(new Callback<DataWrapper<User>>() {
                    @Override
                    public void onResponse(Call<DataWrapper<User>> call, Response<DataWrapper<User>> response) {
                        dialog.dismiss();
                        if (!response.isSuccessful()) {
                            APIError error = ErrorUtil.parsingError(response);
                            KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                            return;
                        }
                        Preferences.setString(mContext, Preferences.API_KEY, "" + response.headers().get("X-Authorization"));
                        Preferences.setString(mContext, Prefs.USER_ID, "" + response.body().getData().id);

                        //                Json Users
                        User user = response.body().getData();
//                        JsonObject jsonObject1 = new JsonObject();
//                        jsonObject1.addProperty("id", user.id);
//                        jsonObject1.addProperty("full_name", user.fullName);
//                        jsonObject1.addProperty("email", user.email);
//                        jsonObject1.addProperty("gender", user.gender);
//                        jsonObject1.addProperty("company", user.company);
//                        jsonObject1.addProperty("position", user.position);
//                        jsonObject1.addProperty("address", user.address);
//                        jsonObject1.addProperty("region_id", user.regionId);
//                        jsonObject1.addProperty("mobile", user.mobile);
//                        jsonObject1.addProperty("phone", user.phone);
//                        jsonObject1.addProperty("qr_code", user.qrCode);

                        Log.i("tag", "userJson: " + ModelUtil.toJsonString(user));

                        Preferences.setString(mContext, Prefs.USER, ModelUtil.toJsonString(user));
//               -------------------------------------------------------- //

                        ActivityCompat.finishAffinity((Activity) mContext);
                        startActivity(DashBoardActivity.newIntent(mContext).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();

                    }

                    @Override
                    public void onFailure(Call<DataWrapper<User>> call, Throwable t) {
                        dialog.dismiss();
                        KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                    }
                });

                break;
        }
    }
}
