package ph.coreproc.android.pfa.fragments.webviewfragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.io.IOException;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;

/**
 * Created by IanBlanco on 3/28/2017.
 */

public class WebViewFragmentTwo extends Fragment {

    @Bind(R.id.wvDisplay)
    WebView mWvDisplay;

    @Bind(R.id.progressBar)
    ProgressBar mProgressBar;

    public static WebViewFragmentTwo newInstance() {
        WebViewFragmentTwo webViewFragmentTwo = new WebViewFragmentTwo();

        return webViewFragmentTwo;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.web_view_layout, container, false);

        ButterKnife.bind(this, view);

        initialize();

        return view;
    }


    private void initialize() {
        setWebView();
    }

    private void setWebView() {
        mWvDisplay.getSettings().setJavaScriptEnabled(true);
        mWvDisplay.getSettings().setBuiltInZoomControls(true);
        mWvDisplay.getSettings().setSupportZoom(true);
        mWvDisplay.getSettings().setDisplayZoomControls(false);
        mWvDisplay.setInitialScale(30);
        mWvDisplay.setWebViewClient(new MapWebClient());
        getData();
    }


    private void getData() {
        Log.d("asdasd", "entered again");
        mProgressBar.setVisibility(View.GONE);
//        storeArrayList = new ArrayList<>();
//        Ion.with(mContext)
//                .load("http://malltest.staging.ph/test.json")
//                .asJsonObject()
//                .setCallback(new FutureCallback<JsonObject>() {
//                    @Override
//                    public void onCompleted(Exception e, JsonObject result) {
//                        // do stuff with the result or error
//                        progressBar.setVisibility(View.GONE);
//                        storeArrayList = new ArrayList<>(new Stores(result).stores);
//                        javaScriptInterface = new JavaScriptInterface(mContext);
//                        wv.addJavascriptInterface(javaScriptInterface, "JSInterface");
//        WebViewFragmentOne.JavaScriptInterface javaScriptInterface = new WebViewFragmentOne.JavaScriptInterface(getActivity());
//        mWvDisplay.addJavascriptInterface(javaScriptInterface, "JSInterface");
        String newHtml = htmlFile();
        mWvDisplay.loadDataWithBaseURL("file:///android_asset/mapsecondfloor/", newHtml, "text/html", "utf-8", "");
        htmlFile();
//                    }
//                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public class MapWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            mProgressBar.setVisibility(View.VISIBLE);

        }

        @Override
        public void onPageFinished(WebView view, String url) {

            try {
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(View.GONE);
            }catch (Exception e) {
//                Do nothing
            }

        }
    }


    private String htmlFile() {
        InputStream is = null;
        byte[] buffer = null;
        try {
            is = getActivity().getAssets().open("mapsecondfloor/pfa_map_second_floor.html");

            int size = is.available();

            buffer = new byte[size];
            is.read(buffer);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String str = new String(buffer);

        return replaceStoresText(str);
    }

    private String replaceStoresText(String html) {

//        for (int i = 0; i < storeArrayList.size(); i++) {
//            Store store = storeArrayList.get(i);
//            String storeDefault = "store" + store.id;
//
//            html = html.replace(storeDefault, store.name);
//        }

        return html;

    }

}
