package ph.coreproc.android.pfa.models.offlinemodels;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Speaker;
import ph.coreproc.android.pfa.models.SubTopic;
import ph.coreproc.android.pfa.models.Topics;


public class TopicLocal implements Serializable {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("day")
    @Expose
    public String day;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("location")
    @Expose
    public String location;

    @SerializedName("starts_at")
    @Expose
    public String startsAt;

    @SerializedName("ends_at")
    @Expose
    public String endsAt;

    @SerializedName("duration")
    @Expose
    public String duration;

    @SerializedName("subtopics")
    @Expose
    public SubTopicsLocal subTopic;

    @SerializedName("speakers")
    @Expose
    public SpeakerLocalData mSpeakerLocalData;

    @SerializedName("rating")
    @Expose
    public int rating;


    public TopicLocal(){

    }

    public TopicLocal(Topics topic){
        this.id = topic.id;
        this.name = topic.name;
        this.day = topic.day;
        this.rating = topic.rating;
        this.description = topic.description;
        this.location = topic.location;
        this.duration = topic.duration;
        this.startsAt = topic.startsAt;
        this.endsAt = topic.endsAt;

//        For SubTopics
        JsonArray jsonArray = new JsonArray();
        ArrayList<SubTopicFieldLocal> subTopicFieldArr = new ArrayList<>();
        for (SubTopic.SubTopicField subTopic : topic.subTopic.subTopics){
            SubTopicFieldLocal subTopicField = new SubTopicFieldLocal(subTopic);
            subTopicFieldArr.add(subTopicField);
            jsonArray.add(subTopicField.getJsonObject());
        }
        this.subTopic = new SubTopicsLocal(jsonArray);
        this.subTopic.subTopics = subTopicFieldArr;
//         For Speakers
        JsonArray jsonArray1 = new JsonArray();
        ArrayList<SpeakerLocal> speakerLocalArrayList = new ArrayList<>();
        try {
            for (Speaker speaker : topic.speakersData.speakers) {
                SpeakerLocal speakerLocal = new SpeakerLocal(speaker);
                speakerLocalArrayList.add(speakerLocal);
                jsonArray1.add(speakerLocal.getData());
            }
        }catch (Exception e){

        }
        this.mSpeakerLocalData = new SpeakerLocalData(jsonArray1);
        this.mSpeakerLocalData.speakers = speakerLocalArrayList;
    }



    public JsonObject getJsonObject(JsonObject subTopic, JsonObject speaker) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", this.id);
        jsonObject.addProperty("name", this.name);
        jsonObject.addProperty("day", this.day);
        jsonObject.addProperty("description", this.description);
        jsonObject.addProperty("location", this.location);
        jsonObject.addProperty("starts_at", this.startsAt);
        jsonObject.addProperty("ends_at", this.endsAt);
        jsonObject.addProperty("rating", this.rating);
        jsonObject.addProperty("duration", this.duration);
        jsonObject.add("subtopics", subTopic);
        jsonObject.add("speakers", speaker);

        return jsonObject;
    }

    public JsonObject getJsonObject() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", this.id);
        jsonObject.addProperty("name", this.name);
        jsonObject.addProperty("day", this.day);
        jsonObject.addProperty("description", this.description);
        jsonObject.addProperty("location", this.location);
        jsonObject.addProperty("starts_at", this.startsAt);
        jsonObject.addProperty("ends_at", this.endsAt);
        jsonObject.addProperty("rating", this.rating);
        jsonObject.addProperty("duration", this.duration);
//        jsonObject.add("subtopics", subTopic);
//        jsonObject.add("speakers", search_item);

        return jsonObject;
    }



    public View getView(Context context, View.OnClickListener onClickListener, boolean isLast) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_topic_speaker, null);
        RelativeLayout rlContainer = (RelativeLayout) view.findViewById(R.id.rlContainer);
        TextView tvConferenceNameTextView = (TextView) view.findViewById(R.id.conferenceNameTextView);
        TextView tvConferenceDescriptionTextView = (TextView) view.findViewById(R.id.conferenceDescriptionTextView);
        ImageView ivProceed = (ImageView) view.findViewById(R.id.proceed);


        if (isLast){
            rlContainer.setBackground(context.getDrawable(R.drawable.footer_background));
        }
        ivProceed.setOnClickListener(onClickListener);

        tvConferenceNameTextView.setText(name);
        tvConferenceDescriptionTextView.setText(description);

        return view;
    }


}