package ph.coreproc.android.pfa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.SpeakersListActivity;
import ph.coreproc.android.pfa.models.Speaker;

/**
 * Created by willm on 3/29/2017.
 */

public class SpeakersRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Speaker> items;
    private ArrayList<Speaker> itemsCopy;
    private Context mContext;

    public interface Callback extends DisplayNoDataInterface {
        void intentToDetails(int id);

    }

    private Callback mCallback;

    private SpeakersListActivity mSpeakersListActivity;

    public SpeakersRecyclerViewAdapter(Context context, ArrayList<Speaker> items, Callback callback, SpeakersListActivity speakersListActivity) {
        mContext = context;
        this.itemsCopy = items;
        this.items = items;
        this.mSpeakersListActivity = speakersListActivity;
        arrangeArray();
        this.mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_speaker_item, parent, false);
        SpeakersRecyclerViewAdapter.SpeakerViewHolder speakerViewHolder = new SpeakersRecyclerViewAdapter.SpeakerViewHolder(view);
        return speakerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final SpeakersRecyclerViewAdapter.SpeakerViewHolder speakerViewHolder = (SpeakersRecyclerViewAdapter.SpeakerViewHolder) holder;
        final Speaker speaker = items.get(position);
        speakerViewHolder.speakerNameTextView.setText(speaker.speakerName.replace("MR.", "").replace("MS.", ""));

//        if (speaker.avatar != null) {
            Glide.with(mContext)
                    .load(speaker.avatar)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Log.d("error loading image", "error" + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            speakerViewHolder.speakerImageView.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(speakerViewHolder.speakerImageView);
//        }


        speakerViewHolder.llSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCallback.intentToDetails(speaker.id);
            }
        });

    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults onReturn = new FilterResults();
                final ArrayList<Speaker> results = new ArrayList<>();
                items = new ArrayList<>(itemsCopy);

                if (constraint != null) {
                    if (items.size() > 0) {
                        for (final Speaker speaker : items) {
                        if (speaker.speakerName.replace("MR.", "").replace("MS.", "").toLowerCase().contains(constraint.toString().toLowerCase()))

                            results.add(speaker);
                        }
                    }
                    onReturn.values = results;
                }

                return onReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                items = (ArrayList<Speaker>) results.values;

                displayNoData(items);
                arrangeArray();
                notifyDataSetChanged();
            }
        };
    }

    public void changeData(ArrayList<Speaker> items) {
        this.items = new ArrayList<>(items);
        this.itemsCopy = new ArrayList<>(items);
        displayNoData(this.items);
//        arrangeArray();
//        notifyItemRangeInserted(previousDataSize, items.size());
        notifyDataSetChanged();
    }

    public void add(ArrayList<Speaker> items) {
        int previousDataSize = this.items.size();
        this.items.addAll(items);
        displayNoData(this.items);
        arrangeArray();
//        notifyItemRangeInserted(previousDataSize, items.size());
        notifyDataSetChanged();
    }

    public void arrangeArray() {
//        Collections.sort(items, new Comparator<Speaker>() {
//            @Override
//            public int compare(Speaker o1, Speaker o2) {
//                return o1.speakerName.replace("MR.", "").replace("MS.", "").compareTo(o2.speakerName.replace("MR.", "").replace("MS.", ""));
//            }
//        });
    }

    private void displayNoData(ArrayList<Speaker> items) {

        if (items.size() > 0 == false) {
            mCallback.displayNoDataText();
            return;
        }
        mCallback.removeNoDataText();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class SpeakerViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.speakerImageView)
        ImageView speakerImageView;

        @Bind(R.id.speakerNameTextView)
        TextView speakerNameTextView;

        @Bind(R.id.llSpeaker)
        LinearLayout llSpeaker;

        public SpeakerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}