package ph.coreproc.android.pfa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import ph.coreproc.android.pfa.models.offlinemodels.OfflineExtension;

/**
 * Created by willm on 3/29/2017.
 */

public class Speaker extends OfflineExtension implements Serializable {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String speakerName;

    @SerializedName("title")
    @Expose
    public String speakerTitle;

    @SerializedName("company")
    @Expose
    public String speakerCompany;

    @SerializedName("website")
    @Expose
    public String website;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("avatar")
    @Expose
    public String avatar;

    @SerializedName("rating")
    @Expose
    public int rating;

    @SerializedName("role")
    @Expose
    public String role;

    @SerializedName("topics")
    @Expose
    public SpeakerTopic speakerTopic;



    public class SpeakerTopic implements Serializable {

        @SerializedName("data")
        @Expose
        public ArrayList<Topics> speakerTopics;

    }
}