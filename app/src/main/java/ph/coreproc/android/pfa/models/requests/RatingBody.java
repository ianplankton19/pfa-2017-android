package ph.coreproc.android.pfa.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 4/18/2017.
 */

public class RatingBody implements Serializable {

    public RatingBody(int comment) {
        this.rating = comment;
    }

    @SerializedName("rating")
    @Expose
    public int rating;

}
