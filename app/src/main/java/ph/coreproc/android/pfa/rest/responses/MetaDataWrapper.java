package ph.coreproc.android.pfa.rest.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 4/24/2017.
 */

public class MetaDataWrapper <T> extends DataWrapper<T> implements Serializable{

    @SerializedName("meta")
    @Expose
    private Meta meta;
    public Meta getMeta() {
        return meta;
    }
    public class Meta {
        @SerializedName("pagination")
        @Expose
        private Pagination pagination;
        public Pagination getPagination() {
            return pagination;
        }
    }
    public class Pagination implements Serializable {
        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("count")
        @Expose
        private String count;
        @SerializedName("per_page")
        @Expose
        private String perPage;
        @SerializedName("current_page")
        @Expose
        private int currentPage;
        @SerializedName("total_pages")
        @Expose
        private int totalPages;
        public String getTotal() {
            return total;
        }
        public String getCount() {
            return count;
        }
        public String getPerPage() {
            return perPage;
        }
        public int getCurrentPage() {
            return currentPage;
        }
        public int getTotalPages() {
            return totalPages;
        }
    }

}
