package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;
import com.ivankocijan.magicviews.views.MagicTextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.techery.properratingbar.ProperRatingBar;
import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.NxtGen;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.rest.NxtGenInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.TextViewEx;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 4/26/2017.
 */

public class NxtGenDetailsActivity extends BaseActivity {


    @Bind(R.id.exhibitorBoothNumberTextView)
    TextView mExhibitorBoothNumberTextView;

    @Bind(R.id.exhibitorContactPerson)
    TextView mExhibitorContactPerson;

    @Bind(R.id.exhibitorSector)
    TextView mExhibitorSector;

    @Bind(R.id.exhibitorContactTextView)
    TextView mExhibitorContactTextView;

    @Bind(R.id.exhibitorDesignationTextView)
    TextView mExhibitorDesignationTextView;

    @Bind(R.id.exhibitorWebsiteTextView)
    TextView mExhibitorWebsiteTextView;

    @Bind(R.id.exhibitorEmailTextView)
    TextView mExhibitorEmailTextView;

    @Bind(R.id.exhibitorBusinessLineTextView)
    TextView mExhibitorBusinessLineTextView;

    @Bind(R.id.exhibitorFranchisePackageTextView)
    TextView mExhibitorFranchisePackageTextView;

    @Bind(R.id.exhibitorFranchiseFeeTextView)
    TextView mExhibitorFranchiseFeeTextView;

    @Bind(R.id.exhibitorRoiPeriodTextView)
    TextView mExhibitorRoiPeriodTextView;

    @Bind(R.id.exhibitorTermOfFranchiseTextView)
    TextView mExhibitorTermOfFranchiseTextView;

    @Bind(R.id.exhibitorRenewalTextView)
    TextView mExhibitorRenewalTextView;

    @Bind(R.id.exhibitorFranchiseTextView)
    TextViewEx mExhibitorFranchiseTextView;

    @Bind(R.id.tvRate)
    TextView mTvRate;

    @Bind(R.id.upperRatingBar)
    ProperRatingBar mUpperRatingBar;

    @Bind(R.id.favoriteButton)
    Button mFavoriteButton;

    @Bind(R.id.etComment)
    EditText mEtComment;

    @Bind(R.id.submitButton)
    Button mSubmitButton;

    @Bind(R.id.createQuestion)
    Button mCreateQuestion;

    @Bind(R.id.llFooter)
    LinearLayout mLlFooter;

    @Bind(R.id.rlExhibitorDetailsContent)
    RelativeLayout mRlExhibitorDetailsContent;

    @Bind(R.id.ivFooter)
    ImageView mIvFooter;

    @Bind(R.id.exhibitorImageView)
    ImageView mExhibitorImageView;

    @Bind(R.id.exhibitorNameTextView)
    MagicTextView mExhibitorNameTextView;

    @Bind(R.id.exhibitorCompanyTextView)
    MagicTextView mExhibitorCompanyTextView;

    @Bind(R.id.exhibitorPositionTextView)
    MagicTextView mExhibitorPositionTextView;

    @Bind(R.id.rlProfileInfo)
    RelativeLayout mRlProfileInfo;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

//    @Bind(R.id.appbar)
//    AppBarLayout mAppbar;

    @Bind(R.id.exhibitorRoyaltyFeeTextView)
    TextView mExhibitorRoyaltyFeeTextView;

    @Bind(R.id.exhibitorAdvertisingFeeTextView)
    TextView mExhibitorAdvertisingFeeTextView;

    @Bind(R.id.exhibitorNoOfStoresTextView)
    TextView mExhibitorNoOfStoresTextView;

    @Bind(R.id.exhibitorCompanyOwnedTextView)
    TextView mExhibitorCompanyOwnedTextView;

    @Bind(R.id.exhibitorFranchiseStoresTextView)
    TextView mExhibitorFranchiseStoresTextView;

    @Bind(R.id.exhibitorFranchiseSinceTextView)
    TextView mExhibitorFranchiseSinceTextView;

    @Bind(R.id.exhibitorYearStartedTextView)
    TextView mExhibitorYearStartedTextView;

    @Bind(R.id.exhibitorInvestmentLevelTextView)
    TextView mExhibitorInvestmentLevelTextView;

    @Bind(R.id.exhibitorTotalInvestmentLevelTextView)
    TextView mExhibitorTotalInvestmentLevelTextView;

    @Bind(R.id.llSector)
    LinearLayout mLlSector;

    @Bind(R.id.llConactPerson)
    LinearLayout mLlConactPerson;

    @Bind(R.id.llContactNumber)
    LinearLayout mLlContactNumber;

    @Bind(R.id.llDesignation)
    LinearLayout mLlDesignation;

    @Bind(R.id.llWebsite)
    LinearLayout mLlWebsite;

    @Bind(R.id.llEmail)
    LinearLayout mLlEmail;

    @Bind(R.id.llBoothNumber)
    LinearLayout mLlBoothNumber;

    @Bind(R.id.llBusinessLine)
    LinearLayout mLlBusinessLine;

    @Bind(R.id.llFranchisePackage)
    LinearLayout mLlFranchisePackage;

    @Bind(R.id.llFranchiseFee)
    LinearLayout mLlFranchiseFee;

    @Bind(R.id.llRoiPeriod)
    LinearLayout mLlRoiPeriod;

    @Bind(R.id.llTermOfFranchise)
    LinearLayout mLlTermOfFranchise;

    @Bind(R.id.llRenewal)
    LinearLayout mLlRenewal;

    @Bind(R.id.llRoyaltyFee)
    LinearLayout mLlRoyaltyFee;

    @Bind(R.id.llAdvertisingFee)
    LinearLayout mLlAdvertisingFee;

    @Bind(R.id.llNoOfStores)
    LinearLayout mLlNoOfStores;

    @Bind(R.id.llCompanyOwned)
    LinearLayout mLlCompanyOwned;

    @Bind(R.id.llFranchiseStores)
    LinearLayout mLlFranchiseStores;

    @Bind(R.id.llFranchiseSince)
    LinearLayout mLlFranchiseSince;

    @Bind(R.id.llYearStarted)
    LinearLayout mLlYearStarted;

    @Bind(R.id.llInvestmentLevel)
    LinearLayout mLlInvestmentLevel;

    @Bind(R.id.llTotalInvestmentLevel)
    LinearLayout mLlTotalInvestmentLevel;

    public static String ARGS_ID = "ARGS_NAME";


    private NxtGen mNxtGen;

    public static Intent newIntent(Context context, String name) {
        Intent intent = new Intent(context, NxtGenDetailsActivity.class);
        intent.putExtra(ARGS_ID, name);
        return intent;
    }

    private String mId = "";


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_nxt_gen_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);

        initialize();
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();
        mId = extras.getString(ARGS_ID);
        mLlFooter.setVisibility(View.GONE);
        getData();
    }

    private void setUpFields(NxtGen nxtGen) {
        mRlExhibitorDetailsContent.setVisibility(View.VISIBLE);
        mNxtGen = nxtGen;
        hideTextViews();
        mExhibitorNameTextView.setText(nxtGen.name);
//        mExhibitorCompanyTextView.setText(nxtGen.name);
        mExhibitorBoothNumberTextView.setText(nxtGen.boothNumber);
        mExhibitorContactPerson.setText(nxtGen.contactPerson);
        mExhibitorSector.setText(nxtGen.sector);
        mExhibitorDesignationTextView.setText(nxtGen.designation);
        mExhibitorWebsiteTextView.setText(nxtGen.website);
        mExhibitorEmailTextView.setText(nxtGen.email);
        mExhibitorBusinessLineTextView.setText(nxtGen.businessLine);
        mExhibitorFranchisePackageTextView.setText(nxtGen.franchisePackage);
        mExhibitorFranchiseFeeTextView.setText(nxtGen.franchiseFee);
        mExhibitorRoiPeriodTextView.setText(nxtGen.roiPeriod);
        mExhibitorTermOfFranchiseTextView.setText(nxtGen.termOfFranchise);
        mExhibitorContactTextView.setText(nxtGen.contactNo);
        mExhibitorFranchiseTextView.setText(nxtGen.description);
        ((TextViewEx) mExhibitorFranchiseTextView).setText(mExhibitorFranchiseTextView.getText().toString(), true);
        mExhibitorRenewalTextView.setText(nxtGen.renewal);
        mExhibitorRoyaltyFeeTextView.setText(nxtGen.royaltyFee);
        mExhibitorAdvertisingFeeTextView.setText(nxtGen.advertisingFee);
        mExhibitorNoOfStoresTextView.setText(nxtGen.noOfStores);
        mExhibitorCompanyOwnedTextView.setText(nxtGen.companyOwned);
        mExhibitorFranchiseStoresTextView.setText(nxtGen.franchiseStores);
        mExhibitorFranchiseSinceTextView.setText(nxtGen.franchisingSince);
        mExhibitorYearStartedTextView.setText(nxtGen.yearStarted);
        mExhibitorInvestmentLevelTextView.setText(nxtGen.investmentLevel);
        mExhibitorTotalInvestmentLevelTextView.setText(nxtGen.totalInvestmentLevel);
        mTvRate.setText("Rate this Exhibitor");
        mUpperRatingBar.setRating(nxtGen.rating);
//        mAppbar.requestFocus();
        mExhibitorImageView.requestFocus();
//        mAppbar.setExpanded(true, true);
        setUpImage(nxtGen.avatar);

    }


    private void setUpImage(String url) {
        if (url != null) {
            Log.i("url", "url: " + url);
            Glide.with(mContext)
                    .load(url)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            Log.d("error loading image", "erro " + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            mExhibitorImageView.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(mExhibitorImageView);
        }
    }

    private void hideTextViews() {
        setIfVisible(mLlBoothNumber, mNxtGen.boothNumber);
        setIfVisible(mLlConactPerson, mNxtGen.contactPerson);
        setIfVisible(mLlSector, mNxtGen.sector);
        setIfVisible(mLlDesignation, mNxtGen.designation);
        setIfVisible(mLlWebsite, mNxtGen.website);
        setIfVisible(mLlEmail, mNxtGen.email);
        setIfVisible(mLlBusinessLine, mNxtGen.businessLine);
        setIfVisible(mLlFranchisePackage, mNxtGen.franchisePackage);
        setIfVisible(mLlFranchiseFee, mNxtGen.franchiseFee);
        setIfVisible(mLlRoiPeriod, mNxtGen.roiPeriod);
        setIfVisible(mLlTermOfFranchise, mNxtGen.termOfFranchise);
        setIfVisible(mLlContactNumber, mNxtGen.contactNo);
//        setIfVisible(mLlDes, mNxtGen.description);
        setIfVisible(mLlRenewal, mNxtGen.renewal);
        setIfVisible(mLlRoyaltyFee, mNxtGen.royaltyFee);
        setIfVisible(mLlAdvertisingFee, mNxtGen.advertisingFee);
        setIfVisible(mLlNoOfStores, mNxtGen.noOfStores);
        setIfVisible(mLlCompanyOwned, mNxtGen.companyOwned);
        setIfVisible(mLlFranchiseStores, mNxtGen.franchiseStores);
        setIfVisible(mLlFranchiseSince, mNxtGen.franchisingSince);
        setIfVisible(mLlYearStarted, mNxtGen.yearStarted);
        setIfVisible(mLlInvestmentLevel, mNxtGen.investmentLevel);
        setIfVisible(mLlTotalInvestmentLevel, mNxtGen.totalInvestmentLevel);
    }

    private void setIfVisible(LinearLayout linearLayout, String text) {
        try {
            linearLayout.setVisibility(text.equals("") ? View.GONE : View.VISIBLE);
        } catch (Exception e) {
            linearLayout.setVisibility(View.GONE);
        }
    }

    private void getData() {
        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();
        NxtGenInterface nxtGenInterface = KitchenRestClient.create(mContext, NxtGenInterface.class, true);
        Call<DataWrapper<NxtGen>> callNxtGen = nxtGenInterface.getNxtGenDetails(mId);
        callNxtGen.enqueue(new Callback<DataWrapper<NxtGen>>() {
            @Override
            public void onResponse(Call<DataWrapper<NxtGen>> call, Response<DataWrapper<NxtGen>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                setUpFields(response.body().getData());
            }

            @Override
            public void onFailure(Call<DataWrapper<NxtGen>> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global) + "\n" + t.getMessage());
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void voteFunction(String id) {
        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();
        NxtGenInterface nxtGenInterface = KitchenRestClient.create(mContext, NxtGenInterface.class, true);
        Call<ResponseBody> voteCall = nxtGenInterface.voteNxtGen(id);
        voteCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }
                KitchenUiUtils.showAlertDialog(mContext, "Vote sent");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global) + "\n" + t.getMessage());
            }
        });
    }

//    @OnClick(R.id.btnVoteNow)
//    public void onClick() {
//        voteFunction(mId);
//    }
}
