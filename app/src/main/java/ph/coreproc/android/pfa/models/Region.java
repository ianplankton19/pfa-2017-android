package ph.coreproc.android.pfa.models;

import com.google.gson.JsonObject;

import java.io.Serializable;

/**
 * Created by willm on 3/30/2017.
 */

public class Region implements Serializable {

    public String id;
    public String name;
    public String designation;
    public String countryId;

    public Region(JsonObject jsonObject) {
        this.id = jsonObject.get("id").getAsString();
        this.name = jsonObject.get("name").getAsString();
        this.designation = jsonObject.get("designation").getAsString();
        this.countryId = jsonObject.get("country_id").getAsString();
    }

}
