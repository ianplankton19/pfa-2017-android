package ph.coreproc.android.pfa.fragments.registrationfragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.RegistrationActivity;

/**
 * Created by IanBlanco on 4/19/2017.
 */

public class RegistrationFifthFragment extends Fragment {

    @Bind(R.id.etPurposeOfVisit)
    EditText mEtPurposeOfVisit;

    @Bind(R.id.etAreasOfInterest)
    EditText mEtAreasOfInterest;

    @Bind(R.id.etAreYouAnExisting)
    EditText mEtAreYouAnExisting;

    @Bind(R.id.etLookingForFranchise)
    EditText mEtLookingForFranchise;

    @Bind(R.id.etAreYouInterested)
    EditText mEtAreYouInterested;

    @Bind(R.id.etRepeatVisit)
    EditText mEtRepeatVisit;

    @Bind(R.id.etHowDidYouLearn)
    EditText mEtHowDidYouLearn;

    RegistrationActivity mRegistrationActivity;
    private ArrayList<String> mSelectedItemsResult = new ArrayList<>();
    public CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener;
    private DialogCheckView dialogCheckView;

    private int checkId = 0;

    private final String[] mAreaOfInterestPicker = {"Appliances / Suppliers / Equipments", "Bank & Finance", "Beauty & Lifestyle Concepts",
            "Education / Training & Related Services", "Food & Beverages", "Franchise / Legal Consultancy", "Health & Wellness", "International Brands / Concepts",
            "IT / Telecom", "Petro Business & Related Products & Services", "Retail Concepts", "Tourism / Hospitality"};

    private final String[] mPurposeOfVisitPicker = {"To look for franchise concept/s", "To find joint venture partner/s", "To look for supplier/s", "To look for new product/s",
            "To evaluate the show for future participation", "To look for internation concept/s", "Others(Please Specify)"};

    private final String[] mAreYouExistingPicker = {"Franchisor", "Franchisee", "Master Franchisee", "Area Developer", "None of the Above"};

    private final String[] mLookingForFranchisePicker = {"5 M and above", "3 M - 5 M", "1.5 M - 3 M", "500 K - 1.5 M", "500 K and Below"};

    private final String[] mHowDidYouLearnPicker = {"PFA Website", "Radio", "TV Ad", "Social Media", "Exhibitor", "Newspaper/ Magazine",
            "Billboards / Poster", "Email", "Word of Mouth", "SMS / Text", "Others(Please Specify)"};

    private final String[] mYesNo = {"Yes", "No"};

    public RegistrationFifthFragment() {
    }


    public static RegistrationFifthFragment newInstance() {
        RegistrationFifthFragment registrationFifthFragment = new RegistrationFifthFragment();

        return registrationFifthFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.registration_fifth_page_new, container, false);
        ButterKnife.bind(this, view);

        mRegistrationActivity = (RegistrationActivity) getActivity();

        initialize();

        return view;
    }

    private void initialize() {
        mEtAreasOfInterest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(getActivity(), "Areas of Interest", mAreaOfInterestPicker, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEtAreasOfInterest.setText(mAreaOfInterestPicker[which]);
                        mRegistrationActivity.mAreaOfInterestStr = mAreaOfInterestPicker[which];
                    }
                }).show();
            }
        });

        mEtPurposeOfVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPurposeOfVisitDialog();
            }
        });

        mEtAreYouAnExisting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(getActivity(), "Are you an Existing", mAreYouExistingPicker, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEtAreYouAnExisting.setText(mAreYouExistingPicker[which]);
                        mRegistrationActivity.mAreYouAnExistingStr = mAreYouExistingPicker[which];
                    }
                }).show();
            }
        });

        mEtAreYouInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(getActivity(), "", mYesNo, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEtAreYouInterested.setText(mYesNo[which]);
                        if (mYesNo[which].equals("Yes")) {
                            mRegistrationActivity.mAreYouInterestedBool = true;
                            return;
                        }
                        mRegistrationActivity.mAreYouInterestedBool = false;
                    }
                }).show();
            }
        });

        mEtRepeatVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(getActivity(), "", mYesNo, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEtRepeatVisit.setText(mYesNo[which]);
                        if (mYesNo[which].equals("Yes")) {
                            mRegistrationActivity.mRepeatVisitBool = true;
                            return;
                        }
                        mRegistrationActivity.mRepeatVisitBool = false;
                    }
                }).show();
            }
        });

        mEtLookingForFranchise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(getActivity(), "Choose Franchise", mLookingForFranchisePicker, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        mEtLookingForFranchise.setText(mLookingForFranchisePicker[which]);
                        mRegistrationActivity.mLookingForFranchiseStr = mLookingForFranchisePicker[which];
                    }
                }).show();
            }
        });

        mEtHowDidYouLearn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mSelectedItemsResult.size() > 0) {
//                    checkBoxChecker();
//                }
                showDialogPickerWithCheckBox();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void showPurposeOfVisitDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.purpose_visit_items, null);
        builder.setView(view);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.i("tag", "selected:" + mRegistrationActivity.mPurposeOfVisitStr);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        final DialogView dialogView = new DialogView(view);

        dialogView.mRgPurposeOfVisit.check(checkId);
        dialogView.mRgPurposeOfVisit.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkId = checkedId;
                switch (checkedId) {

                    case R.id.rbOthers:
                        dialogView.mEtOthers.setEnabled(true);
                        mRegistrationActivity.mPurposeOfVisitStr = "";
                        dialogView.mEtOthers.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                mRegistrationActivity.mPurposeVisitOthersStr = "" + s;

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                mEtPurposeOfVisit.setText(dialogView.mEtOthers.getText().toString());
                            }
                        });

                        break;

                    default:
                        dialogView.mEtOthers.setEnabled(false);
                        mRegistrationActivity.mPurposeVisitOthersStr = "";
                        int selectedId = dialogView.mRgPurposeOfVisit.getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) view.findViewById(selectedId);
                        mRegistrationActivity.mPurposeOfVisitStr = radioButton.getText().toString();
                        mEtPurposeOfVisit.setText(radioButton.getText().toString());

                }
            }
        });
    }


    private void showDialogPickerWithCheckBox() {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.expo_items, null);
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.i("Collections", "Collection checked:" + mSelectedItemsResult.toString());
                ArrayList<String> mSelectedItemResultCopy = new ArrayList<>(mSelectedItemsResult);
                try {
                    mSelectedItemResultCopy.remove(dialogCheckView.mCbOther.getText().toString());
                } catch (Exception e) {

                }
                String collections = mSelectedItemResultCopy.toString().replace("[", "").replace("]", "");
                mEtHowDidYouLearn.setText(collections + (dialogCheckView.mEtOthers.getText().toString().equals("")
                        ? "" : ", " +dialogCheckView.mEtOthers.getText().toString()));
                mRegistrationActivity.mHowDidYouLearnStr = collections;

                mRegistrationActivity.mHowDidYouLearnOthersStr = dialogCheckView.mEtOthers.getText().toString();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();

        dialogCheckView = new DialogCheckView(view);

        checkBoxChecker(dialogCheckView);


        dialogCheckView.mCbPfaWebsite.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbPfaWebsite, dialogCheckView));
        dialogCheckView.mCbSocialMedia.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbSocialMedia, dialogCheckView));
        dialogCheckView.mCbBillboards.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbBillboards, dialogCheckView));
        dialogCheckView.mCbSms.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbSms, dialogCheckView));
        dialogCheckView.mCbRadio.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbRadio, dialogCheckView));
        dialogCheckView.mCbExhibitor.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbExhibitor, dialogCheckView));
        dialogCheckView.mCbEmail.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbEmail, dialogCheckView));
        dialogCheckView.mCbTvAd.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbTvAd, dialogCheckView));
        dialogCheckView.mCbNewsPaper.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbNewsPaper, dialogCheckView));
        dialogCheckView.mCbWordOfMouth.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbWordOfMouth, dialogCheckView));
        dialogCheckView.mCbOther.setOnCheckedChangeListener(mOnCheckedChangeListenerMethod(dialogCheckView.mCbOther, dialogCheckView));


    }

    private void checkBoxChecker(DialogCheckView dialogCheckView) {
        ViewGroup viewGroup = dialogCheckView.mLlViewLayout;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View childView = viewGroup.getChildAt(i);
            if (childView instanceof CheckBox) {
                CheckBox cbItem = (CheckBox) childView;
                boolean check = mSelectedItemsResult.contains(cbItem.getText().toString());
                cbItem.setChecked(check);

//                For Enabling EditText Others
                boolean checkOthers = mSelectedItemsResult.contains(cbItem.getText().toString());
                dialogCheckView.mEtOthers.setEnabled(checkOthers);

            }
        }

    }


    static class DialogView {

        @Bind(R.id.rbToLookForFranchise)
        RadioButton mRbToLookForFranchise;

        @Bind(R.id.rbToFindJointVenture)
        RadioButton mRbToFindJointVenture;

        @Bind(R.id.rbToLookForSupplier)
        RadioButton mRbToLookForSupplier;

        @Bind(R.id.rbToLookForNewProd)
        RadioButton mRbToLookForNewProd;

        @Bind(R.id.rbToEvaluateTheShow)
        RadioButton mRbToEvaluateTheShow;

        @Bind(R.id.rbToLookForInternational)
        RadioButton mRbToLookForInternational;

        @Bind(R.id.rbOthers)
        RadioButton mRbOthers;

        @Bind(R.id.rgPurposeOfVisit)
        RadioGroup mRgPurposeOfVisit;

        @Bind(R.id.etOthers)
        EditText mEtOthers;

        public DialogView(View view) {
            ButterKnife.bind(this, view);
        }

    }

    static class DialogCheckView {

        @Bind(R.id.cbPfaWebsite)
        CheckBox mCbPfaWebsite;

        @Bind(R.id.cbSocialMedia)
        CheckBox mCbSocialMedia;

        @Bind(R.id.cbBillboards)
        CheckBox mCbBillboards;

        @Bind(R.id.cbSms)
        CheckBox mCbSms;

        @Bind(R.id.cbRadio)
        CheckBox mCbRadio;

        @Bind(R.id.cbExhibitor)
        CheckBox mCbExhibitor;

        @Bind(R.id.cbEmail)
        CheckBox mCbEmail;

        @Bind(R.id.cbTvAd)
        CheckBox mCbTvAd;

        @Bind(R.id.cbNewsPaper)
        CheckBox mCbNewsPaper;

        @Bind(R.id.cbWordOfMouth)
        CheckBox mCbWordOfMouth;

        @Bind(R.id.cbOther)
        CheckBox mCbOther;

        @Bind(R.id.etOthers)
        EditText mEtOthers;

        @Bind(R.id.llViewLayout)
        LinearLayout mLlViewLayout;

        public DialogCheckView(View view) {
            ButterKnife.bind(this, view);
        }

    }


    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListenerMethod(final CheckBox checkBox, final DialogCheckView dialogCheckView) {

        mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                switch (checkBox.getId()) {

                    case R.id.cbPfaWebsite:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbPfaWebsite.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbPfaWebsite.getText().toString());
                        break;

                    case R.id.cbSocialMedia:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbSocialMedia.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbSocialMedia.getText().toString());
                        break;

                    case R.id.cbBillboards:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbBillboards.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbBillboards.getText().toString());
                        break;

                    case R.id.cbSms:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbSms.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbSms.getText().toString());
                        break;

                    case R.id.cbRadio:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbRadio.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbRadio.getText().toString());
                        break;

                    case R.id.cbExhibitor:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbExhibitor.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbExhibitor.getText().toString());
                        break;

                    case R.id.cbEmail:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbEmail.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbEmail.getText().toString());
                        break;

                    case R.id.cbTvAd:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbTvAd.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbTvAd.getText().toString());
                        break;

                    case R.id.cbNewsPaper:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbNewsPaper.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbNewsPaper.getText().toString());
                        break;

                    case R.id.cbWordOfMouth:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbWordOfMouth.getText().toString());
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbWordOfMouth.getText().toString());
                        break;

                    case R.id.cbOther:
                        if (isChecked) {
                            mSelectedItemsResult.add(dialogCheckView.mCbOther.getText().toString());
                            dialogCheckView.mEtOthers.setEnabled(true);
                            return;
                        }
                        mSelectedItemsResult.remove(dialogCheckView.mCbOther.getText().toString());
                        dialogCheckView.mEtOthers.setEnabled(false);
                        break;

                }
            }


        };

        return mOnCheckedChangeListener;
    }

}
