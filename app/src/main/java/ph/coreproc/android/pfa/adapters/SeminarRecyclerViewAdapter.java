package ph.coreproc.android.pfa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Seminar;

/**
 * Created by willm on 3/28/2017.
 */

public class SeminarRecyclerViewAdapter extends RecyclerView.Adapter<SeminarRecyclerViewAdapter.SeminarViewHolder> {

    public interface Callback extends DisplayNoDataInterface {
        void itentToSeminarDetails(int id);
    }

    ArrayList<Seminar> items;
    Context mContext;

    Callback mCallback;

    public SeminarRecyclerViewAdapter(Context context, ArrayList<Seminar> items, Callback callback) {
        mContext = context;
        this.items = items;
        mCallback = callback;
    }

    @Override
    public SeminarRecyclerViewAdapter.SeminarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_seminar_item, parent, false);
        SeminarViewHolder SeminarViewHolder = new SeminarViewHolder(view);
        return SeminarViewHolder;
    }

    @Override
    public void onBindViewHolder(SeminarRecyclerViewAdapter.SeminarViewHolder holder, int position) {
        final Seminar seminar = items.get(position);
        holder.seminarNameTextView.setText(seminar.name);
        holder.seminarTimeTextView.setText("Starts at: " + seminar.startsAt);
        holder.mLlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.itentToSeminarDetails(seminar.id);
            }
        });

    }

    public void add(ArrayList<Seminar> items) {
        int previousDataSize = this.items.size();
        this.items.addAll(items);
        displayNoData(this.items);
        notifyDataSetChanged();
//        notifyItemRangeInserted(previousDataSize, items.size());
    }

    private void displayNoData(ArrayList<Seminar> items) {

        if (items.size() > 0 == false) {
            mCallback.displayNoDataText();
            return;
        }
        mCallback.removeNoDataText();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class SeminarViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.seminarNameTextView)
        TextView seminarNameTextView;

        @Bind(R.id.seminarTimeTextView)
        TextView seminarTimeTextView;

        @Bind(R.id.llContainer)
        LinearLayout mLlContainer;

        public SeminarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

