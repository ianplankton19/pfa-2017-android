package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.SeminarRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.Seminar;
import ph.coreproc.android.pfa.rest.SeminarInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeminarListActivity extends BaseActivity implements SeminarRecyclerViewAdapter.Callback {

    @Bind(R.id.seminarListRecyclerView)
    RecyclerView seminarListRecyclerView;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;


    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    ArrayList<Seminar> mSeminarArrayList;

    SeminarRecyclerViewAdapter mSeminarRecyclerViewAdapter;




    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, SeminarListActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();
    }

    private void initialize() {

        mSeminarArrayList = new ArrayList<>();

        Seminar seminar1 = new Seminar(1, "International Franchise Opportunities and Business Matching", "", "");
        Seminar seminar2 = new Seminar(2, "How to Invest in the Right Franchise Wisely", "", "");
        Seminar seminar3 = new Seminar(3, "How to Franchise Your Business Seminar", "", "");
        Seminar seminar4 = new Seminar(4, "Discovery Day Session", "", "");

        mSeminarArrayList.add(seminar1);
        mSeminarArrayList.add(seminar2);
        mSeminarArrayList.add(seminar3);
        mSeminarArrayList.add(seminar4);
        mSeminarRecyclerViewAdapter = new SeminarRecyclerViewAdapter(mContext, mSeminarArrayList, SeminarListActivity.this);
        PFABase.setDefaultRecyclerView(mContext, seminarListRecyclerView, mSeminarRecyclerViewAdapter);

//        getData();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_seminar_list;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        final Dialog dialog = PFABase.loadDefaultProgressDialogCancellable(mContext, this);
        dialog.show();

        mSeminarArrayList = new ArrayList<>();
        SeminarInterface seminarInterface = KitchenRestClient.create(mContext, SeminarInterface.class, true);
        Call<DataWrapper<ArrayList<Seminar>>> seminarResponseCall = seminarInterface.getSeminar(getString(R.string.get_seminar_url));

        seminarResponseCall.enqueue(new Callback<DataWrapper<ArrayList<Seminar>>>() {
            @Override
            public void onResponse(Call<DataWrapper<ArrayList<Seminar>>> call, Response<DataWrapper<ArrayList<Seminar>>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {
                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                mSeminarArrayList = response.body().getData();

                mSeminarRecyclerViewAdapter.add(mSeminarArrayList);
            }

            @Override
            public void onFailure(Call<DataWrapper<ArrayList<Seminar>>> call, Throwable t) {
                dialog.dismiss();

            }
        });

    }

    @Override
    public void itentToSeminarDetails(int id) {
        startActivity(SeminarDetailsActivity.newIntent(mContext, "" + id, false));
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }
}
