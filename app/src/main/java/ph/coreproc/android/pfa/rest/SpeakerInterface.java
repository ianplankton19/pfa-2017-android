package ph.coreproc.android.pfa.rest;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.models.Speaker;
import ph.coreproc.android.pfa.models.requests.CommentBody;
import ph.coreproc.android.pfa.models.requests.QuestionBody;
import ph.coreproc.android.pfa.models.requests.RatingBody;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by willm on 3/30/2017.
 */

public interface SpeakerInterface {

    @GET
    Call<DataWrapper<ArrayList<Speaker>>> getSpeakers(@Url String url);

    @GET
    Call<DataWrapper<Speaker>> getSpeaker(@Url String url);

    @POST("/api/v1/speakers/{id}/comments")
    Call<ResponseBody> postSpeakerComment(@Path("id") String id, @Body CommentBody commentBody);

    @POST("/api/v1/speakers/{id}/ratings")
    Call<ResponseBody> postSpeakerRating(@Path("id") String id, @Body RatingBody ratingBody);

    @POST("/api/v1/speakers/{id}/questions")
    Call<ResponseBody> postSpeakerQuestion(@Path("id") String id, @Body QuestionBody questionBody);
}
