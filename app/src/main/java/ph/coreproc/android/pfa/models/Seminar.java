package ph.coreproc.android.pfa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ph.coreproc.android.pfa.models.offlinemodels.OfflineExtension;

/**
 * Created by willm on 3/27/2017.
 */

public class Seminar extends OfflineExtension implements Serializable {


    public Seminar(int id, String name, String location, String startsAt) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.startsAt = startsAt;
    }

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("location")
    @Expose
    public String location;

    @SerializedName("starts_at")
    @Expose
    public String startsAt;

    @SerializedName("rating")
    @Expose
    public int rating;

}
