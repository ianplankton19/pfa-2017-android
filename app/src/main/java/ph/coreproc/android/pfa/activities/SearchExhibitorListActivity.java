package ph.coreproc.android.pfa.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.ExhibitorsRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.rest.ExhibitorInterface;
import ph.coreproc.android.pfa.rest.responses.MetaDataWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 4/24/2017.
 */

public class SearchExhibitorListActivity extends BaseActivity implements ExhibitorsRecyclerViewAdapter.Callback {

    @Bind(R.id.rvExhibitorList)
    RecyclerView mRvExhibitorList;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    private ExhibitorsRecyclerViewAdapter mExhibitorsRecyclerViewAdapter;

    private ArrayList<Exhibitor> mExhibitorArrayList;

    private Call<MetaDataWrapper<ArrayList<Exhibitor>>> mExhibitorSearchCallResponse;

    public static Intent newIntent(Context context) {

        Intent intent = new Intent(context, SearchExhibitorListActivity.class);
        return intent;
    }


    SearchView searchView;
    SearchView.OnQueryTextListener queryTextListener;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_search_exhibitor_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        onSearchRequested();
        ButterKnife.bind(this);
        initialize();

    }

    private void initialize() {
        mExhibitorArrayList = new ArrayList<>(PFABase.getExhibitorLocals(mContext, "", "", 0));

        mExhibitorsRecyclerViewAdapter = new ExhibitorsRecyclerViewAdapter(mContext, mExhibitorArrayList, SearchExhibitorListActivity.this);
        PFABase.setDefaultRecyclerView(mContext, mRvExhibitorList, mExhibitorsRecyclerViewAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
                searchView.setOnQueryTextListener(queryTextListener);
                // Not implemented here
                return false;
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.exhibitor_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) SearchExhibitorListActivity.this.getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setIconified(false);
        View closeButton = searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //handle click
                finish();
            }
        });
//        if (searchItem != null) {
//        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {

//                    if (mExhibitorSearchCallResponse != null) {
//                        mExhibitorSearchCallResponse.cancel();
//                    }

//                    getData(newText);
                    mExhibitorsRecyclerViewAdapter.getFilter().filter(newText);
                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }

        return super.onCreateOptionsMenu(menu);
    }

    private void getData(String search) {

        if (mExhibitorSearchCallResponse != null) {
            mExhibitorSearchCallResponse.cancel();
        }

        ExhibitorInterface exhibitorInterface = KitchenRestClient.create(mContext, ExhibitorInterface.class, true);
        mExhibitorSearchCallResponse = exhibitorInterface.getExhibitors(getString(R.string.get_exhibitors_url), "", search, "", "");
        mExhibitorSearchCallResponse.enqueue(new Callback<MetaDataWrapper<ArrayList<Exhibitor>>>() {
            @Override
            public void onResponse(Call<MetaDataWrapper<ArrayList<Exhibitor>>> call, Response<MetaDataWrapper<ArrayList<Exhibitor>>> response) {
                if (!response.isSuccessful()) {
                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "An error occured. Please try again.");
                    return;
                }

                mExhibitorsRecyclerViewAdapter.changeData(response.body().getData());

            }

            @Override
            public void onFailure(Call<MetaDataWrapper<ArrayList<Exhibitor>>> call, Throwable t) {

                if (call.isCanceled() || t.getMessage() != null && t.getMessage().equals("Canceled")) {
                    // User Cancelled
                    //mSwipeRefreshLayout.setRefreshing(false);
                }

//                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global) + "\n" + t.getMessage());
            }
        });
    }


    @Override
    public void intentToDetails(int id) {
        startActivity(ExhibitorDetailsActivity.newIntent(mContext, id, false));
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }
}
