package ph.coreproc.android.pfa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Country;

/**
 * Created by IanBlanco on 4/18/2017.
 */

public class CountriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public interface Callback {
        void getPosition(Country country);
    }

    private Callback mCallback;
    private Context mContext;
    private ArrayList<Country> mCountryArrayList;


    public CountriesAdapter(Context context, ArrayList<Country> countryArrayList, Callback callback) {
        mContext = context;
        mCountryArrayList = countryArrayList;
        mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_region, parent, false);
        CountryViewHolder countryViewHolder = new CountryViewHolder(view);
        return countryViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final CountryViewHolder countryViewHolder = (CountryViewHolder) holder;
        final Country country = mCountryArrayList.get(position);
        countryViewHolder.mRegionName.setText(country.name);
        countryViewHolder.mLlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.getPosition(country);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCountryArrayList.size();
    }

    static class CountryViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.regionName)
        TextView mRegionName;

        @Bind(R.id.llContainer)
        LinearLayout mLlContainer;

        public CountryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
