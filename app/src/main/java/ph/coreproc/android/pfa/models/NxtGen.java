package ph.coreproc.android.pfa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 5/10/2017.
 */

public class NxtGen implements Serializable {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("company_name")
    @Expose
    public String name;

    @SerializedName("sector")
    @Expose
    public String sector;

    @SerializedName("franchise_name")
    @Expose
    public String franchiseName;

    @SerializedName("contact_person")
    @Expose
    public String contactPerson;

    @SerializedName("contact_number")
    @Expose
    public String contactNo;

    @SerializedName("designation")
    @Expose
    public String designation;

    @SerializedName("website")
    @Expose
    public String website;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("booth")
    @Expose
    public String boothNumber;


    @SerializedName("rating")
    @Expose
    public int rating;

    @SerializedName("business_line")
    @Expose
    public String businessLine;

    @SerializedName("franchise_package")
    @Expose
    public String franchisePackage;

    @SerializedName("franchise_fee")
    @Expose
    public String franchiseFee;

    @SerializedName("roi_period")
    @Expose
    public String roiPeriod;

    @SerializedName("term_of_franchise")
    @Expose
    public String termOfFranchise;

    @SerializedName("renewal")
    @Expose
    public String renewal;

    @SerializedName("royalty_fee")
    @Expose
    public String royaltyFee;

    @SerializedName("advertising_fee")
    @Expose
    public String advertisingFee;

    @SerializedName("no_of_stores")
    @Expose
    public String noOfStores;

    @SerializedName("company_owned")
    @Expose
    public String companyOwned;

    @SerializedName("franchise_stores")
    @Expose
    public String franchiseStores;

    @SerializedName("franchising_since")
    @Expose
    public String franchisingSince;

    @SerializedName("year_started")
    @Expose
    public String yearStarted;

    @SerializedName("investment_level")
    @Expose
    public String investmentLevel;

    @SerializedName("total_investment_level")
    @Expose
    public String totalInvestmentLevel;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("logo")
    @Expose
    public String avatar;

}
