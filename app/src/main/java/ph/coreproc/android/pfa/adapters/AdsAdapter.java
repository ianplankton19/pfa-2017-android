package ph.coreproc.android.pfa.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import ph.coreproc.android.pfa.fragments.AdsFragment;
import ph.coreproc.android.pfa.models.Ads;

/**
 * Created by willm on 5/10/2017.
 */

public class AdsAdapter extends FragmentPagerAdapter {
    private Context context;
    private int mCount;
    private ArrayList<AdsFragment> mSlideFragments;

    public AdsAdapter(FragmentManager fm, Context context, ArrayList<Ads> adsArrayList) {
        super(fm);
        this.context = context;

        mSlideFragments = new ArrayList<>();

        prepareFragments(adsArrayList);

    }

    private void prepareFragments(ArrayList<Ads> adsArrayList) {

        for (int i = 0; i < adsArrayList.size(); i++) {
            mSlideFragments.add(AdsFragment.newInstance(adsArrayList.get(i).adBanner, adsArrayList.get(i).link));
        }

    }

    public int getFragmentsCount() {
        return mSlideFragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        return mSlideFragments.get(position);
    }

    @Override
    public int getCount() {
        return mSlideFragments.size();
    }

    @Override
    public String getPageTitle(int position) {
        return null;
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}
