package ph.coreproc.android.pfa.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.activeandroid.util.Log;
import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;

/**
 * Created by willm on 5/10/2017.
 */

public class AdsFragment extends Fragment {

    private Context mContext;

    @Bind(R.id.adImage)
    ImageView adImage;


    public static AdsFragment newInstance(String imageUrl, String link) {
        AdsFragment slideFirstFragment = new AdsFragment();

        Bundle args = new Bundle();
        args.putString("imageUrl", imageUrl);
        args.putString("link", link);
        slideFirstFragment.setArguments(args);

        return slideFirstFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ad, container, false);



        ButterKnife.bind(this, view);
        mContext = getActivity();

        initialize();

        return view;
    }

    private void initialize() {
        final Bundle bundle = getArguments();
        Glide.with(mContext)
                .load(bundle.getString("imageUrl"))
                .into(adImage);

        adImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // open link
                String url = bundle.getString("link") != null ? bundle.getString("link") : "";

                Log.i("test", "test" + url);
                if (url.length() > 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
            }
        });
    }


}
