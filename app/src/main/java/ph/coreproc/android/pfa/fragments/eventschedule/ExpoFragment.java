package ph.coreproc.android.pfa.fragments.eventschedule;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.eventschedule.EventScheduleAdapter;
import ph.coreproc.android.pfa.models.Event;

/**
 * Created by IanBlanco on 4/19/2017.
 */

public class ExpoFragment extends Fragment {

    @Bind(R.id.rvEventSchedule)
    RecyclerView mRvEventSchedule;

    private Context mContext;

    private ArrayList<Event> mEventArrayList;
    private EventScheduleAdapter mEventScheduleAdapter;

    public static ExpoFragment newInstance() {
        ExpoFragment expoFragment = new ExpoFragment();

        return expoFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_event_schedule_list_fragment, container, false);
        mContext = getActivity();
        ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    private void initialize() {
        mEventArrayList = new ArrayList<>(PFABase.getExpoEvents(mContext));
        mEventScheduleAdapter = new EventScheduleAdapter(mContext, mEventArrayList);
        PFABase.setDefaultRecyclerView(mContext, mRvEventSchedule, mEventScheduleAdapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
