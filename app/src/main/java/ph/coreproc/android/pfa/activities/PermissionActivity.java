package ph.coreproc.android.pfa.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import ph.coreproc.android.pfa.R;

/**
 * Created by IanBlanco on 5/19/2017.
 */

public class PermissionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dialog);
        checkRequiredPermissions();
    }


    private void checkRequiredPermissions() {

        Log.d("perrr", "asdasd " + (ActivityCompat.checkSelfPermission(this, Manifest.permission_group.CAMERA)
                == PackageManager.PERMISSION_GRANTED));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission_group.CAMERA)
                == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            finish();
        } else {
            requestCameraPermission();
        }

    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.CAMERA, Manifest.permission_group.CAMERA
        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        boolean permissionGranted = true;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                permissionGranted = false;
                break;
            }
        }

        if (!permissionGranted) {
            checkRequiredPermissions();
            return;
        }

        finish();

    }

    @Override
    public void onBackPressed() {
        // do nothing
    }
}
