package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.requests.DownloadBody;
import ph.coreproc.android.pfa.rest.ConferenceInterface;
import ph.coreproc.android.pfa.rest.ExhibitorInterface;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 6/2/2017.
 */

public class SendEmailDisplayActivity extends BaseActivity {

    public static String ARGS_IDS = "ARGS_IDS";
    private String mIds;

    public static String ARGS_ROLE = "ARGS_ROLE";
    private String mRole;

    public static String ARGS_CONFERENCE_ID = "ARGS_CONFERENCE_ID";
    private String mConferenceId;

    public static Intent newIntent(Context context, String ids, String role, String conferenceId) {
        Intent intent = new Intent(context, SendEmailDisplayActivity.class);
        intent.putExtra(ARGS_IDS, ids);
        intent.putExtra(ARGS_ROLE, role);
        intent.putExtra(ARGS_CONFERENCE_ID, conferenceId);
        return intent;
    }

    @Bind(R.id.etEmail)
    EditText mEtEmail;

    @Bind(R.id.btnSend)
    Button mBtnSend;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_dialog_email;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {
        String response = Preferences.getString(mContext, Prefs.USER);
        User user = ModelUtil.fromJson(User.class, response);
        mEtEmail.setText(user.email);
        Bundle extras = getIntent().getExtras();
        mIds = extras.getString(ARGS_IDS);
        mRole = extras.getString(ARGS_ROLE);
        mConferenceId = extras.getString(ARGS_CONFERENCE_ID);
    }


    @OnClick(R.id.btnSend)
    public void onClick() {

        if (mEtEmail.getText().toString().equals("")) {

            PFABase.dialogWithOnClick(mContext, "Please input your email first", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            return;
        }

        if (!PFABase.isEmailValid(mEtEmail.getText().toString())) {

            PFABase.dialogWithOnClick(mContext, "incorrect email format", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            return;

        }

        StringBuilder sb = new StringBuilder(mIds);
        try {
            sb.setCharAt(mIds.length() - 1, ' ');
        } catch (Exception e) {
//            do nothing
        }
        String ids = sb.toString();

        if (mRole.equals("Exhibitor")) {
            DownloadBody downloadBody = new DownloadBody(ids, mEtEmail.getText().toString());
            final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
            dialog.show();
            ExhibitorInterface exhibitorInterface = KitchenRestClient.create(mContext, ExhibitorInterface.class, true);
            Call<ResponseBody> emailRequestCall = exhibitorInterface.postExhibitorEmail(downloadBody);
            emailRequestCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dialog.dismiss();

                    if (!response.isSuccessful()) {

                        if (response.code() == 401) {
                            User.logout(mContext);
                            return;
                        }

                        APIError error = ErrorUtil.parsingError(response);
                        if (response.code() == 405) {
                            PFABase.dialogWithOnClickCancelable(mContext, "" + error.getError().getMessage(), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                            return;
                        }


                        KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
//                    PFABase.dialogWithOnClickCancelable(mContext, "" + error.getError().getMessage(), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            finish();
//                        }
//                    }).show();

//                    finish();
                        return;
                    }

                    PFABase.dialogWithOnClickCancelable(mContext, "Download links are sent to your email.", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dialog.dismiss();
                    KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                }
            });

        }else {
            DownloadBody downloadBody = new DownloadBody(mEtEmail.getText().toString());
            final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
            dialog.show();
            ConferenceInterface conferenceInterface = KitchenRestClient.create(mContext, ConferenceInterface.class, true);
            Call<ResponseBody> emailRequestCall = conferenceInterface.postConferenceDownload(mConferenceId, downloadBody);
            emailRequestCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dialog.dismiss();

                    if (!response.isSuccessful()) {

                        if (response.code() == 401) {
                            User.logout(mContext);
                            return;
                        }

                        APIError error = ErrorUtil.parsingError(response);
                        if (response.code() == 405) {
                            PFABase.dialogWithOnClickCancelable(mContext, "" + error.getError().getMessage(), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                            return;
                        }


                        KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
//                    PFABase.dialogWithOnClickCancelable(mContext, "" + error.getError().getMessage(), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            finish();
//                        }
//                    }).show();

//                    finish();
                        return;
                    }

                    PFABase.dialogWithOnClickCancelable(mContext, "Download links are sent to your email.", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dialog.dismiss();
                    KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                }
            });
        }

    }
}
