package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.NxtGen;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.rest.NxtGenInterface;
import ph.coreproc.android.pfa.rest.UserInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.rest.responses.MetaNxtGenWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 4/26/2017.
 */

public class NxtGenListActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.btnFullDetailsOne)
    Button mBtnFullDetailsOne;

    @Bind(R.id.btnFullDetailsTwo)
    Button mBtnFullDetailsTwo;

    @Bind(R.id.btnFullDetailsThree)
    Button mBtnFullDetailsThree;

    @Bind(R.id.exhibitorImageViewOne)
    ImageView mExhibitorImageViewOne;

    @Bind(R.id.etNxtGenOne)
    TextView mEtNxtGenOne;

    @Bind(R.id.etConceptOne)
    TextView mEtConceptOne;

    @Bind(R.id.buttonVoteOne)
    Button mButtonVoteOne;

    @Bind(R.id.rlContainerOne)
    LinearLayout mRlContainerOne;

    @Bind(R.id.exhibitorImageViewTwo)
    ImageView mExhibitorImageViewTwo;

    @Bind(R.id.etNxtGenTwo)
    TextView mEtNxtGenTwo;

    @Bind(R.id.etConceptTwo)
    TextView mEtConceptTwo;

    @Bind(R.id.buttonVoteTwo)
    Button mButtonVoteTwo;

    @Bind(R.id.rlContainerTwo)
    LinearLayout mRlContainerTwo;

    @Bind(R.id.exhibitorImageViewThree)
    ImageView mExhibitorImageViewThree;

    @Bind(R.id.etNxtGenThree)
    TextView mEtNxtGenThree;

    @Bind(R.id.etConceptThree)
    TextView mEtConceptThree;

    @Bind(R.id.buttonVoteThree)
    Button mButtonVoteThree;

    @Bind(R.id.rlContainerThree)
    LinearLayout mRlContainerThree;

    private boolean mIsAvailable = false;

    private ArrayList<NxtGen> mNxtGenArrayList;
    private User mUser;

    public static Intent newIntent(Context context) {

        Intent intent = new Intent(context, NxtGenListActivity.class);

        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_nxt_gen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialize();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initialize() {
        mNxtGenArrayList = new ArrayList<>();

        mUser = ModelUtil.fromJson(User.class,Preferences.getString(mContext, Prefs.USER));

        getData();

    }

    private void setUpFields(ArrayList<NxtGen> items) {

        mNxtGenArrayList = items;

//        For 1st Layer
        try {
            mEtNxtGenOne.setText(mNxtGenArrayList.get(0).name);
            mEtConceptOne.setText(mNxtGenArrayList.get(0).sector);

            Glide.with(mContext)
                    .load(mNxtGenArrayList.get(0).avatar)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder)
                    .listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Log.d("error loading image", "error" + e.getMessage());
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    mExhibitorImageViewOne.setImageDrawable(resource);
                    Log.d("error loading image", "success");
                    return false;
                }
            })
                    .into(mExhibitorImageViewOne);

        } catch (Exception e) {
//            error in data
            PFABase.dialogWithOnClick(mContext, "No data to display", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

        }

//        For 2nd Layer
        try {
            mEtNxtGenTwo.setText(mNxtGenArrayList.get(1).name);
            mEtConceptTwo.setText(mNxtGenArrayList.get(1).sector);
            Glide.with(mContext)
                    .load(mNxtGenArrayList.get(1).avatar)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Log.d("error loading image", "error" + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            mExhibitorImageViewTwo.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(mExhibitorImageViewTwo);
        } catch (Exception e) {
//            error in data
            PFABase.dialogWithOnClick(mContext, "No data to display", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }

//        For 3rd Layer
        try {
            mEtNxtGenThree.setText(mNxtGenArrayList.get(2).name);
            mEtConceptThree.setText(mNxtGenArrayList.get(2).sector);
            Glide.with(mContext)
                    .load(mNxtGenArrayList.get(2).avatar)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Log.d("error loading image", "error" + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            mExhibitorImageViewThree.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(mExhibitorImageViewThree);
        } catch (Exception e) {
//            error in data
            PFABase.dialogWithOnClick(mContext, "No data to display", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }

    }


    private void setButtonDisabled(boolean isVoted) {

        if (mIsAvailable) {
            if (isVoted) {
                mButtonVoteOne.setBackgroundColor(R.color.black);
                mButtonVoteTwo.setBackgroundColor(R.color.black);
                mButtonVoteThree.setBackgroundColor(R.color.black);
                mButtonVoteOne.setClickable(false);
                mButtonVoteTwo.setClickable(false);
                mButtonVoteThree.setClickable(false);
            }
        }else {
//            if (!mIsAvailable) {
                mButtonVoteOne.setBackgroundColor(R.color.black);
                mButtonVoteTwo.setBackgroundColor(R.color.black);
                mButtonVoteThree.setBackgroundColor(R.color.black);
                mButtonVoteOne.setClickable(false);
                mButtonVoteTwo.setClickable(false);
                mButtonVoteThree.setClickable(false);
//            }
        }
    }

    private void getData() {
        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();
        NxtGenInterface nxtGenInterface = KitchenRestClient.create(mContext, NxtGenInterface.class, true);
        Call<MetaNxtGenWrapper<ArrayList<NxtGen>>> nxtGenCall = nxtGenInterface.getNxtGen(mContext.getString(R.string.get_nxt_gen_url));
        nxtGenCall.enqueue(new Callback<MetaNxtGenWrapper<ArrayList<NxtGen>>>() {
            @Override
            public void onResponse(Call<MetaNxtGenWrapper<ArrayList<NxtGen>>> call, Response<MetaNxtGenWrapper<ArrayList<NxtGen>>> response) {
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                setUpFields(response.body().getData());
                mIsAvailable = response.body().getMeta().isKey();
                getUsers(dialog);
            }

            @Override
            public void onFailure(Call<MetaNxtGenWrapper<ArrayList<NxtGen>>> call, Throwable t) {
                dialog.dismiss();
                PFABase.dialogWithOnClick(mContext, getString(R.string.dialog_error_global), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                return;
            }
        });

    }

    private void getUsers(final Dialog dialog) {
        UserInterface userInterface = KitchenRestClient.create(mContext, UserInterface.class, true);
        Call<DataWrapper<User>> userResponse = userInterface.getUser("" +mUser.id, null);
        userResponse.enqueue(new Callback<DataWrapper<User>>() {
            @Override
            public void onResponse(Call<DataWrapper<User>> call, Response<DataWrapper<User>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                setButtonDisabled(response.body().getData().isVoted);
                mUser = response.body().getData();
                Preferences.setString(mContext, Prefs.USER, ModelUtil.toJsonString(mUser));

            }

            @Override
            public void onFailure(Call<DataWrapper<User>> call, Throwable t) {
//                dialog.dismiss();
//                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
            }
        });
    }

    @OnClick({R.id.btnFullDetailsOne, R.id.btnFullDetailsTwo, R.id.btnFullDetailsThree,
            R.id.buttonVoteOne, R.id.buttonVoteTwo, R.id.buttonVoteThree})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnFullDetailsOne:
                startActivity(NxtGenDetailsActivity.newIntent(mContext, mNxtGenArrayList.get(0).id + ""));
                break;
            case R.id.btnFullDetailsTwo:
                startActivity(NxtGenDetailsActivity.newIntent(mContext, mNxtGenArrayList.get(1).id + ""));
                break;
            case R.id.btnFullDetailsThree:
                startActivity(NxtGenDetailsActivity.newIntent(mContext, mNxtGenArrayList.get(2).id + ""));
                break;
            case R.id.buttonVoteOne:
                showDialog(mNxtGenArrayList.get(0).id + "");
                break;
            case R.id.buttonVoteTwo:
                showDialog(mNxtGenArrayList.get(1).id + "");
                break;
            case R.id.buttonVoteThree:
                showDialog(mNxtGenArrayList.get(2).id + "");
                break;
        }
    }

    private void showDialog(final String id) {
        PFABase.dialogWithOnCLickCancelable(mContext, "Are you sure you want to vote this nxtgen contestant?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                voteFunction(id);
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    private void voteFunction(String id) {
        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();
        NxtGenInterface nxtGenInterface = KitchenRestClient.create(mContext, NxtGenInterface.class, true);
        Call<ResponseBody> voteCall = nxtGenInterface.voteNxtGen(id);
        voteCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }
                KitchenUiUtils.showAlertDialog(mContext, "Vote sent");
                setButtonDisabled(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global) + "\n" + t.getMessage());
            }
        });
    }

}
