package ph.coreproc.android.pfa.models;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;

import com.coreproc.android.kitchen.preferences.Preferences;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ph.coreproc.android.pfa.activities.SplashScreenActivity;
import ph.coreproc.android.pfa.models.offlinemodels.OfflineExtension;

/**
 * Created by IanBlanco on 5/8/2017.
 */

public class User extends OfflineExtension implements Serializable {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("qr_code")
    @Expose
    public String qrCode;

    @SerializedName("isVoted")
    @Expose
    public boolean isVoted;

    @SerializedName("expovisitor")
    @Expose
    public ExpoVisitor expoVisitor;

    @SerializedName("role")
    @Expose
    public String role;

    @SerializedName("avatar")
    @Expose
    public String logo;


    
    public static void logout(final Context context) {

        Preferences.setString(context, Preferences.API_KEY, "");
        Intent intent = SplashScreenActivity.newIntent(context);
        ActivityCompat.finishAffinity((Activity) context);
        context.startActivity(intent);

    }
}
