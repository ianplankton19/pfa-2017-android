package ph.coreproc.android.pfa.adapters.favorites;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.offlinemodels.SpeakerLocal;

/**
 * Created by IanBlanco on 4/11/2017.
 */

public class FavoritesSpeakerConfernceRecyclerViewAdapter extends RecyclerView.Adapter<FavoritesSpeakerConfernceRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<SpeakerLocal> mSpeakerLocals;

    public FavoritesSpeakerConfernceRecyclerViewAdapter(Context context, ArrayList<SpeakerLocal> speakerLocals) {
        mContext = context;
        mSpeakerLocals = speakerLocals;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_speaker_conference_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SpeakerLocal speakerLocal = mSpeakerLocals.get(position);

        holder.mConferenceSpeakerTextView.setText(speakerLocal.speakerName);
        holder.mTvDetails.setText(speakerLocal.speakerCompany);

        if (position == mSpeakerLocals.size() - 1){
            holder.mRlContainer.setBackground(mContext.getDrawable(R.drawable.footer_background));
        }

    }

    @Override
    public int getItemCount() {
        return mSpeakerLocals.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.conferenceSpeakerImageView)
        CircleImageView mConferenceSpeakerImageView;

        @Bind(R.id.proceed)
        ImageView mProceed;

        @Bind(R.id.tvDetails)
        TextView mTvDetails;

        @Bind(R.id.conferenceSpeakerTextView)
        TextView mConferenceSpeakerTextView;

        @Bind(R.id.rlContainer)
        RelativeLayout mRlContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
