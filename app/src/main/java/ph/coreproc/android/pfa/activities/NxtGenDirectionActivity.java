package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.utils.TextViewEx;

/**
 * Created by IanBlanco on 4/27/2017.
 */

public class NxtGenDirectionActivity extends BaseActivity {


    @Bind(R.id.btnVoteNow)
    Button mBtnVoteNow;

    @Bind(R.id.tvOne)
    TextView mTvOne;

    @Bind(R.id.tvTwo)
    TextView mTvTwo;

    public static Intent newIntent(Context context) {

        Intent intent = new Intent(context, NxtGenDirectionActivity.class);

        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_nxt_gen_direction;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {
        ((TextViewEx) mTvOne).setText(mTvOne.getText().toString(), true);
        ((TextViewEx) mTvTwo).setText(mTvTwo.getText().toString(), true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnVoteNow)
    public void onClick() {
        startActivity(NxtGenListActivity.newIntent(mContext));
    }
}
