package ph.coreproc.android.pfa.models;

import java.io.Serializable;

/**
 * Created by IanBlanco on 3/28/2017.
 */

public class Favorites implements Serializable{


    public Favorites(String name, String booth) {
        this.name = name;
        this.booth = booth;
    }

    private String name;

    private String booth;

    public String getName() {
        return name;
    }

    public String getBooth() {
        return booth;
    }
}
