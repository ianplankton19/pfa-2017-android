package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;

import com.coreproc.android.kitchen.preferences.Preferences;

import java.util.ArrayList;

import butterknife.Bind;
import database.db.notes_local_tbl;
import database.db.notes_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.offlinemodels.Notes;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;

public class NotesActivity extends BaseActivity {

    @Bind(R.id.etNotes)
    EditText mEtNotes;

    public static String ARGS_ID = "ARGS_ID";
    public static String ARGS_NOTENAME = "ARGS_NOTENAME";
    public static String ARGS_CATEGORY = "ARGS_CATEGORY";
    private int mId;
    private String mNoteName = "";
    private String mCategory = "";

    private int rowId;
    private Integer mIndex = null;
    private ArrayList<Notes> mNotesArrayList;
    private ArrayList<Long> mNoteIds;
    private Notes mNotes;

    private User mUser;

    private notes_local_tblDao mNotes_local_tblDao;
    private notes_local_tbl mNotes_local_tbl;

    public static Intent newIntent(Context context, int id, String noteName, String category) {
        Intent intent = new Intent(context, NotesActivity.class);
        intent.putExtra(ARGS_ID, id);
        intent.putExtra(ARGS_NOTENAME, noteName);
        intent.putExtra(ARGS_CATEGORY, category);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNotes_local_tblDao = DataUtil.setUpNotesLocal(mContext, Prefs.DB_NAME);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();
        mId = extras.getInt(ARGS_ID);
        mNoteName = extras.getString(ARGS_NOTENAME);
        mCategory = extras.getString(ARGS_CATEGORY);

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        mNotes = new Notes(mId, "", mNoteName, mCategory, mUser.id, mUser.role);
        mNotesArrayList = new ArrayList<>(DataUtil.getNotesLocal(mNotes_local_tblDao, mUser.id, mUser.role));
        mNoteIds = new ArrayList<>(DataUtil.getNoteIds(mNotes_local_tblDao));

        for (Notes note : mNotesArrayList) {
            if (note.id == mId && note.noteName.equals(mNoteName)) {
                mIndex = mNotesArrayList.indexOf(note);
                mNotes = note;
            }
        }
        mEtNotes.setText(mNotes.note);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_notes;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                PFABase.hideSoftKeyboard(NotesActivity.this);
                finish();
                break;

//            case R.id.menuItemSave:
//                // download conference here
//                String notesToSave = mEtNotes.getText().toString();
//                mNotes.note = notesToSave;
//                saveString(mNotes.getJsonObject().toString());
//                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        String notesToSave = mEtNotes.getText().toString();
        mNotes.note = notesToSave;
        saveString(ModelUtil.toJsonString(mNotes));
        super.onDestroy();
    }

    private void saveString(String saveString) {
        mNotes_local_tbl = new notes_local_tbl(saveString);
        if (!DataUtil.checkIfNoteExist(mNotesArrayList, mNotes)) {

            Log.i("tag", "results :" + saveString);
            mNotes_local_tblDao.insert(mNotes_local_tbl);
            return;
        }

        long updateId = mNoteIds.get(mIndex);
        notes_local_tbl notes_local_tbl = mNotes_local_tblDao.load(updateId);
        notes_local_tbl.setSave_string(saveString);
        mNotes_local_tblDao.update(notes_local_tbl);
//        mNotes_local_tblDao.insertOrReplace();


    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.notes, menu);
//        return true;
//    }
}
