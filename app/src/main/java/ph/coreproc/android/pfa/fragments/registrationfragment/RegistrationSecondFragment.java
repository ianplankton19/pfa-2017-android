package ph.coreproc.android.pfa.fragments.registrationfragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.RegistrationActivity;

/**
 * Created by IanBlanco on 3/29/2017.
 */

public class RegistrationSecondFragment extends Fragment {


    @Bind(R.id.etFullname)
    EditText mEtFullname;

    @Bind(R.id.etAge)
    EditText mEtAge;

    @Bind(R.id.etGender)
    EditText mEtGender;





    private TextWatcher mTextWatcher;

    RegistrationActivity mRegistrationActivity;

    private final String[] mManagementLevelPicker = {"Top Management(CEO / President / COO)", "Middle Management", "Supervisor", "Staff"};

    private final String[] mAgePicker = {"18 - 25 years old", "26 - 35 years old", "36 - 45 years old", "46 - 55 years old", "Above 55 years old"};

    private final String[] mOccupationPicker = {"Business Owner / Entrepreneur", "Retailer", "Professional Service(Provider / Consultant)", "Academe",
            "Student", "Overseas Filipino / OF Family", "Medical Practitioners", "Retiree / Veteran", "Employee", ""};


    public static RegistrationSecondFragment newInstance() {

        RegistrationSecondFragment registrationSecondFragment = new RegistrationSecondFragment();

        return registrationSecondFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.registration_second_page_new, container, false);
        mRegistrationActivity = (RegistrationActivity) getActivity();
        ButterKnife.bind(this, view);
        initialize();

        return view;
    }

    private void initialize() {


        mEtFullname.addTextChangedListener(setTextWatcher(mEtFullname));


        mEtAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(getActivity(), "Choose age group", mAgePicker, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEtAge.setText(mAgePicker[which]);
                        mRegistrationActivity.mAgeRangeStr = mAgePicker[which];
                    }
                }).show();
            }
        });

        mEtGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showPicker();
                final String[] gender = {"Male", "Female"};
                PFABase.dialogPicker(getActivity(), "Choose Gender", gender, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEtGender.setText(gender[which]);
                        mRegistrationActivity.mGenderStr = gender[which];
                    }
                }).show();
            }
        });


    }


    private TextWatcher setTextWatcher(final EditText editText) {

        mTextWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                switch (editText.getId()) {

                    case R.id.etFullname:
                        mRegistrationActivity.mFullnameStr = "" + s;
                        break;


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        return mTextWatcher;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
