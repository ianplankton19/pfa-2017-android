package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import database.db.user_scan_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.RVUserScannedAdapter;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.rest.ExhibitorInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 6/1/2017.
 */

public class MyScansUserActivity extends BaseActivity implements RVUserScannedAdapter.Callback {


    @Bind(R.id.exhibitorsListRecyclerView)
    RecyclerView mExhibitorsListRecyclerView;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    @Bind(R.id.activity_seminar_list)
    LinearLayout mActivitySeminarList;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;


    private user_scan_local_tblDao mUser_scan_local_tblDao;
    private ArrayList<User> mUserArrayList;
    private RVUserScannedAdapter mRVUserScannedAdapter;

    private User mUser;

    public static String ARGS_WHO_SCANNED_ME = "ARGS_WHO_SCANNED_ME";
    private boolean mWhoScannedMe;


    public static Intent newIntent(Context context, boolean whoScannedMe) {

        Intent intent = new Intent(context, MyScansUserActivity.class);
        intent.putExtra(ARGS_WHO_SCANNED_ME, whoScannedMe);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_scanned_exhibitors;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        mUser_scan_local_tblDao = DataUtil.setUpUserScannedLocal(mContext, Prefs.DB_NAME);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {

        Bundle extras = getIntent().getExtras();
        mWhoScannedMe = extras.getBoolean(ARGS_WHO_SCANNED_ME);
        mUserArrayList = new ArrayList<>();
        mRVUserScannedAdapter = new RVUserScannedAdapter(mContext, mUserArrayList, MyScansUserActivity.this);
        PFABase.setDefaultRecyclerView(mContext, mExhibitorsListRecyclerView, mRVUserScannedAdapter);
        getData();
    }


    private void getData() {

        if (mWhoScannedMe) {
            getSupportActionBar().setTitle("Who Scanned Me");
            final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
            dialog.show();
            ExhibitorInterface exhibitorInterface = KitchenRestClient.create(mContext, ExhibitorInterface.class, true);
            Call<DataWrapper<ArrayList<User>>> userResponseCall = exhibitorInterface.getUserScannedMe();
            userResponseCall.enqueue(new Callback<DataWrapper<ArrayList<User>>>() {
                @Override
                public void onResponse(Call<DataWrapper<ArrayList<User>>> call, Response<DataWrapper<ArrayList<User>>> response) {
                    dialog.dismiss();
                    if (!response.isSuccessful()) {

                        if (response.code() == 401) {
                            User.logout(mContext);
                            return;
                        }

                        APIError error = ErrorUtil.parsingError(response);
                        KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                        return;
                    }
                    mUserArrayList = response.body().getData();
                    mRVUserScannedAdapter.add(mUserArrayList);
                }

                @Override
                public void onFailure(Call<DataWrapper<ArrayList<User>>> call, Throwable t) {
                    dialog.dismiss();
                    KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                }
            });
            return;
        }
        mUserArrayList = DataUtil.getUserScanned(mUser_scan_local_tblDao, mUser.id, mUser.role);
        mRVUserScannedAdapter.add(mUserArrayList);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void removeNoDataText() {

    }

    @Override
    public void displayNoDataText() {

    }

    @Override
    public void intentToDetails(int id) {
        startActivity(UserInfoActivity.newIntent(mContext, "" + id, false));
    }
}
