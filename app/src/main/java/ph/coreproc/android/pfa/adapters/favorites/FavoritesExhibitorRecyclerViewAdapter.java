package ph.coreproc.android.pfa.adapters.favorites;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Exhibitor;

/**
 * Created by IanBlanco on 4/17/2017.
 */

public class FavoritesExhibitorRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface Callback extends DisplayNoDataInterface{
        void intentToDetails(int id);
    }

    private Callback mCallback;
    private Context mContext;
    private ArrayList<Exhibitor> mExhibitorLocals;

    public FavoritesExhibitorRecyclerViewAdapter(Context context, ArrayList<Exhibitor> exhibitorLocals, Callback callback) {
        mContext = context;
        mExhibitorLocals = exhibitorLocals;
        arrangeArray();
        mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_exhibitor_item, parent, false);
        ExhibitorViewHolder exhibitorViewHolder = new ExhibitorViewHolder(view);
        return exhibitorViewHolder;
    }

    public void arrangeArray() {
        Collections.sort(mExhibitorLocals, new Comparator<Exhibitor>() {
            @Override
            public int compare(Exhibitor o1, Exhibitor o2) {
                return o1.franchiseName.compareTo(o2.franchiseName);
            }
        });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ExhibitorViewHolder exhibitorViewHolder = (ExhibitorViewHolder) holder;
        final Exhibitor exhibitorLocal = mExhibitorLocals.get(position);
        if (exhibitorLocal.avatar != null) {
            Glide.with(mContext)
                    .load(exhibitorLocal.avatar)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_delete_white)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Log.d("error loading image", "erro " + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            exhibitorViewHolder.mExhibitorImageView.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(exhibitorViewHolder.mExhibitorImageView);
        }
        exhibitorViewHolder.mExhibitorNameTextView.setText(exhibitorLocal.franchiseName);
        exhibitorViewHolder.mBoothNumberTextView.setText("Booth " + exhibitorLocal.boothNumber);
        exhibitorViewHolder.mRlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.intentToDetails(exhibitorLocal.id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mExhibitorLocals.size();
    }


    public void add(ArrayList<Exhibitor> items){
        mExhibitorLocals.addAll(items);
        displayNoData(mExhibitorLocals);
        arrangeArray();
        notifyDataSetChanged();
    }

    public void update(ArrayList<Exhibitor> items) {
        mExhibitorLocals = items;
        displayNoData(mExhibitorLocals);
        arrangeArray();
        notifyDataSetChanged();
    }

    private void displayNoData(ArrayList<Exhibitor> items) {

        if (items.size() > 0 == false) {
            mCallback.displayNoDataText();
            return;
        }
        mCallback.removeNoDataText();
    }

    public class ExhibitorViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.exhibitorImageView)
        ImageView mExhibitorImageView;

        @Bind(R.id.exhibitorNameTextView)
        TextView mExhibitorNameTextView;

        @Bind(R.id.boothNumberTextView)
        TextView mBoothNumberTextView;

        @Bind(R.id.rlContainer)
        RelativeLayout mRlContainer;

        public ExhibitorViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
