package ph.coreproc.android.pfa.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * Created by IanBlanco on 4/4/2017.
 */

public class Regions {

    public ArrayList<Region> mRegionArrayList;

    public Regions(JsonObject jsonObject){
        JsonArray json = jsonObject.get("regions").getAsJsonArray();
        mRegionArrayList = new ArrayList<>();
        for (int i = 0; i < json.size(); i ++){

            mRegionArrayList.add(new Region(json.get(i).getAsJsonObject()));

        }

    }
}
