package ph.coreproc.android.pfa.fragments.registrationfragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.RegistrationActivity;

/**
 * Created by IanBlanco on 3/29/2017.
 */

public class RegistrationFourthFragment extends Fragment {

    @Bind(R.id.etCompany)
    EditText mEtCompany;

    @Bind(R.id.etPhone)
    EditText mEtPhone;

    @Bind(R.id.etManagementLevel)
    EditText mEtManagementLevel;

    @Bind(R.id.etOccupation)
    EditText mEtOccupation;


    RegistrationActivity mRegistrationActivity;

    private final String[] mManagementLevelPicker = {"Top Management(CEO / President / COO)", "Middle Management", "Supervisor", "Staff"};

    private final String[] mOccupationPicker = {"Business Owner / Entrepreneur", "Retailer", "Professional Service(Provider / Consultant)", "Academe",
            "Student", "Overseas Filipino / OF Family", "Medical Practitioners", "Retiree / Veteran", "Employee", ""};

    private TextWatcher mTextWatcher;

    public static RegistrationFourthFragment newInstance() {

        Bundle args = new Bundle();
        RegistrationFourthFragment fragment = new RegistrationFourthFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.registration_fourth_page_new, container, false);

        mRegistrationActivity = (RegistrationActivity) getActivity();

        ButterKnife.bind(this, view);

        initialize();
        return view;
    }

    private void initialize() {

        mEtCompany.addTextChangedListener(setTextWatcher(mEtCompany));
        mEtPhone.addTextChangedListener(setTextWatcher(mEtPhone));

        mEtManagementLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(getActivity(), "Management Level", mManagementLevelPicker, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEtManagementLevel.setText(mManagementLevelPicker[which]);
                        mRegistrationActivity.mPositionTitleStr = mManagementLevelPicker[which];
                    }
                }).show();
            }
        });

        mEtOccupation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PFABase.dialogPicker(getActivity(), "Occupation", mOccupationPicker, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEtOccupation.setText(mOccupationPicker[which]);
                        mRegistrationActivity.mOccupationStr = mOccupationPicker[which];
                    }
                }).show();
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private TextWatcher setTextWatcher(final EditText editText){

        mTextWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                switch (editText.getId()) {

                    case R.id.etCompany:
                        mRegistrationActivity.mCompanyStr = "" + s;
                        break;

                    case R.id.etPhone:
                        mRegistrationActivity.mPhoneStr = "" + s;
                        break;


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        return mTextWatcher;
    }
}
