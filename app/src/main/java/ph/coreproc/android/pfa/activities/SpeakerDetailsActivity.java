package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;
import database.db.speaker_local_tbl;
import database.db.speaker_local_tblDao;
import io.techery.properratingbar.ProperRatingBar;
import io.techery.properratingbar.RatingListener;
import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Speaker;
import ph.coreproc.android.pfa.models.Topics;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.offlinemodels.SpeakerLocal;
import ph.coreproc.android.pfa.models.offlinemodels.TopicLocal;
import ph.coreproc.android.pfa.models.requests.CommentBody;
import ph.coreproc.android.pfa.models.requests.RatingBody;
import ph.coreproc.android.pfa.rest.SpeakerInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import ph.coreproc.android.pfa.utils.TextViewEx;
import ph.coreproc.android.pfa.utils.UiUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpeakerDetailsActivity extends BaseActivity {

    public static String ARGS_ID = "ARGS_ID";
    public static String ARGS_IS_LOCAL = "ARGS_IS_LOCAL";

    private boolean isLocal;
    private String mSpeakerId;

    @Bind(R.id.cardViewAbout)
    CardView mCardViewAbout;

    @Bind(R.id.speakerImageView)
    ImageView speakerImageView;

    @Bind(R.id.speakerNameTextView)
    TextView speakerNameTextView;

    @Bind(R.id.speakerCompanyTextView)
    TextView speakerCompanyTextView;

    @Bind(R.id.speakerPositionTextView)
    TextView speakerPositionTextView;

    @Bind(R.id.speakerDescriptionTextView)
    TextView speakerDescriptionTextView;

    @Bind(R.id.llTopicContainer)
    LinearLayout mLlTopicContainer;

//    @Bind(R.id.appbar)
//    AppBarLayout mAppbar;

//    @Bind(R.id.collapsingToolbar)
//    CollapsingToolbarLayout mCollapsingToolbar;

    @Bind(R.id.favoriteButton)
    Button mFavoriteButton;

    @Bind(R.id.submitButton)
    Button mSubmitButton;

    @Bind(R.id.upperRatingBar)
    ProperRatingBar mUpperRatingBar;

    @Bind(R.id.etComment)
    EditText mEtComment;

    @Bind(R.id.createQuestion)
    Button mCreateQuestion;

    @Bind(R.id.llFooter)
    LinearLayout mLlFooter;

    @Bind(R.id.rlProfileInfo)
    RelativeLayout mRlProfileInfo;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.tvRate)
    TextView mTvRate;

    //    private SpeakerLocal mSpeakerLocal;
    private ArrayList<Speaker> mSpeakerLocals;
    private ArrayList<TopicLocal> mTopicLocalArrayList;
    private Speaker mSpeaker;
    private ArrayList<Topics> mTopicsArrayList;
    private speaker_local_tblDao mSpeaker_local_tblDao;
    private speaker_local_tbl mSpeaker_local_tbl;
    private Call<ResponseBody> mPostRateCall;
    private RatingBody mRatingBody;
    private User mUser;

    public static Intent newIntent(Context context, String id, boolean isLocal) {
        Intent intent = new Intent(context, SpeakerDetailsActivity.class);
        intent.putExtra(ARGS_ID, id);
        intent.putExtra(ARGS_IS_LOCAL, isLocal);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        mSpeaker_local_tblDao = DataUtil.setUpSpeakerLocal(mContext, Prefs.DB_NAME);
        mSpeakerId = getIntent().getStringExtra(ARGS_ID);
        isLocal = getIntent().getExtras().getBoolean(ARGS_IS_LOCAL);
        mUpperRatingBar.setListener(ratingListener);
        mUpperRatingBar.toggleClickable();
        initialize();
    }


    private void initialize() {
        mCreateQuestion.setVisibility(View.GONE);
        String response = Preferences.getString(mContext, Prefs.USER);
        User user = ModelUtil.fromJson(User.class, response);
        if (user.role.equals("exhibitor")) {
            mLlFooter.setVisibility(View.GONE);
        }
        if (!isLocal) {
            getSpeaker();
            return;
        }
        setFields(getSpeakerLocal());


    }


//    private void setFieldsLocal(SpeakerLocal speakerLocal) {
//
//        mSpeakerLocal = speakerLocal;
//
//        if (checkItemToLocal(mSpeakerLocal) != null) {
//            mFavoriteButton.setText("Unfavorite");
//        }
//
//        mTopicsArrayList = new ArrayList<>();
//        speakerNameTextView.setText(speakerLocal.speakerName.replace("MR.", "").replace("MS.", ""));
//        speakerCompanyTextView.setText(speakerLocal.speakerCompany);
//        speakerPositionTextView.setText(speakerLocal.speakerTitle);
//        speakerDescriptionTextView.setText(Html.fromHtml(speakerLocal.description));
//
//        ((TextViewEx) speakerDescriptionTextView).setText(speakerDescriptionTextView.getText().toString(), true);
//        speakerDescriptionTextView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
//        speakerDescriptionTextView.setPadding(5, 0, 0, 0);
//        mTopicLocalArrayList = speakerLocal.speakerTopic.speakerTopics;
////        mLlFooter.setVisibility(View.GONE);
//        for (int i = 0; mTopicLocalArrayList.size() > i; i++) {
//            mLlTopicContainer.addView(mTopicLocalArrayList.get(i).getView(mContext, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            }, i == mTopicLocalArrayList.size() - 1));
//        }
//
//        mAppbar.requestFocus();
//        mAppbar.setExpanded(true, true);
//
//    }

    private void setFields(Speaker speaker) {
        mSpeaker = speaker;

        if (checkItemToLocal(mSpeaker) != null) {
            mFavoriteButton.setText("Unfavorite");
        }

        try {
            mCardViewAbout.setVisibility(speaker.description.trim().equals("") ? View.GONE:View.VISIBLE);
        }catch (Exception e) {
            mCardViewAbout.setVisibility(View.GONE);
        }

        mTopicsArrayList = new ArrayList<>();
        try {
            speakerNameTextView.setText(speaker.speakerName.replace("MR.", "").replace("MS.", ""));
        }catch (Exception e) {
//            do nothing.
        }
        speakerCompanyTextView.setText(speaker.speakerCompany);
        speakerPositionTextView.setText(speaker.speakerTitle);
        speakerDescriptionTextView.setText(Html.fromHtml(speaker.description + ""));
        ((TextViewEx) speakerDescriptionTextView).setText(speakerDescriptionTextView.getText().toString(), true);
        mTvRate.setText("Rate this Speaker");
        mTopicsArrayList = speaker.speakerTopic.speakerTopics;
        mUpperRatingBar.setRating(mSpeaker.rating);
        for (int i = 0; mTopicsArrayList.size() > i; i++) {

            final int finalI = i;
            mLlTopicContainer.addView(mTopicsArrayList.get(i).getView(mContext, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(ConferenceDetailsActivity.newIntent(mContext, "" + mTopicsArrayList.get(finalI).id, false, true));
                }
            }, i == mTopicsArrayList.size() - 1));

        }
        if (speaker.avatar != null) {
            Glide.with(mContext)
                    .load(speaker.avatar)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_download_conference)
                    .fitCenter()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            Log.d("error loading image", "error " + e.getMessage());
                            return false;
                        }


                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            speakerImageView.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(speakerImageView);
        }

//        mAppbar.requestFocus();
//        mAppbar.setExpanded(true, true);


    }

    private Integer checkItemToLocal(Speaker speaker) {
        mSpeakerLocals = new ArrayList<>(DataUtil.getSpeakerLocal(mSpeaker_local_tblDao, mUser.id, mUser.role));
//        Speaker speakerLocal = new SpeakerLocal(speaker);
        Integer isExistedToLocal = DataUtil.checkSpeakerIfExist(mSpeakerLocals, mSpeaker);

        return isExistedToLocal;
    }

    private Integer checkItemToLocal(SpeakerLocal speakerLocal) {
        mSpeakerLocals = new ArrayList<>(DataUtil.getSpeakerLocal(mSpeaker_local_tblDao, mUser.id, mUser.role));
        Integer isExistedToLocal = DataUtil.checkSpeakerIfExist(mSpeakerLocals, mSpeaker);

        return isExistedToLocal;
    }

    private void getSpeaker() {

        final Dialog dialog = PFABase.loadDefaultProgressDialogCancellable(mContext, this);
        dialog.show();

        SpeakerInterface speakerInterface = KitchenRestClient.create(mContext, SpeakerInterface.class, true);
        Call<DataWrapper<Speaker>> speakerResponseCall = speakerInterface.getSpeaker(getString(R.string.get_speakers_url) + "/" + mSpeakerId);
        speakerResponseCall.enqueue(new Callback<DataWrapper<Speaker>>() {
            @Override
            public void onResponse(Call<DataWrapper<Speaker>> call, Response<DataWrapper<Speaker>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                setFields(response.body().getData());

            }

            @Override
            public void onFailure(Call<DataWrapper<Speaker>> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
            }
        });

    }

    private Speaker getSpeakerLocal() {

        mSpeakerLocals = new ArrayList<>(DataUtil.getSpeakerLocal(mSpeaker_local_tblDao, mUser.id, mUser.role));

        for (Speaker speaker : mSpeakerLocals) {
            if (mSpeakerId.equals("" + speaker.id)) {
                mSpeaker = speaker;
            }
        }

        return mSpeaker;
    }

    @Override
    protected void onResume() {
        super.onResume();
        PFABase.hideSoftKeyboard(this);
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_speaker_details;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveString(String saveString) {

        mSpeaker_local_tbl = new speaker_local_tbl(saveString);
        Log.i("tag", "results :" + saveString);
        mSpeaker_local_tblDao.insert(mSpeaker_local_tbl);
        mFavoriteButton.setText("Unfavorite");
//        Toast.makeText(mContext, "Saved to Favorites", Toast.LENGTH_SHORT).show();
    }

    private void deleteFavorite(int index) {
        ArrayList<Long> ids = DataUtil.getSpeakerId(mSpeaker_local_tblDao);
        mSpeaker_local_tbl = mSpeaker_local_tblDao.load(ids.get(index));
        mSpeaker_local_tblDao.delete(mSpeaker_local_tbl);
        mFavoriteButton.setText("Favorite");
    }

    @OnClick({R.id.favoriteButton, R.id.tvCommentSubmit, R.id.tvRateSubmit, R.id.createQuestion})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.favoriteButton:

                ArrayList<Speaker> speakerLocalArrayList =
                        new ArrayList<>(DataUtil.getSpeakerLocal(mSpeaker_local_tblDao, mUser.id, mUser.role));
                SpeakerLocal speakerLocalAdd;
//                if (mSpeaker != null) {
//                    speakerLocalAdd = new SpeakerLocal(mSpeaker);
//                } else {
//                    speakerLocalAdd = mSpeakerLocal;
//                }

                Integer isExist = DataUtil.checkSpeakerIfExist(speakerLocalArrayList, mSpeaker);

                if (isExist == null) {
//
//                    JsonArray jsonArrayTopic = new JsonArray();
//                    for (TopicLocal topicLocal : speakerLocalAdd.speakerTopic.speakerTopics) {
//                        jsonArrayTopic.add(topicLocal.getJsonObject());
//                    }
//                    SpeakerTopicLocal speakerTopicLocal = new SpeakerTopicLocal(jsonArrayTopic);

                    mSpeaker.userId = mUser.id;
                    mSpeaker.userRole = mUser.role;

                    saveString(ModelUtil.toJsonString(mSpeaker));

                    return;
                }

                deleteFavorite(isExist);


                break;
            case R.id.tvCommentSubmit:
                String comment = mEtComment.getText().toString();
                if (comment.equals("")) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up first the comment box.");
                    return;
                }
                PFABase.hideSoftKeyboard(SpeakerDetailsActivity.this);
                sendComment(comment);
                break;

            case R.id.tvRateSubmit:
                rateFunction();
                break;

            case R.id.createQuestion:
                startActivity(QuestionDisplayActivity.newIntent(mContext, Integer.parseInt(mSpeakerId), "Speaker"));
                break;
        }
    }

    private RatingListener ratingListener = new RatingListener() {
        @Override
        public void onRatePicked(ProperRatingBar ratingBar) {
            mRatingBody = new RatingBody(ratingBar.getRating());

        }

    };


    private void rateFunction() {

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();

        SpeakerInterface speakerInterface = KitchenRestClient.create(mContext, SpeakerInterface.class, true);

        mPostRateCall = speakerInterface.postSpeakerRating(mSpeaker.id + "", mRatingBody);
        mPostRateCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    mUpperRatingBar.setRating(mSpeaker.rating);
                    return;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                try {
                    mUpperRatingBar.setRating(mSpeaker.rating);
                } catch (Exception e) {
                    mUpperRatingBar.setRating(0);
                }
            }
        });

    }

    private void sendComment(String comment) {

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();

        SpeakerInterface speakerInterface = KitchenRestClient.create(mContext, SpeakerInterface.class, true);
        CommentBody commentBody = new CommentBody(comment);

        Call<ResponseBody> commentRequest = speakerInterface.postSpeakerComment(mSpeakerId, commentBody);
        commentRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                UiUtil.showMessageDialog(getSupportFragmentManager(), "Comment Sent");
                mEtComment.setText("");


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
            }
        });

    }

}
