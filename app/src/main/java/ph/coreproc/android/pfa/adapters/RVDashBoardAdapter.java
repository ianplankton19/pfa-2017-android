package ph.coreproc.android.pfa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.DashBoardCategory;
import ph.coreproc.android.pfa.models.User;

/**
 * Created by IanBlanco on 3/27/2017.
 */

public class RVDashBoardAdapter extends RecyclerView.Adapter<RVDashBoardAdapter.ViewHolder> {


    public interface Callback {
        void intentLocator(String category);
    }


    private Context mContext;
    private List<DashBoardCategory> mDashBoardCategories;
    private Callback mCallback;
    private User mUSer;


    public RVDashBoardAdapter(Context context, List<DashBoardCategory> dashBoardCategories, Callback callback, User user) {
        mContext = context;
        mDashBoardCategories = dashBoardCategories;
        mCallback = callback;
        mUSer = user;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_dashboard_category, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DashBoardCategory dashBoardCategory = mDashBoardCategories.get(position);

        holder.mTvTitle.setText(dashBoardCategory.getCategoryName());

//        Getting resource by string

        String resource = dashBoardCategory.getCategoryResource();
        if (mUSer.role.toLowerCase().equals("exhibitor")) {
            if (dashBoardCategory.getCategoryResource().equals("ic_logo_favorites") ||
                    dashBoardCategory.getCategoryResource().equals("ic_logo_qr_code")) {
                resource = resource + "_gray";
            }

        }
        int resourceId = mContext.getResources().getIdentifier(resource,
                "drawable", mContext.getPackageName());

//        Drawable color = new ColorDrawable(getRes)
            holder.mIvIcon.setImageResource(resourceId);
            Glide.with(mContext).
                    load(resourceId).
                    into(holder.mIvIcon);




        holder.mLlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.intentLocator(dashBoardCategory.getCategoryName());
            }
        });

    }


    @Override
    public int getItemCount() {
        return mDashBoardCategories.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.ivIcon)
        ImageView mIvIcon;

        @Bind(R.id.tvTitle)
        TextView mTvTitle;

        @Bind(R.id.llContainer)
        LinearLayout mLlContainer;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
