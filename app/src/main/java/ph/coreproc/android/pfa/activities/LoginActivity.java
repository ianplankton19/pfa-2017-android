package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;
import com.google.gson.JsonObject;

import butterknife.Bind;
import butterknife.OnClick;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.rest.UserInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by johneris on 6/16/2015.
 */
public class LoginActivity extends BaseActivity {


    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    @Bind(R.id.etUsername)
    EditText etUsername;

    @Bind(R.id.etPassword)
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_dark);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.btnLogin, R.id.forgotPassword})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                login();
                break;
            case R.id.forgotPassword:
                Intent intent = ForgotPasswordActivity.newIntent(mContext);
                startActivity(intent);
                break;
        }
    }

    public void login() {

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        String username = etUsername.getText().toString().trim();
        String password = etPassword.getText().toString();

        if (username.length() == 0 || password.length() == 0) {
            KitchenUiUtils.showAlertDialog(mContext, getString(R.string.error_field_required));
            return;
        }

        if (!PFABase.isEmailValid(username)) {
            etUsername.setError("Invalid e-mail address.");
            return;
        }


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("email", username);
        jsonObject.addProperty("password", password);
        UserInterface userInterface = KitchenRestClient.create(mContext, UserInterface.class, false);
        Call<DataWrapper<User>> userCall = userInterface.loginUser(getString(R.string.login_url), jsonObject);


        dialog.show();
        userCall.enqueue(new Callback<DataWrapper<User>>() {
            @Override
            public void onResponse(Call<DataWrapper<User>> call, Response<DataWrapper<User>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {
                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                Preferences.setString(mContext, Preferences.API_KEY, "" + response.headers().get("X-Authorization"));
                Preferences.setString(mContext, Prefs.USER_ID, "" + response.body().getData().id);

//                Json Users
                User user = response.body().getData();
//                JsonObject jsonObject1 = new JsonObject();
//                jsonObject1.addProperty("id", user.id);
//                jsonObject1.addProperty("full_name", user.fullName);
//                jsonObject1.addProperty("email", user.email);
//                jsonObject1.addProperty("gender", user.gender);
//                jsonObject1.addProperty("company", user.company);
//                jsonObject1.addProperty("position", user.position);
//                jsonObject1.addProperty("address", user.address);
//                jsonObject1.addProperty("region_id", user.regionId);
//                jsonObject1.addProperty("mobile", user.mobile);
//                jsonObject1.addProperty("phone", user.phone);
//                jsonObject1.addProperty("qr_code", user.qrCode);

                Log.i("tag", "userJson: " + ModelUtil.toJsonString(user));

                Preferences.setString(mContext, Prefs.USER, ModelUtil.toJsonString(user));
//               -------------------------------------------------------- //

                ActivityCompat.finishAffinity(LoginActivity.this);
                startActivity(DashBoardActivity.newIntent(mContext));
                finish();
            }

            @Override
            public void onFailure(Call<DataWrapper<User>> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
            }
        });
    }
}
