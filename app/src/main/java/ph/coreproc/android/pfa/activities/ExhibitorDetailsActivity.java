package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;
import com.ivankocijan.magicviews.views.MagicTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import database.db.exhibitor_local_tbl;
import database.db.exhibitor_local_tblDao;
import database.db.exhibitor_scan_local_tbl;
import database.db.exhibitor_scan_local_tblDao;
import io.techery.properratingbar.ProperRatingBar;
import io.techery.properratingbar.RatingListener;
import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.offlinemodels.ExhibitorLocal;
import ph.coreproc.android.pfa.models.requests.CommentBody;
import ph.coreproc.android.pfa.models.requests.QrRequestId;
import ph.coreproc.android.pfa.models.requests.RatingBody;
import ph.coreproc.android.pfa.rest.ExhibitorInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import ph.coreproc.android.pfa.utils.TextViewEx;
import ph.coreproc.android.pfa.utils.UiUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExhibitorDetailsActivity extends BaseActivity {

    public static String ARGS_EXHIBITOR_ID = "ARGS_EXHIBITOR_ID";
    public static String ARGS_IS_LOCAL = "ARGS_IS_LOCAL";
    public static String ARGS_RESPONSE = "ARGS_RESPONSE";
    public static String ARGS_IS_BY_QR = "ARGS_IS_BY_QR";
    public static String ARGS_QR_RESULT = "ARGS_QR_RESULT";



    public static Intent newIntent(Context context, int id, boolean isLocal) {
        Intent intent = new Intent(context, ExhibitorDetailsActivity.class);
        intent.putExtra(ARGS_EXHIBITOR_ID, id);
        intent.putExtra(ARGS_IS_LOCAL, isLocal);
        return intent;
    }

    public static Intent newIntent(Context context, boolean byQr, boolean isLocal, int qrResult, Exhibitor exhibitorLocal) {
        Intent intent = new Intent(context, ExhibitorDetailsActivity.class);
        intent.putExtra(ARGS_IS_BY_QR, byQr);
        intent.putExtra(ARGS_IS_LOCAL, isLocal);
        intent.putExtra(ARGS_RESPONSE, ModelUtil.toJsonString(exhibitorLocal));
        intent.putExtra(ARGS_QR_RESULT, qrResult);
        return intent;
    }


    @Bind(R.id.cardViewDetails)
    CardView mCardViewDetails;

    @Bind(R.id.llSector)
    LinearLayout mLlSector;

    @Bind(R.id.llConactPerson)
    LinearLayout mLlConactPerson;

    @Bind(R.id.llContactNumber)
    LinearLayout mLlContactNumber;

    @Bind(R.id.llDesignation)
    LinearLayout mLlDesignation;

    @Bind(R.id.llWebsite)
    LinearLayout mLlWebsite;

    @Bind(R.id.llEmail)
    LinearLayout mLlEmail;

    @Bind(R.id.llBoothNumber)
    LinearLayout mLlBoothNumber;

    @Bind(R.id.llBusinessLine)
    LinearLayout mLlBusinessLine;

    @Bind(R.id.llFranchisePackage)
    LinearLayout mLlFranchisePackage;

    @Bind(R.id.llFranchiseFee)
    LinearLayout mLlFranchiseFee;

    @Bind(R.id.llRoiPeriod)
    LinearLayout mLlRoiPeriod;

    @Bind(R.id.llTermOfFranchise)
    LinearLayout mLlTermOfFranchise;

    @Bind(R.id.llRenewal)
    LinearLayout mLlRenewal;

    @Bind(R.id.llRoyaltyFee)
    LinearLayout mLlRoyaltyFee;

    @Bind(R.id.llAdvertisingFee)
    LinearLayout mLlAdvertisingFee;

    @Bind(R.id.llNoOfStores)
    LinearLayout mLlNoOfStores;

    @Bind(R.id.llCompanyOwned)
    LinearLayout mLlCompanyOwned;

    @Bind(R.id.llFranchiseStores)
    LinearLayout mLlFranchiseStores;

    @Bind(R.id.llFranchiseSince)
    LinearLayout mLlFranchiseSince;

    @Bind(R.id.llYearStarted)
    LinearLayout mLlYearStarted;

    @Bind(R.id.llInvestmentLevel)
    LinearLayout mLlInvestmentLevel;

    @Bind(R.id.llTotalInvestmentLevel)
    LinearLayout mLlTotalInvestmentLevel;

    @Bind(R.id.rlExhibitorDetailsContent)
    RelativeLayout mRlExhibitorDetailsContent;

    @Bind(R.id.exhibitorImageView)
    ImageView exhibitorImageView;

    @Bind(R.id.exhibitorBoothNumberTextView)
    TextView mExhibitorBoothNumberTextView;

    @Bind(R.id.exhibitorContactTextView)
    TextView mExhibitorContactTextView;

    @Bind(R.id.exhibitorFranchiseTextView)
    TextView mExhibitorFranchiseTextView;

    @Bind(R.id.exhibitorNameTextView)
    MagicTextView mExhibitorNameTextView;

    @Bind(R.id.exhibitorCompanyTextView)
    MagicTextView mExhibitorCompanyTextView;

    @Bind(R.id.exhibitorPositionTextView)
    MagicTextView mExhibitorPositionTextView;

    @Bind(R.id.rlProfileInfo)
    RelativeLayout mRlProfileInfo;

    @Bind(R.id.upperRatingBar)
    ProperRatingBar mUpperRatingBar;

    @Bind(R.id.favoriteButton)
    Button mFavoriteButton;

    @Bind(R.id.submitButton)
    Button mSubmitButton;

    @Bind(R.id.llFooter)
    LinearLayout mLlFooter;

//    @Bind(R.id.toolbar)
//    Toolbar mToolbar;

//    @Bind(R.id.appbar)
//    AppBarLayout mAppbar;

    @Bind(R.id.fabNotes)
    FloatingActionButton mFabNotes;

    @Bind(R.id.etComment)
    EditText mEtComment;

    @Bind(R.id.tvRate)
    TextView mTvRate;

    @Bind(R.id.createQuestion)
    Button mCreateQuestion;

    @Bind(R.id.exhibitorContactPerson)
    TextView mExhibitorContactPerson;

    @Bind(R.id.exhibitorSector)
    TextView mExhibitorSector;

    @Bind(R.id.exhibitorDesignationTextView)
    TextView mExhibitorDesignationTextView;

    @Bind(R.id.exhibitorWebsiteTextView)
    TextView mExhibitorWebsiteTextView;

    @Bind(R.id.exhibitorEmailTextView)
    TextView mExhibitorEmailTextView;

    @Bind(R.id.exhibitorBusinessLineTextView)
    TextView mExhibitorBusinessLineTextView;

    @Bind(R.id.exhibitorFranchisePackageTextView)
    TextView mExhibitorFranchisePackageTextView;

    @Bind(R.id.exhibitorFranchiseFeeTextView)
    TextView mExhibitorFranchiseFeeTextView;

    @Bind(R.id.exhibitorRoiPeriodTextView)
    TextView mExhibitorRoiPeriodTextView;

    @Bind(R.id.exhibitorTermOfFranchiseTextView)
    TextView mExhibitorTermOfFranchiseTextView;

    @Bind(R.id.exhibitorRenewalTextView)
    TextView mExhibitorRenewalTextView;


    @Bind(R.id.exhibitorRoyaltyFeeTextView)
    TextView mExhibitorRoyaltyFeeTextView;

    @Bind(R.id.exhibitorAdvertisingFeeTextView)
    TextView mExhibitorAdvertisingFeeTextView;

    @Bind(R.id.exhibitorNoOfStoresTextView)
    TextView mExhibitorNoOfStoresTextView;

    @Bind(R.id.exhibitorCompanyOwnedTextView)
    TextView mExhibitorCompanyOwnedTextView;

    @Bind(R.id.exhibitorFranchiseStoresTextView)
    TextView mExhibitorFranchiseStoresTextView;

    @Bind(R.id.exhibitorFranchiseSinceTextView)
    TextView mExhibitorFranchiseSinceTextView;

    @Bind(R.id.exhibitorYearStartedTextView)
    TextView mExhibitorYearStartedTextView;

    @Bind(R.id.exhibitorInvestmentLevelTextView)
    TextView mExhibitorInvestmentLevelTextView;

    @Bind(R.id.exhibitorTotalInvestmentLevelTextView)
    TextView mExhibitorTotalInvestmentLevelTextView;


    private int mExhibitorId;
    private String mExhibitorName;
    private boolean mIsLocal;
    private boolean mByQrCode;
    private int mQrResult;
    private Exhibitor mExhibitor;
    private String mQrResponse = "";
    //    private ExhibitorLocal mExhibitorLocal;
    private Call<ResponseBody> mPostRateCall;

    private User mUser;


    private RatingBody mRatingBody;

    private exhibitor_local_tblDao mExhibitor_local_tblDao;
    private exhibitor_scan_local_tblDao mExhibitor_scan_local_tblDao;
    private exhibitor_local_tbl mExhibitor_local_tbl;
    private ArrayList<Exhibitor> mExhibitorLocalArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mExhibitor_local_tblDao = DataUtil.setUpExhibitorLocal(mContext, Prefs.DB_NAME);
        mExhibitor_scan_local_tblDao = DataUtil.setUpExhibitorScannedLocal(mContext, Prefs.DB_NAME);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mUpperRatingBar.setListener(ratingListener);
        mUpperRatingBar.toggleClickable();

//        mAppbar.requestFocus();
        initialize();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_exhibitor_details;
    }


    private void initialize() {

        Bundle extras = getIntent().getExtras();
        mExhibitorId = extras.getInt(ARGS_EXHIBITOR_ID);
        mIsLocal = extras.getBoolean(ARGS_IS_LOCAL);
        mByQrCode = extras.getBoolean(ARGS_IS_BY_QR);
        mQrResponse = extras.getString(ARGS_RESPONSE);
        mQrResult = extras.getInt(ARGS_QR_RESULT);
        mCreateQuestion.setVisibility(View.GONE);

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        if (mUser.role.equals("exhibitor")) {
            mLlFooter.setVisibility(View.GONE);
        }


        if (!mIsLocal) {

            getData(mExhibitorId);
            return;
        }
//        if OffLine
        if (mByQrCode) {

            mExhibitor = ModelUtil.fromJson(Exhibitor.class, mQrResponse);
            if (mExhibitor != null) {
//                setUpFieldsLocal(exhibitorLocal);
                getData(mExhibitor.id);
                QrRequestId qrRequestId = new QrRequestId(mExhibitor.id);
                sendExhibitorId(qrRequestId);
//                return;
//            }
//            if no data get in Local JSON FILE
//            mIsLocal = false;
//            getData(mQrResult);
                return;
            }
//            setUpFieldsLocal(getDataLocal());
        }
//        mExhibitor = getDataLocal();
//        setUpFields();
        getData(mExhibitorId);
    }


    private RatingListener ratingListener = new RatingListener() {
        @Override
        public void onRatePicked(ProperRatingBar ratingBar) {
            mRatingBody = new RatingBody(ratingBar.getRating());
        }
    };

    private void rateFunction() {
        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();

        ExhibitorInterface exhibitorInterface = KitchenRestClient.create(mContext, ExhibitorInterface.class, true);
        mPostRateCall = exhibitorInterface.postExhibitorsRating(mExhibitorId + "", mRatingBody);
        mPostRateCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    mUpperRatingBar.setRating(mExhibitor.rating);
                    return;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                try {
                    mUpperRatingBar.setRating(mExhibitor.rating);
                } catch (Exception e) {
                    mUpperRatingBar.setRating(0);
                }
            }
        });

    }

    private void getData(int exhibitorId) {

        mExhibitor = PFABase.getSpecificExhibitor(mContext, exhibitorId);
        if (mExhibitor != null) {
//            setUpFields();
        } else {
            PFABase.dialogWithOnClickCancelable(mContext, "Exhibitor does not exist", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }

        setUpFields();
        getRating(exhibitorId);
    }

    private void getRating(int exhibitorId) {
//        final Dialog dialog = PFABase.loadDefaultProgressDialogCancellable(mContext, this);
//        dialog.show();
        ExhibitorInterface exhibitorInterface = KitchenRestClient.create(mContext, ExhibitorInterface.class, true);
        Call<DataWrapper<Exhibitor>> exhibitorCall = exhibitorInterface.getExhibitor("" + exhibitorId);
        exhibitorCall.enqueue(new Callback<DataWrapper<Exhibitor>>() {
            @Override
            public void onResponse(Call<DataWrapper<Exhibitor>> call, Response<DataWrapper<Exhibitor>> response) {

//                dialog.dismiss();

                switch (response.code()) {
                    case 404:
                        PFABase.dialogWithOnClickCancelable(mContext, "Exhibitor does not exist", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        break;
                    case 401:
                        User.logout(mContext);
                        break;
                    case 200:
                        mExhibitor.rating = response.body().getData().rating;
                        mUpperRatingBar.setRating(response.body().getData().rating);
//                        setUpFields();
                        break;
                    default:
                        APIError error = ErrorUtil.parsingError(response);
                        KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
//                        setUpFields();
                        break;

                }
            }

            @Override
            public void onFailure(Call<DataWrapper<Exhibitor>> call, Throwable t) {
//                dialog.dismiss();
//                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));

//                PFABase.dialogWithOnClickCancelable(mContext, getString(R.string.dialog_error_global), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        if (mByQrCode) {
//                            finish();
//                        }
//                        setUpFields();
//                    }
//                });

            }
        });
    }

    private Exhibitor getDataLocal() {

        mExhibitorLocalArrayList = new ArrayList<>(DataUtil.getExhibitorLocal(mExhibitor_local_tblDao, mUser.id, mUser.role));
        Exhibitor exhibitorLocalToReturn = new Exhibitor();
        for (Exhibitor exhibitorLocal : mExhibitorLocalArrayList) {
            if (exhibitorLocal.id == mExhibitorId) {
                exhibitorLocalToReturn = exhibitorLocal;
            }
        }
        return exhibitorLocalToReturn;
    }

//    private void setUpFieldsLocal(ExhibitorLocal exhibitorLocal) {
//        mExhibitorLocal = exhibitorLocal;
////        mRlExhibitorDetailsContent.setVisibility(View.VISIBLE);
//        mEtComment.clearFocus();
////        mExhibitor = exhibitor;
//        mExhibitorId = exhibitorLocal.id;
//        mExhibitorName = exhibitorLocal.franchiseName;
//        if (mByQrCode) {
//            saveScannedExhibitor(exhibitorLocal);
//
//        }
//
//        if (checkItemToLocal(mExhibitorLocal) != null) {
//            mFavoriteButton.setText("Unfavorite");
//        }
//        hideTextViews();
//        mExhibitorNameTextView.setText(exhibitorLocal.franchiseName);
//        mExhibitorCompanyTextView.setText(exhibitorLocal.name);
//        mExhibitorBoothNumberTextView.setText(Html.fromHtml("<b>Booth Number: </b>" +
//                exhibitorLocal.boothNumber));
//        mExhibitorContactPerson.setText(Html.fromHtml("<b>Contact Person: </b>" + exhibitorLocal.contactPerson));
//        mExhibitorSector.setText(Html.fromHtml("<b>Sector: </b>" + exhibitorLocal.sector));
//        mExhibitorDesignationTextView.setText(Html.fromHtml("<b>Designation: </b>" + exhibitorLocal.designation));
//        mExhibitorWebsiteTextView.setText(Html.fromHtml("<b>Website: </b>" + exhibitorLocal.website));
//        mExhibitorEmailTextView.setText(Html.fromHtml("<b>Email: </b>" + exhibitorLocal.email));
//        mExhibitorBusinessLineTextView.setText(Html.fromHtml("<b>Business Line: </b>" + exhibitorLocal.businessLine));
//        mExhibitorFranchisePackageTextView.setText(Html.fromHtml("<b>Franchise Package: </b>" + exhibitorLocal.franchisePackage));
//        mExhibitorFranchiseFeeTextView.setText(Html.fromHtml("<b>Franchise Fee: </b>" + exhibitorLocal.franchiseFee));
//        mExhibitorRoiPeriodTextView.setText(Html.fromHtml("<b>ROI Period: </b>" + exhibitorLocal.roiPeriod));
//        mExhibitorTermOfFranchiseTextView.setText(Html.fromHtml("<b>Term of Franchise: </b>" + exhibitorLocal.termOfFranchise));
//        mExhibitorContactTextView.setText(Html.fromHtml("<b>Contact: </b>" +
//                exhibitorLocal.contactNo));
//        mExhibitorFranchiseTextView.setText(Html.fromHtml("<b>Description: </b>" + exhibitorLocal.description));
//        ((TextViewEx) mExhibitorFranchiseTextView).setText(mExhibitorFranchiseTextView.getText().toString(), true);
//        mExhibitorRenewalTextView.setText(Html.fromHtml("<b>Renewal: </b>" + exhibitorLocal.renewal));
//        mExhibitorRoyaltyFeeTextView.setText(Html.fromHtml("<b>Royalty Fee: </b>" + exhibitorLocal.royaltyFee));
//        mExhibitorAdvertisingFeeTextView.setText(Html.fromHtml("<b>Advertising Fee: </b>" + exhibitorLocal.advertisingFee));
//        mExhibitorNoOfStoresTextView.setText(Html.fromHtml("<b>No of Stores: </b>" + exhibitorLocal.noOfStores));
//        mExhibitorCompanyOwnedTextView.setText(Html.fromHtml("<b>Company Owned: </b>" + exhibitorLocal.companyOwned));
//        mExhibitorFranchiseStoresTextView.setText(Html.fromHtml("<b>Franchise Stores: </b>" + exhibitorLocal.franchiseStores));
//        mExhibitorFranchiseSinceTextView.setText(Html.fromHtml("<b>Franchise Since: </b>" + exhibitorLocal.franchisingSince));
//        mExhibitorYearStartedTextView.setText(Html.fromHtml("<b>Year Started: </b>" + exhibitorLocal.yearStarted));
//        mExhibitorInvestmentLevelTextView.setText(Html.fromHtml("<b>Investment Level: </b>" + exhibitorLocal.investmentLevel));
//        mExhibitorTotalInvestmentLevelTextView.setText(Html.fromHtml("<b>Total Investment Level: </b>" + exhibitorLocal.totalInvestmentLevel));
////        mFabNotes.setVisibility(View.GONE);
//        mUpperRatingBar.setRating(exhibitorLocal.rating);
//        setUpImage(mExhibitorLocal.avatar);
//    }


    private void setUpFields() {
        mRlExhibitorDetailsContent.setVisibility(View.VISIBLE);
//        mExhibitor = exhibitor;
        mExhibitorId = mExhibitor.id;
        mExhibitorName = mExhibitor.franchiseName;
        if (mByQrCode) {

            mExhibitor.userId = mUser.id;
            mExhibitor.userRole = mUser.role;

            saveScannedExhibitor(mExhibitor);
            QrRequestId qrRequestId = new QrRequestId(mExhibitorId);
            sendExhibitorId(qrRequestId);
        }

        if (checkItemToLocal(mExhibitor) != null) {
            mFavoriteButton.setText("Unfavorite");
        }
        hideTextViews();
        mExhibitorNameTextView.setText(mExhibitor.franchiseName);
        mExhibitorCompanyTextView.setText(mExhibitor.name);
        mExhibitorBoothNumberTextView.setText(mExhibitor.boothNumber);
        mExhibitorContactPerson.setText(mExhibitor.contactPerson);
        mExhibitorSector.setText(mExhibitor.sector);
        mExhibitorDesignationTextView.setText(mExhibitor.designation);
        mExhibitorWebsiteTextView.setText(mExhibitor.website);
        mExhibitorEmailTextView.setText(mExhibitor.email);
        mExhibitorBusinessLineTextView.setText(mExhibitor.businessLine);
        mExhibitorFranchisePackageTextView.setText(mExhibitor.franchisePackage);
        mExhibitorFranchiseFeeTextView.setText(mExhibitor.franchiseFee);
        mExhibitorRoiPeriodTextView.setText(mExhibitor.roiPeriod);
        mExhibitorTermOfFranchiseTextView.setText(mExhibitor.termOfFranchise);
        mExhibitorContactTextView.setText(mExhibitor.contactNo);
//        mExhibitorFranchiseTextView.setText(Html.fromHtml(mExhibitor.description));
        ((TextViewEx) mExhibitorFranchiseTextView).setText(mExhibitor.description, true);
        mExhibitorRenewalTextView.setText(mExhibitor.renewal);
        mExhibitorRoyaltyFeeTextView.setText(mExhibitor.royaltyFee);
        mExhibitorAdvertisingFeeTextView.setText(mExhibitor.advertisingFee);
        mExhibitorNoOfStoresTextView.setText(mExhibitor.noOfStores);
        mExhibitorCompanyOwnedTextView.setText(mExhibitor.companyOwned);
        mExhibitorFranchiseStoresTextView.setText(mExhibitor.franchiseStores);
        mExhibitorFranchiseSinceTextView.setText(mExhibitor.franchisingSince);
        mExhibitorYearStartedTextView.setText(mExhibitor.yearStarted);
        mExhibitorInvestmentLevelTextView.setText(mExhibitor.investmentLevel);
        mExhibitorTotalInvestmentLevelTextView.setText(mExhibitor.totalInvestmentLevel);
        mTvRate.setText("Rate this Exhibitor");
        mUpperRatingBar.setRating(mExhibitor.rating);
//        mAppbar.requestFocus();
        exhibitorImageView.requestFocus();
//        mAppbar.setExpanded(true, true);
        setUpImage(mExhibitor.avatar);

    }

    private void hideTextViews() {

        setIfVisible(mLlBoothNumber, mExhibitor.boothNumber);
        setIfVisible(mLlConactPerson, mExhibitor.contactPerson);
        setIfVisible(mLlSector, mExhibitor.sector);
        setIfVisible(mLlDesignation, mExhibitor.designation);
        setIfVisible(mLlWebsite, mExhibitor.website);
        setIfVisible(mLlEmail, mExhibitor.email);
        setIfVisible(mLlBusinessLine, mExhibitor.businessLine);
        setIfVisible(mLlFranchisePackage, mExhibitor.franchisePackage);
        setIfVisible(mLlFranchiseFee, mExhibitor.franchiseFee);
        setIfVisible(mLlRoiPeriod, mExhibitor.roiPeriod);
        setIfVisible(mLlTermOfFranchise, mExhibitor.termOfFranchise);
        setIfVisible(mLlContactNumber, mExhibitor.contactNo);
//        setIfVisible(mLlFranchisePackage, mExhibitor.description);
        setIfVisible(mLlRenewal, mExhibitor.renewal);
        setIfVisible(mLlRoyaltyFee, mExhibitor.royaltyFee);
        setIfVisible(mLlAdvertisingFee, mExhibitor.advertisingFee);
        setIfVisible(mLlNoOfStores, mExhibitor.noOfStores);
        setIfVisible(mLlCompanyOwned, mExhibitor.companyOwned);
        setIfVisible(mLlFranchiseStores, mExhibitor.franchiseStores);
        setIfVisible(mLlFranchiseSince, mExhibitor.franchisingSince);
        setIfVisible(mLlYearStarted, mExhibitor.yearStarted);
        setIfVisible(mLlInvestmentLevel, mExhibitor.investmentLevel);
        setIfVisible(mLlTotalInvestmentLevel, mExhibitor.totalInvestmentLevel);


        try {
            mCardViewDetails.setVisibility(mExhibitor.description.equals("") ? View.GONE : View.VISIBLE);
        }catch (Exception e) {
            mCardViewDetails.setVisibility(View.GONE);
        }
    }

    private void setIfVisible(LinearLayout linearLayout, String text) {
        try {
            linearLayout.setVisibility(text.equals("") ? View.GONE : View.VISIBLE);
        } catch (Exception e) {
            linearLayout.setVisibility(View.GONE);
        }
    }


    private void setUpImage(String url) {
        if (url != null) {
            Log.i("url", "url: " + url);
            Glide.with(mContext)
                    .load(url)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Log.d("error loading image", "erro " + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            exhibitorImageView.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(exhibitorImageView);
        }
    }

    private void saveScannedExhibitor(Exhibitor exhibitorLocal) {
        List<exhibitor_scan_local_tbl> exhibitor_scan_local_tbls = mExhibitor_scan_local_tblDao.queryBuilder().
                orderDesc(exhibitor_scan_local_tblDao.Properties.Id).build().list();
        ArrayList<Exhibitor> exhibitorLocals = new ArrayList<>();
        ArrayList<Long> ids = new ArrayList<>();
        if (exhibitor_scan_local_tbls.size() > 0) {

            for (exhibitor_scan_local_tbl exhibitorScanLocalTbl : exhibitor_scan_local_tbls) {

                String dataResponse = exhibitorScanLocalTbl.getSave_string();
                long id = exhibitorScanLocalTbl.getId();
                Exhibitor exhibitorLocal1 = ModelUtil.fromJson(Exhibitor.class, dataResponse);
                exhibitorLocals.add(exhibitorLocal1);

            }


            if (DataUtil.checkScanIfExist(exhibitorLocals, exhibitorLocal) != null) {
                //        Update Function
                String sameResponse = ModelUtil.toJsonString(exhibitorLocals.get(DataUtil.checkScanIfExist(exhibitorLocals, exhibitorLocal)));

                if (ModelUtil.toJsonString(exhibitorLocal).length() > sameResponse.length()) {
                    /**
                     * Comment Update for a while
                     */
//                    long updateId = ids.get(DataUtil.checkScanIfExist(exhibitorLocals, exhibitorLocal));
//                    exhibitor_scan_local_tbl exhibitorScanLocalTbl = mExhibitor_scan_local_tblDao.load(updateId);
//                    mExhibitor_scan_local_tblDao.update(exhibitorScanLocalTbl);
//                    exhibitorLocal = ModelUtil.fromJson(ExhibitorLocal.class, response);
                }
//        Nothing to do

            } else {

                exhibitor_scan_local_tbl exhibitorScanLocalTbl = new exhibitor_scan_local_tbl(ModelUtil.toJsonString(exhibitorLocal));
                mExhibitor_scan_local_tblDao.insert(exhibitorScanLocalTbl);
//                exhibitorLocal = ModelUtil.fromJson(ExhibitorLocal.class, response);
            }

            return;
        }

        exhibitor_scan_local_tbl exhibitorScanLocalTbl = new exhibitor_scan_local_tbl(ModelUtil.toJsonString(exhibitorLocal));
        mExhibitor_scan_local_tblDao.insert(exhibitorScanLocalTbl);
//        exhibitorLocal = ModelUtil.fromJson(ExhibitorLocal.class, response);
    }


    private Integer checkItemToLocal(Exhibitor exhibitor) {
        mExhibitorLocalArrayList = new ArrayList<>(DataUtil.getExhibitorLocal(mExhibitor_local_tblDao, mUser.id, mUser.role));
        ExhibitorLocal exhibitorLocal = new ExhibitorLocal(exhibitor);
        Integer isExistedToLocal = DataUtil.checkExhibitorIfExist(mExhibitorLocalArrayList, mExhibitor);

        return isExistedToLocal;
    }

    private Integer checkItemToLocal(ExhibitorLocal exhibitorLocal) {
        mExhibitorLocalArrayList = new ArrayList<>(DataUtil.getExhibitorLocal(mExhibitor_local_tblDao, mUser.id, mUser.role));
        Integer isExistedToLocal = DataUtil.checkExhibitorIfExist(mExhibitorLocalArrayList, mExhibitor);

        return isExistedToLocal;
    }

    @Override
    protected void onResume() {
        super.onResume();
        PFABase.hideSoftKeyboard(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.favoriteButton, R.id.tvCommentSubmit, R.id.tvRateSubmit, R.id.createQuestion})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.favoriteButton:


                mExhibitorLocalArrayList = new ArrayList<>(DataUtil.getExhibitorLocal(mExhibitor_local_tblDao, mUser.id, mUser.role));

                ExhibitorLocal exhibitorLocalAdd;

//                if (mExhibitor != null) {
//                    exhibitorLocalAdd = new ExhibitorLocal(mExhibitor);
//                } else {
//                    exhibitorLocalAdd = mExhibitorLocal;
//                }

                Integer isExist = DataUtil.checkExhibitorIfExist(mExhibitorLocalArrayList, mExhibitor);

                if (isExist == null) {
                    mExhibitorLocalArrayList.add(mExhibitor);

                    String response = Preferences.getString(mContext, Prefs.USER);
                    User user = ModelUtil.fromJson(User.class, response);

                    mExhibitor.userId = user.id;
                    mExhibitor.userRole = user.role;
                    saveString(ModelUtil.toJsonString(mExhibitor));
                    return;
                }
                deleteFavorite(isExist);
                break;
            case R.id.tvCommentSubmit:
                String comment = mEtComment.getText().toString();
                if (comment.equals("")) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up first the comment box.");
                    return;
                }
                sendComment(comment);
                PFABase.hideSoftKeyboard(ExhibitorDetailsActivity.this);
                break;

            case R.id.tvRateSubmit:

                rateFunction();
                break;

            case R.id.createQuestion:
                startActivity(QuestionDisplayActivity.newIntent(mContext, mExhibitorId, "Exhibitor"));
                break;
        }
    }


    private void saveString(String toSaveString) {
        mExhibitor_local_tbl = new exhibitor_local_tbl(toSaveString);
        Log.i("tag", "results :" + toSaveString);
        mExhibitor_local_tblDao.insert(mExhibitor_local_tbl);
        mFavoriteButton.setText("Unfavorite");
//        TriggerEventBus triggerEventBus = new TriggerEventBus(1);
//        EventBus.getDefault().postSticky(triggerEventBus);
//        Toast.makeText(mContext, "Saved to Favorites", Toast.LENGTH_SHORT).show();
    }

    private void deleteFavorite(int index) {
        ArrayList<Long> ids = DataUtil.getExhibitorId(mExhibitor_local_tblDao);

        mExhibitor_local_tbl = mExhibitor_local_tblDao.load(ids.get(index));
        mExhibitor_local_tblDao.delete(mExhibitor_local_tbl);
        mFavoriteButton.setText("Favorite");

//        TriggerEventBus triggerEventBus = new TriggerEventBus(1);
//        EventBus.getDefault().postSticky(triggerEventBus);
    }

    @OnClick(R.id.fabNotes)
    public void onClick() {
        startActivity(NotesActivity.newIntent(mContext,
                mExhibitorId, mExhibitorName, "Exhibitor"));

    }

    private void sendExhibitorId(QrRequestId qrRequestId) {

//        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
//        dialog.show();

        ExhibitorInterface exhibitorInterface = KitchenRestClient.create(mContext, ExhibitorInterface.class, true);

        Call<ResponseBody> exhibitorRequest = exhibitorInterface.postExhibitorScannedId(qrRequestId);
        exhibitorRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//            dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                dialog.dismiss();
            }
        });

    }

    private void sendComment(String comment) {

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();

        ExhibitorInterface exhibitorInterface = KitchenRestClient.create(mContext, ExhibitorInterface.class, true);
        CommentBody commentBody = new CommentBody(comment);


        Call<ResponseBody> commentRequest = exhibitorInterface.postExhibitorsComment(mExhibitorId + "", commentBody);
        commentRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }
                UiUtil.showMessageDialog(getSupportFragmentManager(), "Comment Sent");
                mEtComment.setText("");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
            }
        });


    }
}
