package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import butterknife.Bind;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.favorites.FavoritesViewPagerAdapter;
import ph.coreproc.android.pfa.fragments.favoritesfragment.FavoritesConferenceListFragment;
import ph.coreproc.android.pfa.fragments.favoritesfragment.FavoritesExhibitorsListFragment;
import ph.coreproc.android.pfa.fragments.favoritesfragment.FavoritesSeminarListFragment;
import ph.coreproc.android.pfa.fragments.favoritesfragment.FavoritesSpeakerListFragment;

public class FavoritesListActivity extends BaseActivity {


    @Bind(R.id.favoritesTabLayout)
    TabLayout mFavoritesTabLayout;

    @Bind(R.id.favoritesListViewPager)
    ViewPager mFavoritesListViewPager;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, FavoritesListActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();

    }

    private void initialize() {
        setViewPager();
        mFavoritesTabLayout.setupWithViewPager(mFavoritesListViewPager);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_favorites_list;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setViewPager() {

        FavoritesViewPagerAdapter adapter = new FavoritesViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(FavoritesConferenceListFragment.newInstance(), "Conference");
        adapter.addFragment(FavoritesSeminarListFragment.newInstance(), "Seminar");
        adapter.addFragment(FavoritesSpeakerListFragment.newInstance(), "Speaker");
        adapter.addFragment(FavoritesExhibitorsListFragment.newInstance(), "Exhibitor");
        mFavoritesListViewPager.setAdapter(adapter);
        mFavoritesListViewPager.setOffscreenPageLimit(adapter.getCount());
    }
}
