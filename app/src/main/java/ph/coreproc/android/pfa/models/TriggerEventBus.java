package ph.coreproc.android.pfa.models;

/**
 * Created by IanBlanco on 5/10/2017.
 */

public class TriggerEventBus {

    public TriggerEventBus(int trigger) {
        this.trigger = trigger;
    }

    public int trigger;

}
