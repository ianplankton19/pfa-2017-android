package ph.coreproc.android.pfa.models.offlinemodels;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ph.coreproc.android.pfa.models.Exhibitor;

/**
 * Created by IanBlanco on 4/17/2017.
 */

public class ExhibitorLocal {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("company_name")
    @Expose
    public String name;

    @SerializedName("sector")
    @Expose
    public String sector;

    @SerializedName("franchise_name")
    @Expose
    public String franchiseName;

    @SerializedName("contact_person")
    @Expose
    public String contactPerson;

    @SerializedName("contact_number")
    @Expose
    public String contactNo;

    @SerializedName("designation")
    @Expose
    public String designation;

    @SerializedName("website")
    @Expose
    public String website;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("description")
    @Expose
    public String description;


    @SerializedName("booth")
    @Expose
    public String boothNumber;

    @SerializedName("business_line")
    @Expose
    public String businessLine;

    @SerializedName("franchise_package")
    @Expose
    public String franchisePackage;

    @SerializedName("franchise_fee")
    @Expose
    public String franchiseFee;

    @SerializedName("roi_period")
    @Expose
    public String roiPeriod;

    @SerializedName("term_of_franchise")
    @Expose
    public String termOfFranchise;

    @SerializedName("renewal")
    @Expose
    public String renewal;

    @SerializedName("royalty_fee")
    @Expose
    public String royaltyFee;

    @SerializedName("advertising_fee")
    @Expose
    public String advertisingFee;

    @SerializedName("no_of_stores")
    @Expose
    public String noOfStores;

    @SerializedName("company_owned")
    @Expose
    public String companyOwned;

    @SerializedName("franchise_stores")
    @Expose
    public String franchiseStores;

    @SerializedName("franchising_since")
    @Expose
    public String franchisingSince;

    @SerializedName("year_started")
    @Expose
    public String yearStarted;

    @SerializedName("investment_level")
    @Expose
    public String investmentLevel;

    @SerializedName("total_investment_level")
    @Expose
    public String totalInvestmentLevel;


    @SerializedName("rating")
    @Expose
    public int rating;

    @SerializedName("logo")
    @Expose
    public String avatar;

    public ExhibitorLocal() {

    }


    public ExhibitorLocal(Exhibitor exhibitor) {

        this.id = exhibitor.id;
        this.name = exhibitor.name;
        this.sector = exhibitor.sector;
        this.boothNumber = exhibitor.boothNumber;
        this.franchiseName = exhibitor.franchiseName;
        this.contactPerson = exhibitor.contactPerson;
        this.contactNo = exhibitor.contactNo;
        this.designation = exhibitor.designation;
        this.website = exhibitor.website;
        this.email = exhibitor.email;
        this.description = exhibitor.description;
        this.franchisePackage = exhibitor.franchisePackage;
        this.businessLine = exhibitor.businessLine;
        this.franchiseFee = exhibitor.franchiseFee;
        this.roiPeriod = exhibitor.roiPeriod;
        this.termOfFranchise = exhibitor.termOfFranchise;
        this.renewal = exhibitor.renewal;
        this.royaltyFee = exhibitor.royaltyFee;
        this.advertisingFee = exhibitor.advertisingFee;
        this.noOfStores = exhibitor.noOfStores;
        this.companyOwned = exhibitor.companyOwned;
        this.franchiseStores = exhibitor.franchiseStores;
        this.franchisingSince =exhibitor.franchisingSince;
        this.yearStarted = exhibitor.yearStarted;
        this.investmentLevel = exhibitor.investmentLevel;
        this.totalInvestmentLevel = exhibitor.totalInvestmentLevel;
        this.avatar = exhibitor.avatar;

    }


    public JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", this.id);
        jsonObject.addProperty("company_name", this.name);
        jsonObject.addProperty("sector", this.sector);
        jsonObject.addProperty("booth", this.boothNumber);
        jsonObject.addProperty("franchise_name", this.franchiseName);
        jsonObject.addProperty("contact_person", this.contactPerson);
        jsonObject.addProperty("contact_number", this.contactNo);
        jsonObject.addProperty("designation", this.designation);
        jsonObject.addProperty("website", this.website);
        jsonObject.addProperty("email", this.email);
        jsonObject.addProperty("description", this.description);
        jsonObject.addProperty("franchise_package", this.franchisePackage);
        jsonObject.addProperty("business_line", this.businessLine);
        jsonObject.addProperty("franchise_fee", this.franchiseFee);
        jsonObject.addProperty("roi_period", this.roiPeriod);
        jsonObject.addProperty("term_of_franchise", this.termOfFranchise);
        jsonObject.addProperty("renewal", this.renewal);
        jsonObject.addProperty("royalty_fee", this.royaltyFee);
        jsonObject.addProperty("advertising_fee", this.advertisingFee);
        jsonObject.addProperty("no_of_stores", this.noOfStores);
        jsonObject.addProperty("company_owned", this.companyOwned);
        jsonObject.addProperty("franchise_stores", this.franchiseStores);
        jsonObject.addProperty("franchising_since", this.franchisingSince);
        jsonObject.addProperty("year_started", this.yearStarted);
        jsonObject.addProperty("investment_level", this.investmentLevel);
        jsonObject.addProperty("total_investment_level", this.totalInvestmentLevel);
        jsonObject.addProperty("logo", this.avatar);

        return jsonObject;
    }



}
