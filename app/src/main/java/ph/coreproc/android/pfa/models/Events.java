package ph.coreproc.android.pfa.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * Created by IanBlanco on 4/19/2017.
 */

public class Events {

    public ArrayList<Event> mEvents;

    public Events(JsonObject jsonObject){

        JsonObject jsonObjectEvents = jsonObject.get("events").getAsJsonObject();

        JsonArray jsonArrayCfe = jsonObjectEvents.get("cfe").getAsJsonArray();
        JsonArray jsonArrayConference = jsonObjectEvents.get("conference").getAsJsonArray();
        JsonArray jsonArrayExp = jsonObjectEvents.get("expo").getAsJsonArray();
        JsonArray jsonArraySeminars = jsonObjectEvents.get("seminars").getAsJsonArray();
    }


//    public Regions(JsonObject jsonObject){
//        JsonArray json = jsonObject.get("regions").getAsJsonArray();
//        mRegionArrayList = new ArrayList<>();
//        for (int i = 0; i < json.size(); i ++){
//
//            mRegionArrayList.add(new Region(json.get(i).getAsJsonObject()));
//
//        }
//
//    }
}
