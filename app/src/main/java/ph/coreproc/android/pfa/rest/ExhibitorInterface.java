package ph.coreproc.android.pfa.rest;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.requests.CommentBody;
import ph.coreproc.android.pfa.models.requests.DownloadBody;
import ph.coreproc.android.pfa.models.requests.QrRequestId;
import ph.coreproc.android.pfa.models.requests.QuestionBody;
import ph.coreproc.android.pfa.models.requests.RatingBody;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.rest.responses.MetaDataWrapper;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by willm on 3/30/2017.
 */

public interface ExhibitorInterface {

    @GET
    Call<MetaDataWrapper<ArrayList<Exhibitor>>> getExhibitors(@Url String url,
                                                              @Query("page") String pageStr,
                                                              @Query("q") String query,
                                                              @Query("sector") String sector,
                                                              @Query("investment_level") String investment);


    @GET("/api/v1/exhibitors/{id}")
    Call<DataWrapper<Exhibitor>> getExhibitor(@Path("id") String id);

    @POST("/api/v1/exhibitors/{id}/comments")
    Call<ResponseBody> postExhibitorsComment(@Path("id") String id, @Body CommentBody commentBody);

    @POST("/api/v1/exhibitors/{id}/ratings")
    Call<ResponseBody> postExhibitorsRating(@Path("id") String id, @Body RatingBody ratingBody);

    @POST("/api/v1/exhibitors/{id}/questions")
    Call<ResponseBody> postExhibitorQuestion(@Path("id") String id, @Body QuestionBody questionBody);

    @POST("/api/v1/exhibitors_scanner")
    Call<ResponseBody> postExhibitorScannedId(@Body QrRequestId qrRequestId);

    /**
     * Exclusive for exhibitor account
     */
    @GET("/api/v1/exhibitor_scanner")
    Call<DataWrapper<ArrayList<User>>> getUserScannedMe();

    @POST("/api/v1/exhibitors/download")
    Call<ResponseBody> postExhibitorEmail(@Body DownloadBody downloadBody);

}
