package ph.coreproc.android.pfa.adapters.notes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import database.db.notes_local_tblDao;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.fragments.notes.NotesExhibitor;
import ph.coreproc.android.pfa.models.offlinemodels.Notes;

/**
 * Created by IanBlanco on 4/6/2017.
 */

public class NotesExhibitorListAdapter extends RecyclerView.Adapter<NotesExhibitorListAdapter.ViewHolder> {


    public interface Callback extends DisplayNoDataInterface {
        void intentToSavedNote(int id, String noteName, String category);
        void showCheckBox(int id);
        void deleteMarkedNote(ArrayList<Integer> ids);
    }


    private ArrayList<Notes> mNotesArrayList;
    private Context mContext;
    private Callback mCallback;
    public ViewHolder mViewHolder;
    private NotesExhibitor mNotesExhibitor;
    private ArrayList<Long> mIds;
    private notes_local_tblDao mNotes_local_tblDao;

    public NotesExhibitorListAdapter(Context context, ArrayList<Notes> notesArrayList, NotesExhibitor notesExhibitor, ArrayList<Long> ids,
                                     notes_local_tblDao notes_local_tblDao, Callback callback) {
        mContext = context;
        ArrayList<Notes> noteToAdd = new ArrayList<>();
        for (Notes notes : notesArrayList) {
            if (notes.category.equals("Exhibitor")) {
                noteToAdd.add(notes);
            }
        }
        mNotesArrayList = noteToAdd;
        mNotesExhibitor = notesExhibitor;
        mCallback = callback;
        mIds = ids;
        mNotes_local_tblDao = notes_local_tblDao;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_notes_item, parent, false);
        mViewHolder = new ViewHolder(view);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mNotesNameTextView.setText(mNotesArrayList.get(position).noteName);

        holder.mLlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mNotesExhibitor.mShowCheckBox) {
                    mCallback.intentToSavedNote(mNotesArrayList.get(position).id,
                            mNotesArrayList.get(position).noteName, mNotesArrayList.get(position).category);
                }
            }
        });

//        holder.mLlContainer.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
////                Toast.makeText(mContext, "Long Pressed", Toast.LENGTH_LONG).show();
//                mCallback.showCheckBox(mNotesArrayList.get(position).id);
//
//                return false;
//            }
//        });
//
//
//        checkBoxFunc(holder, mNotesArrayList.get(position), position);
    }

    private void checkBoxFunc(ViewHolder holder, Notes item, final int position) {

        holder.mCbNotes.setOnCheckedChangeListener(null);
        if (!item.isShowed) {
            holder.mCbNotes.setVisibility(View.INVISIBLE);
            return;
        }
        holder.mCbNotes.setVisibility(View.VISIBLE);

        holder.mCbNotes.setChecked(item.isChecked);


        holder.mCbNotes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                mNotesArrayList.get(position).isChecked = isChecked;

            }
        });

    }


    public boolean noMarkedData() {
        for (Notes notes : mNotesArrayList) {
            if (notes.isChecked) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Notes> removeMarkData(){
        for (Notes notes : mNotesArrayList) {
            notes.isChecked = false;
        }
        notifyDataSetChanged();
        return mNotesArrayList;
    }

    public ArrayList<Notes> getLatestData(){
        return mNotesArrayList;
    }

//    public void deleteMarked() {
//        for (Notes notes : mNotesArrayList) {
//            if (notes.isChecked) {
//                notes_local_tbl notes_local_tbl = mNotes_local_tblDao.load(mIds.get(mNotesArrayList.indexOf(notes)));
//                mNotes_local_tblDao.delete(notes_local_tbl);
//                mIds.remove(mNotesArrayList.indexOf(notes));
//            }
//        }
////        mNotesArrayList = new ArrayList<>(DataUtil.getNotesLocal(mNotes_local_tblDao));
//        mIds = new ArrayList<>(DataUtil.getNoteIds(mNotes_local_tblDao));
//
//        notifyDataSetChanged();
//    }

    public void updateList(ArrayList<Notes> items) {
        mNotesArrayList = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    public void add(ArrayList<Notes> items) {
//        ArrayList<Notes> noteToAdd = new ArrayList<>();
        mNotesArrayList.addAll(items);
//        for (Notes notes : items) {
//            if (notes.category.equals("Exhibitor")) {
////                noteToAdd.add(notes);
//                mNotesArrayList.add(notes);
//            }
//        }
//        mNotesArrayList = noteToAdd;
        displayNoData(mNotesArrayList);
        notifyDataSetChanged();
    }


    private void displayNoData(ArrayList<Notes> items) {

        if (items.size() > 0 == false) {
            mCallback.displayNoDataText();
            return;
        }
        mCallback.removeNoDataText();
    }

    @Override
    public int getItemCount() {
        return mNotesArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.notesNameTextView)
        TextView mNotesNameTextView;

        @Bind(R.id.cbNotes)
        CheckBox mCbNotes;

        @Bind(R.id.llContainer)
        LinearLayout mLlContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
