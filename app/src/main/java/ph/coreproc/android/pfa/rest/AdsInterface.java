package ph.coreproc.android.pfa.rest;

import java.util.ArrayList;

import ph.coreproc.android.pfa.models.Ads;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by IanBlanco on 5/8/2017.
 */

public interface AdsInterface {

    @GET
    Call<DataWrapper<ArrayList<Ads>>> getAds(@Url String url);
}
