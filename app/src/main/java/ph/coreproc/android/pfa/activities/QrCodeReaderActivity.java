package ph.coreproc.android.pfa.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.coreproc.android.kitchen.preferences.Preferences;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import butterknife.Bind;
import butterknife.ButterKnife;
import database.db.exhibitor_scan_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;

public class QrCodeReaderActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    @Bind(R.id.qrCodeReaderView)
    QRCodeReaderView qrCodeReaderView;


    Toolbar mToolbar;
    Context mContext;
    int mQrResult;

    private User mUser;

    private boolean detectOnce = false;

    private exhibitor_scan_local_tblDao mExhibitor_scan_local_tblDao;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, QrCodeReaderActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code_reader);

        ButterKnife.bind(this);
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mExhibitor_scan_local_tblDao = DataUtil.setUpExhibitorScannedLocal(mContext, Prefs.DB_NAME);
        checkRequiredPermissions();

    }

    private void checkRequiredPermissions() {

//        Log.d("perrr", "asdasd " + (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
//                == PackageManager.PERMISSION_GRANTED));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission_group.CAMERA)
                == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            setUpQrScanner();
        } else {
            requestCameraPermission();
        }

    }

    private void requestCameraPermission() {
        Toast.makeText(mContext, "You have to allow permissions first.", Toast.LENGTH_LONG).show();
        ActivityCompat.requestPermissions(QrCodeReaderActivity.this, new String[]{
                Manifest.permission.CAMERA, Manifest.permission_group.CAMERA
        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        boolean permissionGranted = true;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                permissionGranted = false;
                break;
            }
        }

        if (!permissionGranted) {
            checkRequiredPermissions();
            Toast.makeText(mContext, "You have to allow permissions first.", Toast.LENGTH_LONG).show();
            return;
        }

        setUpQrScanner();

    }


    private void setUpQrScanner() {

        qrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrCodeReaderView);

        qrCodeReaderView.setOnQRCodeReadListener(this);
        qrCodeReaderView.setQRDecodingEnabled(true);
        qrCodeReaderView.setAutofocusInterval(2000L);
        qrCodeReaderView.setBackCamera();


        if (qrCodeReaderView != null) {
            qrCodeReaderView.startCamera();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (qrCodeReaderView != null) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        boolean isParsedSuccess;
        try {
            mQrResult = Integer.parseInt(text);
            isParsedSuccess = true;
        } catch (Exception e) {
            isParsedSuccess = false;
        }
        Log.i("tag", "tag: " + mQrResult);
        if (!detectOnce) {
            Exhibitor exhibitorLocal = PFABase.getExhibitorLocalByScan(mContext, mQrResult);

            String response = Preferences.getString(mContext, Prefs.USER);
            mUser = ModelUtil.fromJson(User.class, response);

            if (mUser.role.equals("visitor") || mUser.role.equals("conference")) {
                Log.i("tag", "tagsss: " + exhibitorLocal);
                detectOnce = true;

                if (!isParsedSuccess) {
                    PFABase.dialogWithOnClick(mContext, "This is not an Exhibitor QR code", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            detectOnce = false;

                        }
                    });
                    return;
                }

                if (exhibitorLocal == null) {
                    PFABase.dialogWithOnClick(mContext, "Exhibitor does not exists", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                           dialog.dismiss();
                            detectOnce = false;
                        }
                    });
                    return;
                }

                startActivity(ExhibitorDetailsActivity.newIntent(mContext, true, true, mQrResult, exhibitorLocal));

                finish();
            }

            if (mUser.role.equals("exhibitor")) {
                Log.i("OkHttp", "textResult: " + exhibitorLocal);
                detectOnce = true;

                if (isParsedSuccess) {
                    PFABase.dialogWithOnClick(mContext, "This is not an Visitor QR code", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            detectOnce = false;
                        }
                    });
                    return;
                }
                startActivity(UserInfoActivity.newIntent(mContext, "" + text, true));

                finish();
            }
        }
    }
}
