package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;
import database.db.seminar_local_tbl;
import database.db.seminar_local_tblDao;
import io.techery.properratingbar.ProperRatingBar;
import io.techery.properratingbar.RatingListener;
import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.models.Seminar;
import ph.coreproc.android.pfa.models.TriggerEventBus;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.offlinemodels.SeminarLocal;
import ph.coreproc.android.pfa.models.requests.CommentBody;
import ph.coreproc.android.pfa.models.requests.RatingBody;
import ph.coreproc.android.pfa.rest.SeminarInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import ph.coreproc.android.pfa.utils.TextViewEx;
import ph.coreproc.android.pfa.utils.UiUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeminarDetailsActivity extends BaseActivity {

    @Bind(R.id.upperRatingBar)
    ProperRatingBar upperRatingBar;

    @Bind(R.id.seminarDescriptionTextView)
    TextView mSeminarDescriptionTextView;

    @Bind(R.id.seminarLocationTextView)
    TextView mSeminarLocationTextView;

    @Bind(R.id.seminarDateTextView)
    TextView mSeminarDateTextView;

    @Bind(R.id.seminarTimeTextView)
    TextView mSeminarTimeTextView;

    @Bind(R.id.favoriteButton)
    Button mFavoriteButton;

    @Bind(R.id.activity_seminar_details)
    LinearLayout mActivitySeminarDetails;

    @Bind(R.id.fabNotes)
    FloatingActionButton mFabNotes;

    @Bind(R.id.llFooter)
    LinearLayout mLlFooter;

    @Bind(R.id.tvRate)
    TextView mTvRate;

    @Bind(R.id.createQuestion)
    Button mCreateQuestion;

    @Bind(R.id.etComment)
    EditText mEtComment;

    @Bind(R.id.submitButton)
    Button mSubmitButton;

    @Bind(R.id.seminarContainer)
    LinearLayout mSeminarContainer;


    private Seminar mSeminar;
    private SeminarLocal mSeminarLocal;

    private ArrayList<Seminar> mSeminarLocalArrayList;

    private seminar_local_tblDao mSeminar_local_tblDao;
    private seminar_local_tbl mSeminar_local_tbl;
    private Call<ResponseBody> mPostRateCall;
    private boolean mHandler;
    public static String ARGS_SEMINAR_ID = "ARGS_SEMINAR_ID";
    private String mSeminarId = "";

    public static String ARGS_IS_LOCAL = "ARGS_IS_LOCAL";
    private boolean mIsLocal;

    private RatingBody mRatingBody;

    private User mUser;

    private TextView mSeminarTitleTextView;
    private TextView mSpeakerOneTextView;
    private TextView mSpeakerTwoTextView;

    public static Intent newIntent(Context context, String id, boolean isLocal) {
        Intent intent = new Intent(context, SeminarDetailsActivity.class);
        intent.putExtra(ARGS_SEMINAR_ID, id);
        intent.putExtra(ARGS_IS_LOCAL, isLocal);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        upperRatingBar.setListener(ratingListener);
        upperRatingBar.toggleClickable();
        mSeminar_local_tblDao = DataUtil.setUpSeminarLocal(mContext,
                Prefs.DB_NAME);
        initialize();
    }


    private void initialize() {
        Bundle extra = getIntent().getExtras();
        mSeminarId = extra.getString(ARGS_SEMINAR_ID);
        mIsLocal = extra.getBoolean(ARGS_IS_LOCAL);
//        if (!mIsLocal) {
//            getData(mSeminarId);
//            return;
//        }
//        setDataFieldsLocal(getDataLocal());

        View view;
        if (mSeminarId.equals("1")) {
            view = LayoutInflater.from(mContext).inflate(R.layout.activity_seminar_details_one, null);
        } else if (mSeminarId.equals("2")) {
            view = LayoutInflater.from(mContext).inflate(R.layout.activity_seminar_details_two, null);

        } else if (mSeminarId.equals("3")) {
            view = LayoutInflater.from(mContext).inflate(R.layout.activity_seminar_details_three, null);
        }else {
            view = LayoutInflater.from(mContext).inflate(R.layout.activity_seminar_details_four, null);
            mSpeakerOneTextView = (TextView) view.findViewById(R.id.tvSpeakerOne);
            mSpeakerTwoTextView = (TextView) view.findViewById(R.id.tvSpeakerTwo);

            mSpeakerOneTextView.setText(Html.fromHtml("<b>Mr. Arief Adnan</b><br>First Secretary <br>Embassy of the Republic of Indonesia, Manila"));
            mSpeakerTwoTextView.setText(Html.fromHtml("<b>Mr. Alexander Sultan</b><br>President<br>Halal International Chamber of Commerce and industries of the Philippines, Inc."));
        }
            TextView tvDetails = (TextView) view.findViewById(R.id.tvDetails);
            ((TextViewEx) tvDetails).setText(tvDetails.getText().toString(), true);
            mSeminarTitleTextView = (TextView) view.findViewById(R.id.seminarTitleTextView);
        mSeminarContainer.addView(view);
        Log.i("text", "this is a test" + mSeminarTitleTextView.getText().toString());
        mSeminar = new Seminar(Integer.parseInt(mSeminarId), mSeminarTitleTextView.getText().toString(), "", "");

        mTvRate.setText("Rate this Seminar");
        if (checkItemToLocal(mSeminar) != null) {
            mFavoriteButton.setText("Unfavorite");
        }

//        setDataFields(mSeminar);
        getData(mSeminarId);

        String response = Preferences.getString(mContext, Prefs.USER);
        User user = ModelUtil.fromJson(User.class, response);

        if (user.role.equals("exhibitor")) {
            mLlFooter.setVisibility(View.GONE);
        }
    }

    private void setDataFieldsLocal(SeminarLocal seminarLocal) {

        mSeminarLocal = seminarLocal;

        if (checkItemToLocal(mSeminarLocal) != null) {
            mFavoriteButton.setText("Unfavorite");
        }

        mSeminarTitleTextView.setText(Html.fromHtml("<b>Name: </b>" + seminarLocal.name));
        mSeminarDateTextView.setText(Html.fromHtml("<b>Date: </b>" + seminarLocal.startsAt));
        mSeminarLocationTextView.setText(Html.fromHtml("<b>Location: </b>" + seminarLocal.location));
//        mLlFooter.setVisibility(View.GONE);
        mFabNotes.setVisibility(View.GONE);
    }


    private Seminar getDataLocal() {

        mSeminarLocalArrayList = new ArrayList<>(DataUtil.getSeminarLocals(mSeminar_local_tblDao, mUser.id, mUser.role));
//        Seminar seminarLocalToReturn = new Seminar();
        for (Seminar seminarLocal : mSeminarLocalArrayList) {
            if (seminarLocal.id == Integer.parseInt(mSeminarId)) {
//                seminarLocalToReturn = seminarLocal;
                mSeminar = seminarLocal;
            }
        }
        return mSeminar;
    }

    private void saveString(String toSaveString) {
        mSeminar_local_tbl = new seminar_local_tbl(toSaveString);
        Log.i("tag", "results :" + toSaveString);
        mSeminar_local_tblDao.insert(mSeminar_local_tbl);
        mFavoriteButton.setText("Unfavorite");
//        Toast.makeText(mContext, "Saved to Favorites", Toast.LENGTH_SHORT).show();
    }

    private void deleteFavorite(int index) {
        ArrayList<Long> ids = DataUtil.getSeminarId(mSeminar_local_tblDao);

        mSeminar_local_tbl = mSeminar_local_tblDao.load(ids.get(index));
        mSeminar_local_tblDao.delete(mSeminar_local_tbl);
        mFavoriteButton.setText("Favorite");
    }

    private void setDataFields(Seminar seminar) {
//        mSeminar = seminar;

//        mSeminarTitleTextView.setText(Html.fromHtml("<b>Name: </b>" + seminar.name));
//        mSeminarDateTextView.setText(Html.fromHtml("<b>Date: </b>" + mSeminar.startsAt));
//        mSeminarLocationTextView.setText(Html.fromHtml("<b>Location: </b>" + mSeminar.location));

        upperRatingBar.setRating(seminar.rating);

    }

    private Integer checkItemToLocal(Seminar seminar) {
        mSeminarLocalArrayList = new ArrayList<>(DataUtil.getSeminarLocals(mSeminar_local_tblDao, mUser.id, mUser.role));
        SeminarLocal seminarLocal = new SeminarLocal(seminar);
        Integer isExistedToLocal = DataUtil.checkSeminarIfExist(mSeminarLocalArrayList, mSeminar);

        return isExistedToLocal;
    }

    private Integer checkItemToLocal(SeminarLocal seminarLocal) {
        mSeminarLocalArrayList = new ArrayList<>(DataUtil.getSeminarLocals(mSeminar_local_tblDao, mUser.id, mUser.role));
        Integer isExistedToLocal = DataUtil.checkSeminarIfExist(mSeminarLocalArrayList, mSeminar);

        return isExistedToLocal;
    }

    private void getData(String seminarId) {
        final Dialog dialog = PFABase.loadDefaultProgressDialogCancellable(mContext, this);
        dialog.show();
        SeminarInterface seminarInterface = KitchenRestClient.create(mContext, SeminarInterface.class, true);
        Call<DataWrapper<Seminar>> seminarResponseCall = seminarInterface.getSeminarDetails(seminarId);
        seminarResponseCall.enqueue(new Callback<DataWrapper<Seminar>>() {
            @Override
            public void onResponse(Call<DataWrapper<Seminar>> call, Response<DataWrapper<Seminar>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {
                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
//                    setDataFields(response.body().getData());
                    return;
                }

                setDataFields(response.body().getData());
            }

            @Override
            public void onFailure(Call<DataWrapper<Seminar>> call, Throwable t) {
                dialog.dismiss();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        PFABase.hideSoftKeyboard(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_seminar_details;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.createQuestion, R.id.fabNotes, R.id.tvCommentSubmit, R.id.tvRateSubmit, R.id.favoriteButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.createQuestion:
                startActivity(QuestionDisplayActivity.newIntent(mContext, Integer.parseInt(mSeminarId), "Seminar"));
                break;
            case R.id.fabNotes:
                startActivity(NotesActivity.newIntent(mContext, Integer.parseInt(mSeminarId), mSeminarTitleTextView.getText().toString(), "Seminar"));
                break;
            case R.id.tvCommentSubmit:
                String comment = mEtComment.getText().toString();
                if (comment.equals("")) {
                    UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up first the comment box.");
                    return;
                }
                sendComment(comment);
                PFABase.hideSoftKeyboard(SeminarDetailsActivity.this);
                break;

            case R.id.tvRateSubmit:
                rateFunction();
                break;

            case R.id.favoriteButton:
                TriggerEventBus triggerEventBus = new TriggerEventBus(2);
                EventBus.getDefault().postSticky(triggerEventBus);

                mSeminarLocalArrayList = new ArrayList<>(DataUtil.getSeminarLocals(mSeminar_local_tblDao, mUser.id, mUser.role));

                SeminarLocal seminarLocalAdd;
//                if (mSeminar != null) {
//                    seminarLocalAdd = new SeminarLocal(mSeminar);
//                } else {
//                    seminarLocalAdd = mSeminarLocal;
//                }

                Integer isExisted = DataUtil.checkSeminarIfExist(mSeminarLocalArrayList, mSeminar);

                if (isExisted == null) {
                    mSeminarLocalArrayList.add(mSeminar);

                    mSeminar.userId = mUser.id;
                    mSeminar.userRole = mUser.role;

                    saveString(ModelUtil.toJsonString(mSeminar));
                    return;
                }

                deleteFavorite(isExisted);
                break;
        }
    }


    private RatingListener ratingListener = new RatingListener() {
        @Override
        public void onRatePicked(final ProperRatingBar ratingBar) {
//            Toast.makeText(mContext, "" + ratingBar.getRating(), Toast.LENGTH_SHORT).show();
            mRatingBody = new RatingBody(ratingBar.getRating());
        }
    };

    private void rateFunction() {

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();


        SeminarInterface seminarInterface = KitchenRestClient.create(mContext, SeminarInterface.class, true);

        mPostRateCall = seminarInterface.postSeminarsRating(mSeminarId + "", mRatingBody);
        mPostRateCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    upperRatingBar.setRating(mSeminar.rating);
                    return;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                try {
                    upperRatingBar.setRating(mSeminar.rating);
                } catch (Exception e) {
                    upperRatingBar.setRating(0);
                }
            }
        });
    }

    private void sendComment(String comment) {

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();

        SeminarInterface seminarInterface = KitchenRestClient.create(mContext, SeminarInterface.class, true);
        CommentBody commentBody = new CommentBody(comment);

        Call<ResponseBody> commentRequest = seminarInterface.postSeminarsComment(mSeminarId, commentBody);
        commentRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }
                UiUtil.showMessageDialog(getSupportFragmentManager(), "Comment Sent");
                mEtComment.setText("");

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
            }
        });

    }
}