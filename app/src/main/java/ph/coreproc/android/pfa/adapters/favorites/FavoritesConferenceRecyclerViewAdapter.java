package ph.coreproc.android.pfa.adapters.favorites;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Topics;

/**
 * Created by IanBlanco on 3/28/2017.
 */

public class FavoritesConferenceRecyclerViewAdapter extends RecyclerView.Adapter<FavoritesConferenceRecyclerViewAdapter.ViewHolder> {

    public interface Callback extends DisplayNoDataInterface{
        void intentToDetails(String id);
    }

    private Callback mCallback;
    private Context mContext;
    private ArrayList<Topics> mFavoritesTopicsList;

    public FavoritesConferenceRecyclerViewAdapter(Context context, ArrayList<Topics> favoritesTopicList, Callback callback) {
        mContext = context;
        mFavoritesTopicsList = favoritesTopicList;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_favorites, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Topics topics = mFavoritesTopicsList.get(position);
        holder.mTvName.setText(topics.name);
        holder.mTvBooth.setText(topics.description);

        holder.mLlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              mCallback.intentToDetails("" + topics.id);
            }
        });
    }

    public void add(ArrayList<Topics> items) {
        this.mFavoritesTopicsList.addAll(items);
        displayNoData(mFavoritesTopicsList);
        notifyDataSetChanged();
    }

    public void update(ArrayList<Topics> items) {
        this.mFavoritesTopicsList = items;
        displayNoData(mFavoritesTopicsList);
        notifyDataSetChanged();
    }

    private void displayNoData(ArrayList<Topics> items) {

        if (items.size() > 0 == false) {
            mCallback.displayNoDataText();
            return;
        }
        mCallback.removeNoDataText();
    }

    @Override
    public int getItemCount() {
        return mFavoritesTopicsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvName)
        TextView mTvName;

        @Bind(R.id.tvBooth)
        TextView mTvBooth;

        @Bind(R.id.llContainer)
        LinearLayout mLlContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
