package ph.coreproc.android.pfa.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * Created by IanBlanco on 4/17/2017.
 */

public class Countries {

    public ArrayList<Country> mCountryArrayList;

    public Countries(JsonObject jsonObject){
        JsonArray json = jsonObject.get("countries").getAsJsonArray();
        mCountryArrayList = new ArrayList<>();
        for (int i = 0; i < json.size(); i ++){

            mCountryArrayList.add(new Country(json.get(i).getAsJsonObject()));

        }
    }

}
