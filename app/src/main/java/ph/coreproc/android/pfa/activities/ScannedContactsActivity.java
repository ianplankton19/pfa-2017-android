package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import butterknife.Bind;
import ph.coreproc.android.pfa.R;

public class ScannedContactsActivity extends BaseActivity {

    @Bind(R.id.scannedRecyclerView)
    RecyclerView scannedRecyclerView;

    @Bind(R.id.scannedTabLayout)
    TabLayout scannedTabLayout;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ScannedContactsActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupTabLayout();

    }

    private void setupTabLayout() {
        if (scannedTabLayout != null) {
            TabLayout.Tab tab = scannedTabLayout.newTab();
            tab.setText("Exhibitors");
            scannedTabLayout.addTab(tab);
            tab = scannedTabLayout.newTab();
            tab.setText("Visitors");
            scannedTabLayout.addTab(tab);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_scanned_contacts;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
