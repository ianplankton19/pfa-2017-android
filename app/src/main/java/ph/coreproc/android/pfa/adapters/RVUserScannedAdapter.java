package ph.coreproc.android.pfa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.User;

/**
 * Created by IanBlanco on 6/1/2017.
 */

public class RVUserScannedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface Callback extends DisplayNoDataInterface {
        void intentToDetails(int id);
    }

    private Callback mCallback;

    private Context mContext;
    private ArrayList<User> mUserArrayList;

    public RVUserScannedAdapter(Context context, ArrayList<User> userArrayList, Callback callback) {
        mContext = context;
        mUserArrayList = userArrayList;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_exhibitor_item, parent, false);
        ViewHolder userViewHolder = new ViewHolder(view);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder userViewHolder = (ViewHolder) holder;
        final User user = mUserArrayList.get(position);
        if (user.logo != null) {
            Glide.with(mContext)
                    .load(user.logo)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_delete_white)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            Log.d("error loading image", "erro " + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            userViewHolder.mExhibitorImageView.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(userViewHolder.mExhibitorImageView);
        }
        try {
            userViewHolder.mExhibitorNameTextView.setText(user.expoVisitor.mExpoVisitorContent.fullName);
        }catch (Exception e) {
//            To avoid null
        }
        userViewHolder.mBoothNumberTextView.setText(user.email);
        userViewHolder.mRlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.intentToDetails(user.id);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mUserArrayList.size();
    }


    public void add(ArrayList<User> items) {
        int previousDataSize = this.mUserArrayList.size();
        this.mUserArrayList.addAll(items);
//        displayNoData(this.items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.exhibitorImageView)
        ImageView mExhibitorImageView;

        @Bind(R.id.exhibitorNameTextView)
        TextView mExhibitorNameTextView;

        @Bind(R.id.boothNumberTextView)
        TextView mBoothNumberTextView;

        @Bind(R.id.rlContainer)
        RelativeLayout mRlContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
