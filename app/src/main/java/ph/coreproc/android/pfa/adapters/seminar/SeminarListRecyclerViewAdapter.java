package ph.coreproc.android.pfa.adapters.seminar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Seminar;

/**
 * Created by willm on 3/27/2017.
 */

public class SeminarListRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Seminar> items;
    Context mContext;


    public SeminarListRecyclerViewAdapter(Context context, ArrayList<Seminar> items) {
        mContext = context;
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_seminar_item, parent, false);
        SeminarViewHolder seminarViewHolder = new SeminarViewHolder(view);
        return seminarViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    public void add(ArrayList<Seminar> items) {
        int previousDataSize = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class SeminarViewHolder extends RecyclerView.ViewHolder {

        public SeminarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

