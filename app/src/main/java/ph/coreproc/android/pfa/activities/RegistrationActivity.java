package ph.coreproc.android.pfa.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.RegistrationViewPagerAdapter;
import ph.coreproc.android.pfa.fragments.registrationfragment.RegistrationFifthFragment;
import ph.coreproc.android.pfa.fragments.registrationfragment.RegistrationFirstFragment;
import ph.coreproc.android.pfa.fragments.registrationfragment.RegistrationFourthFragment;
import ph.coreproc.android.pfa.fragments.registrationfragment.RegistrationSecondFragment;
import ph.coreproc.android.pfa.fragments.registrationfragment.RegistrationThirdFragment;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.Registration;
import ph.coreproc.android.pfa.rest.UserInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import ph.coreproc.android.pfa.utils.UiUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v4.view.ViewPager.OnPageChangeListener;

/**
 * Created by IanBlanco on 3/29/2017.
 */

public class RegistrationActivity extends BaseActivity {

    //    For first Fragment Strings
    public String mPasswordStr = "", mEmailStr = "";

    //    For Second Fragment Strings
    public String mFullnameStr = "", mAgeRangeStr = "", mGenderStr = "";


    //    For Third Fragment Strings
    public String mCountryStr = "", mRegionStr = "", mMobileNoStr = "";

    // For Fourth Fragment Strings
    public String mCompanyStr = "", mPhoneStr = "", mPositionTitleStr = "", mOccupationStr = "";

    // For Fifth Fragment Strings
    public String mHowDidYouLearnStr = "", mHowDidYouLearnOthersStr = "", mLookingForFranchiseStr = "",
            mAreaOfInterestStr = "", mPurposeOfVisitStr = "", mPurposeVisitOthersStr = "", mAreYouAnExistingStr = "";
    public Boolean mRepeatVisitBool, mAreYouInterestedBool;


    private boolean mCountryCheck;
    private int mPagePosition;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, RegistrationActivity.class);
        return intent;
    }

    @Bind(R.id.btnSignUp)
    Button mBtnSignUp;

    @Bind(R.id.vpRegistration)
    ViewPager mVpRegistration;

    @Bind(R.id.circlePageIndicator)
    CirclePageIndicator mCirclePageIndicator;

    @Bind(R.id.btnCancel)
    Button mBtnCancel;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_registration;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_dark);
        initialize();
        ButterKnife.bind(this);
    }

    private void initialize() {
        setUpViewPager();

        mVpRegistration.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mPagePosition = position;
            }

            @Override
            public void onPageSelected(int position) {
                if (position < 4) {
                    mBtnSignUp.setText("NEXT");
                } else {
                    mBtnSignUp.setText("FINISH");
                }

                if (position == 0) {
                    mBtnCancel.setText("CANCEL");
                } else {
                    mBtnCancel.setText("BACK");
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean pageChecker = mVpRegistration.getCurrentItem() == 0;
                if (!pageChecker) {
                    mVpRegistration.setCurrentItem(mVpRegistration.getCurrentItem() - 1);
                    return;
                }
                finish();
            }
        });

        mBtnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (mPagePosition) {
                    case 0:

                        if (mEmailStr.trim().equals("")) {
                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your email.");
                            return;
                        }

                        if (!PFABase.isEmailValid(mEmailStr.trim())) {
                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Wrong format of email.");
                            return;
                        }

                        if (mPasswordStr.length() < 8) {
                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Password length is minimum of 8.");
                            return;
                        }

//                        mVpRegistration.setCurrentItem(position);
                        mVpRegistration.setCurrentItem(mPagePosition + 1);
                        break;

                    case 1:
                        if (mFullnameStr.equals("")) {
                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your name.");
                            return;
                        }

//                        if (mAgeRangeStr.equals("")) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your age range.");
//                            return;
//                        }
//
//                        if (mGenderStr.equals("")) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your gender.");
//                            return;
//                        }
                        mVpRegistration.setCurrentItem(mPagePosition + 1);
                        break;

                    case 2:
                        if (mCountryStr.equals("")) {
                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your Country.");
                            return;
                        }

//                        Check if country is Philippines
                        mCountryCheck = mCountryStr.equals("174");

                        if (mCountryCheck) {
                            if (mRegionStr.equals("")) {
                                UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your region.");
                                return;
                            }
                        }

//                        if (mMobileNoStr.length() < 10) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Mobile Number length is more than 10 digits.");
//                            return;
//                        }
                        mVpRegistration.setCurrentItem(mPagePosition + 1);
                        break;

                    case 3:
//                        if (mCompanyStr.equals("")) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your company.");
//                            return;
//                        }

                        if (mPhoneStr.length() > 0) {
                            if (mPhoneStr.length() < 7) {
                                UiUtil.showMessageDialog(getSupportFragmentManager(), "Phone Number length is minimum of 7 digits.");
                                return;
                            }
                        }

//                        if (mPositionTitleStr.equals("")) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your position.");
//                            return;
//                        }
//
//                        if (mOccupationStr.equals("")) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your occupation.");
//                            return;
//                        }
                        mVpRegistration.setCurrentItem(mPagePosition + 1);
                        break;

                    case 4:
                        if (mVpRegistration.getCurrentItem() < 4) {
                            mVpRegistration.setCurrentItem(mVpRegistration.getCurrentItem() + 1, true);
                            return;
                        }


//                        if (mPurposeOfVisitStr.equals("") && mPurposeVisitOthersStr.equals("")) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your purpose of visit.");
//                            return;
//                        }

                        if (mAreaOfInterestStr.equals("")) {
                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your area of interest.");
                            return;
                        }

//                        if (mAreYouAnExistingStr == null) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up the missing fields.");
//                            return;
//                        }

                        if (mLookingForFranchiseStr.equals("")) {
                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up your desired franchise package.");
                            return;
                        }

//                        if (mAreYouInterestedBool == null) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up if you are interested in aquiring a master franchise.");
//                            return;
//                        }

//                        if (mRepeatVisitBool == null) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up if if you repeat visit.");
//                            return;
//                        }

//                        if (mHowDidYouLearnOthersStr.equals("") && mHowDidYouLearnStr.equals("")) {
//                            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up if how did you learn this expo.");
//                            return;
//                        }
//                        mVpRegistration.setCurrentItem(mPagePosition + 1);
                        mFullnameStr = PFABase.removeEndSpacing(mFullnameStr);
                        mEmailStr = PFABase.removeEndSpacing(mEmailStr.trim());
                        mCompanyStr = PFABase.removeEndSpacing(mCompanyStr);


                        Registration registration = new Registration(mFullnameStr, mEmailStr, mPasswordStr, mRegionStr.equals("") ? "     " : mRegionStr,
                                mGenderStr.toLowerCase(), mCompanyStr, mMobileNoStr, mPhoneStr,
                                mAgeRangeStr, mOccupationStr, mPositionTitleStr, mPurposeOfVisitStr, mPurposeVisitOthersStr,
                                mAreaOfInterestStr, mLookingForFranchiseStr, mAreYouAnExistingStr, mRepeatVisitBool, mAreYouInterestedBool,
                                mHowDidYouLearnStr, mHowDidYouLearnOthersStr);


                        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
                        dialog.show();

                        UserInterface userInterface = KitchenRestClient.create(mContext, UserInterface.class, false);
                        Call<DataWrapper<User>> userCall = userInterface.registerUser(getString(R.string.register_url), registration);

                        userCall.enqueue(new Callback<DataWrapper<User>>() {
                            @Override
                            public void onResponse(Call<DataWrapper<User>> call, Response<DataWrapper<User>> response) {
                                dialog.dismiss();
                                if (!response.isSuccessful()) {
                                    APIError error = ErrorUtil.parsingError(response);
                                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                                    return;
                                }
                                Preferences.setString(mContext, Preferences.API_KEY, "" + response.headers().get("X-Authorization"));
                                Preferences.setString(mContext, Prefs.USER_ID, "" + response.body().getData().id);

                                //                Json Users
                                User user = response.body().getData();
//                        JsonObject jsonObject1 = new JsonObject();
//                        jsonObject1.addProperty("id", user.id);
//                        jsonObject1.addProperty("full_name", user.fullName);
//                        jsonObject1.addProperty("email", user.email);
//                        jsonObject1.addProperty("gender", user.gender);
//                        jsonObject1.addProperty("company", user.company);
//                        jsonObject1.addProperty("position", user.position);
//                        jsonObject1.addProperty("address", user.address);
//                        jsonObject1.addProperty("region_id", user.regionId);
//                        jsonObject1.addProperty("mobile", user.mobile);
//                        jsonObject1.addProperty("phone", user.phone);
//                        jsonObject1.addProperty("qr_code", user.qrCode);

                                Log.i("tag", "userJson: " + ModelUtil.toJsonString(user));

                                Preferences.setString(mContext, Prefs.USER, ModelUtil.toJsonString(user));
//               -------------------------------------------------------- //

                                ActivityCompat.finishAffinity((Activity) mContext);
                                startActivity(DashBoardActivity.newIntent(mContext).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                finish();

                            }

                            @Override
                            public void onFailure(Call<DataWrapper<User>> call, Throwable t) {
                                dialog.dismiss();
                                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
                            }
                        });

                        break;

                }


            }
        });
    }

    private void setUpViewPager() {

        RegistrationViewPagerAdapter adapter = new RegistrationViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(RegistrationFirstFragment.newInstance());
        adapter.addFragment(RegistrationSecondFragment.newInstance());
        adapter.addFragment(RegistrationThirdFragment.newInstance());
        adapter.addFragment(RegistrationFourthFragment.newInstance());
        adapter.addFragment(RegistrationFifthFragment.newInstance());

        mVpRegistration.setAdapter(adapter);
        mCirclePageIndicator.setViewPager(mVpRegistration);
        mCirclePageIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//            do nothing
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
