package ph.coreproc.android.pfa;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.coreproc.android.kitchen.preferences.Preferences;
import com.getsentry.raven.android.Raven;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ivankocijan.magicviews.MagicViews;

import java.io.IOException;
//import com.joshdholtz.sentry.Sentry;

/**
 * Created by johneris on 6/5/2015.
 */
public class App extends com.activeandroid.app.Application {

    // Save DNS strings here
    public static String SENTRY_DNS = "https://6c0cf22e1f1149fe915c4a72504b5967:55cb77fb0d6c4727add005f68b180648@sentry.coreproc.ph/40";
    public static String SENTRY_DNS_PROD = "https://164fea15cf494e06bce26f73d55bb11f:a2ae7ce10b5e4f9faf7fd63219e8d838@sentry.coreproc.ph/46";

    @Override
    public void onCreate() {
        super.onCreate();
        MagicViews.setFontFolderPath(this, "fonts");

        // Turn this on if needed

            Raven.init(this, BuildConfig.DEBUG ? SENTRY_DNS : SENTRY_DNS_PROD);
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);
//        // Normal app init code...

        if (Preferences.getString(this, Preferences.FCM_TOKEN).length() == 0) {
            Preferences.setString(this, Preferences.FCM_TOKEN, "");
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
                Preferences.setString(this, Preferences.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        throw new RuntimeException();
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
