package ph.coreproc.android.pfa.models.offlinemodels;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by IanBlanco on 4/13/2017.
 */

public class SpeakerTopicLocal implements Serializable {

    @SerializedName("data")
    @Expose
    public ArrayList<TopicLocal> speakerTopics;



    private JsonObject data;

    public void setData(JsonObject data) {
        this.data = data;
    }

    public JsonObject getData() {
        return data;
    }


    public SpeakerTopicLocal(JsonArray jsonArray) {
        data = new JsonObject();
        data.add("data", jsonArray);
    }

}
