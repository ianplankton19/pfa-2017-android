package ph.coreproc.android.pfa.models.offlinemodels;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.SubTopic;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;

public class SubTopicFieldLocal implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("day")
    @Expose
    public int day;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("location")
    @Expose
    public String location;

    @SerializedName("starts_at")
    @Expose
    public String startsAt;

    @SerializedName("ends_at")
    @Expose
    public String endsAt;

    @SerializedName("duration")
    @Expose
    public String duration;

    @SerializedName("speakers")
    @Expose
    DataWrapper<SpeakerLocal> speakersData;

    public JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", this.id);
        jsonObject.addProperty("name", this.name);
        jsonObject.addProperty("day", this.day);
        jsonObject.addProperty("description", this.description);
        jsonObject.addProperty("location", this.location);
        jsonObject.addProperty("starts_at", this.startsAt);
        jsonObject.addProperty("ends_at", this.endsAt);
        jsonObject.addProperty("duration", this.duration);

        return jsonObject;
    }

    public SubTopicFieldLocal(SubTopic.SubTopicField subTopicField){
        this.id = subTopicField.id;
        this.name = subTopicField.name;
        this.day = subTopicField.day;
        this.description = subTopicField.description;
        this.location = subTopicField.location;
        this.startsAt = subTopicField.startsAt;
        this.endsAt = subTopicField.endsAt;
        this.duration = subTopicField.duration;

    }


    public View getView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_track_item, null);
        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);

        tvName.setText(name);
        tvDescription.setText(description);

        return view;

    }

}
