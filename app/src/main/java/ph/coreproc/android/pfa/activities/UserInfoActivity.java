package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;
import com.ivankocijan.magicviews.views.MagicTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import database.db.user_scan_local_tbl;
import database.db.user_scan_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.rest.UserInterface;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 5/24/2017.
 */

public class UserInfoActivity extends BaseActivity {

    @Bind(R.id.tvSourceOfInfo)
    TextView mTvSourceOfInfo;

    @Bind(R.id.tvRole)
    TextView mTvRole;

    @Bind(R.id.tvCompany)
    TextView mTvCompany;

    @Bind(R.id.tvMobile)
    TextView mTvMobile;

    @Bind(R.id.tvAgeGroup)
    TextView mTvAgeGroup;

    @Bind(R.id.tvPosition)
    TextView mTvPosition;

    @Bind(R.id.tvAreaOfInterest)
    TextView mTvAreaOfInterest;

    @Bind(R.id.tvFranchisePackage)
    TextView mTvFranchisePackage;

    @Bind(R.id.tvFrequencyVisit)
    TextView mTvFrequencyVisit;

//    @Bind(R.id.tvMasterFranchise)
//    TextView mTvMasterFranchise;

    @Bind(R.id.rlExhibitorDetailsContent)
    RelativeLayout mRlExhibitorDetailsContent;

    @Bind(R.id.ivFooter)
    ImageView mIvFooter;

    @Bind(R.id.ivUSer)
    ImageView mIvUSer;

    @Bind(R.id.tvUSerFullname)
    MagicTextView mTvUSerFullname;

    @Bind(R.id.tvEmail)
    MagicTextView mTvEmail;

    @Bind(R.id.llCompany)
    LinearLayout mLlCompany;

    @Bind(R.id.llMobileNumber)
    LinearLayout mLlMobileNumber;

    @Bind(R.id.llAgeGroup)
    LinearLayout mLlAgeGroup;

    @Bind(R.id.llAreaOfInterest)
    LinearLayout mLlAreaOfInterest;

    @Bind(R.id.llFranchisePackage)
    LinearLayout mLlFranchisePackage;

    @Bind(R.id.llFrequecyVisit)
    LinearLayout mLlFrequecyVisit;

    @Bind(R.id.llSourceOfInfo)
    LinearLayout mLlSourceOfInfo;

    private User mUser;
    private User mUser1;
    public static String ARGS_ID = "ARGS_ID";
    private String mUserId;

    private boolean mIsScanned;
    public static String ARGS_ISSCANNED = "ARGS_ISSCANNED";


    private user_scan_local_tblDao mUser_scan_local_tblDao;

    public static Intent newIntent(Context context, String userId, boolean isScanned) {

        Intent intent = new Intent(context, UserInfoActivity.class);
        intent.putExtra(ARGS_ID, userId);
        intent.putExtra(ARGS_ISSCANNED, isScanned);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_user_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation

        mUser_scan_local_tblDao = DataUtil.setUpUserScannedLocal(mContext, Prefs.DB_NAME);

        ButterKnife.bind(this);
        String response = Preferences.getString(mContext, Prefs.USER);
        mUser1 = ModelUtil.fromJson(User.class, response);

        initialize();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void initialize() {

        Bundle extras = getIntent().getExtras();
        mUserId = extras.getString(ARGS_ID);
        mIsScanned = extras.getBoolean(ARGS_ISSCANNED);

        if (mUserId == null) {
            mUser = ModelUtil.fromJson(User.class, Preferences.getString(mContext, Prefs.USER));
            return;
        }
        getData();

    }


    private void setFields(User user) {
        mUser = user;
        if (mIsScanned) {
            if (!user.role.equals("visitor")) {
//                UiUtil.showMessageDialog(getSupportFragmentManager(), "visitor does not exist.");
                PFABase.dialogWithOnClick(mContext, "visitor does not exist.", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                return;
            }


            user.userId = mUser1.id;
            user.userRole = mUser1.role;
            saveScannedUser(user);
        }

        hideTextViews();
        mTvEmail.setText(user.email == null ? "" : user.email);
        if (user.expoVisitor != null) {
            mTvUSerFullname.setText(user.expoVisitor.mExpoVisitorContent.fullName == null ? "" : user.expoVisitor.mExpoVisitorContent.fullName);
            mTvPosition.setText(user.expoVisitor.mExpoVisitorContent.positionTitle);
            mTvAgeGroup.setText(user.expoVisitor.mExpoVisitorContent.ageGroup);
//            mTvRole.setText(Html.fromHtml("<b>Role: </b>" + user.role));
            mTvCompany.setText(user.expoVisitor.mExpoVisitorContent.company);
            mTvMobile.setText(user.expoVisitor.mExpoVisitorContent.mobile);
            mTvAreaOfInterest.setText(user.expoVisitor.mExpoVisitorContent.areaOfInterest);
            mTvFranchisePackage.setText(user.expoVisitor.mExpoVisitorContent.franchisePackage);
            mTvFrequencyVisit.setText(user.expoVisitor.mExpoVisitorContent.frequencyVisit);
            mTvSourceOfInfo.setText(user.expoVisitor.mExpoVisitorContent.sourceOfInfo);

            mLlFrequecyVisit.setVisibility((user.expoVisitor.mExpoVisitorContent.frequencyVisit == null) ? View.GONE : View.VISIBLE);
            mLlSourceOfInfo.setVisibility((user.expoVisitor.mExpoVisitorContent.sourceOfInfo == null) ? View.GONE : View.VISIBLE);

//        mTvMasterFranchise.setText(Html.fromHtml("<b>Master Franchise: </b>" + mUser.expoVisitor.mExpoVisitorContent.masterFranchise));
        }


        if (user.logo != null) {
            Glide.with(mContext)
                    .load(user.logo)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            Log.d("error loading image", "error " + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            mIvUSer.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(mIvUSer);
        }

//        String response = ModelUtil.toJsonString(mUser);

    }


    private void saveScannedUser(User user) {
        List<user_scan_local_tbl> user_scan_local_tbls = mUser_scan_local_tblDao.queryBuilder().
                orderDesc(user_scan_local_tblDao.Properties.Id).build().list();
        ArrayList<User> userLocals = new ArrayList<>();
        ArrayList<Long> ids = new ArrayList<>();
        if (user_scan_local_tbls.size() > 0) {

            for (user_scan_local_tbl user_scan_local_tbl : user_scan_local_tbls) {

                String dataResponse = user_scan_local_tbl.getSave_string();
                long id = user_scan_local_tbl.getId();
                User user1 = ModelUtil.fromJson(User.class, dataResponse);
                userLocals.add(user1);

            }


            if (DataUtil.checkScanUserIfExist(userLocals, user) != null) {
                //        Update Function
                String sameResponse = ModelUtil.toJsonString(userLocals.get(DataUtil.checkScanUserIfExist(userLocals, user)));

                if (ModelUtil.toJsonString(user).length() > sameResponse.length()) {
                    /**
                     * Comment update for a while
                     */
//                    long updateId = ids.get(DataUtil.checkScanIfExist(exhibitorLocals, exhibitorLocal));
//                    exhibitor_scan_local_tbl exhibitorScanLocalTbl = mExhibitor_scan_local_tblDao.load(updateId);
//                    mExhibitor_scan_local_tblDao.update(exhibitorScanLocalTbl);
//                    exhibitorLocal = ModelUtil.fromJson(ExhibitorLocal.class, response);
                }
//        Nothing to do

            } else {

                user_scan_local_tbl userScanLocalTbl = new user_scan_local_tbl(ModelUtil.toJsonString(user));
                mUser_scan_local_tblDao.insert(userScanLocalTbl);
//                exhibitorLocal = ModelUtil.fromJson(ExhibitorLocal.class, response);
            }

            return;
        }

        user_scan_local_tbl userScanLocalTbl = new user_scan_local_tbl(ModelUtil.toJsonString(user));
        mUser_scan_local_tblDao.insert(userScanLocalTbl);
//        exhibitorLocal = ModelUtil.fromJson(ExhibitorLocal.class, response);
    }

    private void getData() {
        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();
        UserInterface userInterface = KitchenRestClient.create(mContext, UserInterface.class, true);
        Call<DataWrapper<User>> userResponseCall;
        if (mUserId.contains("M")) {
            userResponseCall = userInterface.getUser("", mUserId);
        } else {
            userResponseCall = userInterface.getUser(mUserId, "");
        }


        userResponseCall.enqueue(new Callback<DataWrapper<User>>() {
            @Override
            public void onResponse(Call<DataWrapper<User>> call, Response<DataWrapper<User>> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);

                    if (error.getError().getMessage().toLowerCase().equals("this user does not exist.")) {
                        PFABase.dialogWithOnClick(mContext, error.getError().getMessage(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                        return;
                    }
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                setFields(response.body().getData());
            }

            @Override
            public void onFailure(Call<DataWrapper<User>> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
            }
        });
    }

    private void hideTextViews() {

        if (mUser != null) {
//            setIfVisible(mTvUSerFullname, mUser.expoVisitor.mExpoVisitorContent.fullName);
//            setIfVisible(mTvEmail, mUser.email);
//            setIfVisible(mTvRole, mUser.role);

//            setIfVisible(mTvPhone, mUser.expoVisitor.mExpoVisitorContent.phone);
//            setIfVisible(mTvPosition, mUser.expoVisitor.mExpoVisitorContent.positionTitle);
            setIfVisible(mLlAreaOfInterest, mUser.expoVisitor.mExpoVisitorContent.areaOfInterest);
            setIfVisible(mLlFranchisePackage, mUser.expoVisitor.mExpoVisitorContent.franchisePackage);
            setIfVisible(mLlFrequecyVisit, mUser.expoVisitor.mExpoVisitorContent.frequencyVisit);
            setIfVisible(mLlAgeGroup, mUser.expoVisitor.mExpoVisitorContent.ageGroup);
//            setIfVisible(mTvRole, mUser.role);
            setIfVisible(mLlCompany, mUser.expoVisitor.mExpoVisitorContent.company);
            setIfVisible(mLlMobileNumber, mUser.expoVisitor.mExpoVisitorContent.mobile);
//            setIfVisible(mTvMasterFranchise, mUser.expoVisitor.mExpoVisitorContent.masterFranchise);
            setIfVisible(mLlSourceOfInfo, mUser.expoVisitor.mExpoVisitorContent.sourceOfInfo);
        }
    }


    private void setIfVisible(LinearLayout linearLayout, String text) {

        Log.i("text","text" + text);

        try {
            linearLayout.setVisibility(text.trim().equals("") || text.equals("null") ? View.GONE : View.VISIBLE);
        } catch (Exception e) {
            linearLayout.setVisibility(View.GONE);
        }
    }

}
