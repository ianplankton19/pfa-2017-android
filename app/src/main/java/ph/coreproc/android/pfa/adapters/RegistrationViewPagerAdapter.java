package ph.coreproc.android.pfa.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IanBlanco on 3/29/2017.
 */

public class RegistrationViewPagerAdapter extends FragmentPagerAdapter {

    public List<Fragment> mFragmentList = new ArrayList<>();

    public RegistrationViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    public void addFragment(Fragment fragment) {
    mFragmentList.add(fragment);

    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
