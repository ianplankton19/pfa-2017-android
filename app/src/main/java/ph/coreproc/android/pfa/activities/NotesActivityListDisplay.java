package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.notes.NotesViewPagerAdapter;
import ph.coreproc.android.pfa.fragments.notes.NotesConference;
import ph.coreproc.android.pfa.fragments.notes.NotesExhibitor;
import ph.coreproc.android.pfa.fragments.notes.NotesSeminar;

/**
 * Created by IanBlanco on 4/19/2017.
 */

public class NotesActivityListDisplay extends BaseActivity {


    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.noteTabLayout)
    TabLayout mNoteTabLayout;

    @Bind(R.id.noteListViewPager)
    ViewPager mNoteListViewPager;

    public static int mPagePosition = 0;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, NotesActivityListDisplay.class);

        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_note;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialize();
    }



    private void initialize() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setViewPager();
        mNoteTabLayout.setupWithViewPager(mNoteListViewPager);
    }

    private void setViewPager() {
        NotesViewPagerAdapter adapter = new NotesViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(NotesConference.newInstance(), "Conference");
        adapter.addFragment(NotesSeminar.newInstance(), "Seminar");
        adapter.addFragment(NotesExhibitor.newInstance(), "Exhibitor");
//        adapter.addFragment(ConferenceFragment.newInstance(), "CONFERENCE");
//        adapter.addFragment(ExpoFragment.newInstance(), "EXPO");
//        adapter.addFragment(SeminarFragment.newInstance(), "SEMINARS");
        mNoteListViewPager.setAdapter(adapter);
        mNoteListViewPager.setOffscreenPageLimit(3);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
//                if (mShowCheckBox) {
//                    mShowCheckBox = false;
//                    mIvDelete.setVisibility(View.GONE);
//                    mNotesArrayList = mNotesListAdapter.getLatestData();
//                    mNotesListAdapter.updateList(arrayToPass(mShowCheckBox));
//                    break;
//                }
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        if (mShowCheckBox) {
//            mShowCheckBox = false;
//            mIvDelete.setVisibility(View.GONE);
//            mNotesArrayList = mNotesListAdapter.getLatestData();
//            mNotesListAdapter.updateList(arrayToPass(mShowCheckBox));
//            return;
//        }
        finish();
    }

}
