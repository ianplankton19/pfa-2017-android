package ph.coreproc.android.pfa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 3/30/2017.
 */

public class Registration implements Serializable{

    public Registration(String fullName, String email, String password, String regionId,
                        String gender, String company, String mobile, String phone, String ageGroup,
                        String occupation, String positionTitle, String purposeOfVisit,
                        String purposeOfVisitOthers, String areaOfInterest, String franchisePackage,
                        String existing, Boolean repeatVisit, Boolean masterFranchise, String sourceOfInfo,
                        String sourceOfInfoOthers) {

        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.regionId = regionId;
        this.gender = gender;
        this.company = company;
        this.mobile = mobile;
        this.phone = phone;
        this.fax = fax;
        this.ageGroup = ageGroup;
        this.occupation = occupation;
        this.positionTitle = positionTitle;
        this.purposeOfVisit = purposeOfVisit;
        this.purposeOfVisitOthers = purposeOfVisitOthers;
        this.areaOfInterest = areaOfInterest;
        this.franchisePackage = franchisePackage;
        this.existing = existing;
        this.repeatVisit = repeatVisit;
        this.masterFranchise = masterFranchise;
        this.sourceOfInfo = sourceOfInfo;
        this.sourceOfInfoOthers = sourceOfInfoOthers;
    }

    @SerializedName("full_name")
    @Expose
    private String fullName;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("region_id")
    @Expose
    private String regionId;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("company")
    @Expose
    private String company;

    @SerializedName("position")
    @Expose
    private String position;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("fax")
    @Expose
    private String fax;

    @SerializedName("age_group")
    @Expose
    private String ageGroup;

    @SerializedName("occupation")
    @Expose
    private String occupation;

    @SerializedName("position_title")
    @Expose
    private String positionTitle;

    @SerializedName("purpose_of_visit")
    @Expose
    private String purposeOfVisit;

    @SerializedName("purpose_of_visit_others")
    @Expose
    private String purposeOfVisitOthers;

    @SerializedName("area_of_interest")
    @Expose
    private String areaOfInterest;

    @SerializedName("franchise_package")
    @Expose
    private String franchisePackage;

    @SerializedName("existing")
    @Expose
    private String existing;

    @SerializedName("repeat_visit")
    @Expose
    private Boolean repeatVisit;

    @SerializedName("master_franchise")
    @Expose
    private Boolean masterFranchise;

    @SerializedName("source_of_info")
    @Expose
    private String sourceOfInfo;

    @SerializedName("source_of_info_others")
    @Expose
    private String sourceOfInfoOthers;


    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getRegionId() {
        return regionId;
    }

    public String getGender() {
        return gender;
    }

    public String getCompany() {
        return company;
    }

    public String getPosition() {
        return position;
    }

    public String getAddress() {
        return address;
    }

    public String getMobile() {
        return mobile;
    }

    public String getPhone() {
        return phone;
    }

    public String getFax() {
        return fax;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getPositionTitle() {
        return positionTitle;
    }

    public String getPurposeOfVisit() {
        return purposeOfVisit;
    }

    public String getPurposeOfVisitOthers() {
        return purposeOfVisitOthers;
    }

    public String getAreaOfInterest() {
        return areaOfInterest;
    }

    public String getFranchisePackage() {
        return franchisePackage;
    }

    public String getExisting() {
        return existing;
    }

    public Boolean getRepeatVisit() {
        return repeatVisit;
    }

    public Boolean getMasterFranchise() {
        return masterFranchise;
    }

    public String getSourceOfInfo() {
        return sourceOfInfo;
    }

    public String getSourceOfInfoOthers() {
        return sourceOfInfoOthers;
    }
}
