package ph.coreproc.android.pfa.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.coreproc.android.kitchen.preferences.Preferences;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.Bind;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.utils.Prefs;

public class SplashScreenActivity extends BaseActivity {

//    @Bind(R.id.tvWebsite)
//    MagicTextView mTvWebsite;

    @Bind(R.id.loginButton)
    Button mBtnLogin;

    @Bind(R.id.signupButton)
    Button mBtnSignUp;

    @Bind(R.id.background)
    ImageView mIvBackground;

    private View mContentView;
    private Context mContext;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, SplashScreenActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

//        mContentView = findViewById(R.id.fullscreen_content);
//        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

//        mTvWebsite.setText(Html.fromHtml("www.<b>franchiseasiaph</b>.com"));

        if (Preferences.getString(mContext, Preferences.FCM_TOKEN).isEmpty()) {
            Preferences.setString(mContext, Preferences.FCM_TOKEN, "");
            Preferences.setString(this, Prefs.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
        }

        if (PFABase.isUserLoggedIn(mContext)) {
            startActivity(DashBoardActivity.newIntent(mContext));
            finish();
        }

        initialize();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_splash_screen;
    }

    private void initialize() {
        checkRequiredPermissions();

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(LoginActivity.newIntent(mContext));
            }
        });

        mBtnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(RegistrationActivityNew.newIntent(mContext));
            }
        });

        Glide.with(this)
                .load(R.drawable.splash_background_two)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .thumbnail(0.7f)
                .into(mIvBackground);

    }


    private void checkRequiredPermissions() {

//        Log.d("perrr", "asdasd " + (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
//                == PackageManager.PERMISSION_GRANTED));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission_group.CAMERA)
                == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
        } else {
           startActivity(new Intent(this, PermissionActivity.class));
        }

    }


}
