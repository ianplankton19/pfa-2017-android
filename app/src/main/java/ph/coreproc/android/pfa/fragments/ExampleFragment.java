package ph.coreproc.android.pfa.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;

/**
 * Created by johneris on 6/15/2015.
 */
public class ExampleFragment extends Fragment {

    private Context mContext;


    public static ExampleFragment newInstance() {
        ExampleFragment exampleFragment = new ExampleFragment();

//        Bundle args = new Bundle();
//        args.putString("key", "value");
//        fragment.setArguments(args);

        return exampleFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_conference_list_fragment, container, false);

//        Bundle bundle = getArguments();

        ButterKnife.bind(this, view);
        initialize();

        mContext = getActivity();

        return view;
    }

    private void initialize() {

    }

}
