package ph.coreproc.android.pfa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by IanBlanco on 5/3/2017.
 */

public class SubSessionList {

    @SerializedName("data")
    @Expose
    public ArrayList<SubSession> subSessions;

}
