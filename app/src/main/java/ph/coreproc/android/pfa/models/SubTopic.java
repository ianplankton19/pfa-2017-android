package ph.coreproc.android.pfa.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.ConferenceDetailsActivity;

/**
 * Created by IanBlanco on 4/5/2017.
 */

public class SubTopic implements Serializable {

    @SerializedName("data")
    @Expose
    public ArrayList<SubTopicField> subTopics;


    public class SubTopicField implements Serializable {
        @SerializedName("id")
        @Expose
        public int id;

        @SerializedName("name")
        @Expose
        public String name;

        @SerializedName("day")
        @Expose
        public int day;

        @SerializedName("description")
        @Expose
        public String description;

        @SerializedName("location")
        @Expose
        public String location;

        @SerializedName("starts_at")
        @Expose
        public String startsAt;

        @SerializedName("ends_at")
        @Expose
        public String endsAt;

        @SerializedName("duration")
        @Expose
        public String duration;

        @SerializedName("speakers")
        @Expose
        SpeakersData speakers;

        public class SpeakersData implements Serializable {

            @SerializedName("data")
            @Expose
            public ArrayList<Speaker> speakers;

        }

        public View getView(final Context context, View.OnClickListener onClickListener) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_track_item, null);
            TextView tvName = (TextView) view.findViewById(R.id.tvName);
            TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            LinearLayout llSubTopicContainer = (LinearLayout) view.findViewById(R.id.llSubTopicContainer);
            tvName.setText(name);
            llSubTopicContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(ConferenceDetailsActivity.newIntent(context, id + "", false, true));
                }
            });
//            tvName.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });
            tvDescription.setText(description);

            return view;

        }

    }

}
