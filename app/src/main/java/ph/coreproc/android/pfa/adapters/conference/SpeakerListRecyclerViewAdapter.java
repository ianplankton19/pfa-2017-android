package ph.coreproc.android.pfa.adapters.conference;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Speaker;

/**
 * Created by IanBlanco on 4/5/2017.
 */

public class SpeakerListRecyclerViewAdapter extends RecyclerView.Adapter<SpeakerListRecyclerViewAdapter.ViewHolder> {

    public interface Callback {

        void intentSpeakerDetails(String id);
    }

    Callback mCallback;

    private Context mContext;
    private ArrayList<Speaker> mSpeakerArrayList;

    public SpeakerListRecyclerViewAdapter(Context context, ArrayList<Speaker> speakerArrayList, Callback callback) {
        mContext = context;
        mSpeakerArrayList = speakerArrayList;

        arrangeArray();

        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_speaker_conference_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Speaker speaker = mSpeakerArrayList.get(position);
        holder.mConferenceSpeakerTextView.setText(speaker.speakerName);
        holder.mTvDetails.setText(speaker.speakerCompany);

        if (speaker.avatar != null) {
            Glide.with(mContext)
                    .load(speaker.avatar)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_delete_white)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Log.d("error loading image", "error" + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.mConferenceSpeakerImageView.setImageDrawable(resource);
                            Log.d("error loading image", "success");
                            return false;
                        }
                    })
                    .into(holder.mConferenceSpeakerImageView);
        }



        holder.mRlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.intentSpeakerDetails("" + speaker.id);
            }
        });

        if (position == mSpeakerArrayList.size() - 1) {
            holder.mRlContainer.setBackground(mContext.getDrawable(R.drawable.footer_background));
        }



    }

    @Override
    public int getItemCount() {
        return mSpeakerArrayList.size();
    }

    public void add(ArrayList<Speaker> items) {
        int previousDataSize = this.mSpeakerArrayList.size();
        this.mSpeakerArrayList.addAll(items);
        arrangeArray();
        notifyDataSetChanged();
    }

    public void changeData(ArrayList<Speaker> items) {
        this.mSpeakerArrayList = new ArrayList<>(items);
        this.notifyDataSetChanged();
    }


    public void arrangeArray() {
        Collections.sort(mSpeakerArrayList, new Comparator<Speaker>() {
            @Override
            public int compare(Speaker o1, Speaker o2) {
                return o1.speakerName.compareTo(o2.speakerName);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.conferenceSpeakerImageView)
        CircleImageView mConferenceSpeakerImageView;

        @Bind(R.id.proceed)
        ImageView mProceed;

        @Bind(R.id.conferenceSpeakerTextView)
        TextView mConferenceSpeakerTextView;

        @Bind(R.id.tvDetails)
        TextView mTvDetails;

        @Bind(R.id.rlContainer)
        RelativeLayout mRlContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
