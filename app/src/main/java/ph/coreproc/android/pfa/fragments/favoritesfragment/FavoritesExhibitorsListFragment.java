package ph.coreproc.android.pfa.fragments.favoritesfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.preferences.Preferences;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import database.db.exhibitor_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.ExhibitorDetailsActivity;
import ph.coreproc.android.pfa.adapters.favorites.FavoritesExhibitorRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;

/**
 * Created by IanBlanco on 4/17/2017.
 */

public class FavoritesExhibitorsListFragment extends Fragment implements FavoritesExhibitorRecyclerViewAdapter.Callback {


    @Bind(R.id.rvFavorites)
    RecyclerView mRvFavorites;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    private Context mContext;
    private exhibitor_local_tblDao mExhibitor_local_tblDao;
    private ArrayList<Exhibitor> mExhibitorLocals;

    private User mUser;

    private FavoritesExhibitorRecyclerViewAdapter mFavoritesExhibitorRecyclerViewAdapter;
    private EventBus mEventBus = EventBus.getDefault();
    public static FavoritesExhibitorsListFragment newInstance() {

        FavoritesExhibitorsListFragment favoritesExhibitorsListFragment = new FavoritesExhibitorsListFragment();

        return favoritesExhibitorsListFragment;
    }


    @Override
    public void onResume() {
        super.onResume();



        mExhibitorLocals = DataUtil.getExhibitorLocal(mExhibitor_local_tblDao, mUser.id, mUser.role);
        mFavoritesExhibitorRecyclerViewAdapter.update(mExhibitorLocals);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_favorites_conference_display, null);

        mContext = getActivity();

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);


        mExhibitor_local_tblDao = DataUtil.setUpExhibitorLocal(mContext, Prefs.DB_NAME);

        ButterKnife.bind(this, view);

        initialize();
        return view;
    }

    private void initialize() {

        mExhibitorLocals = new ArrayList<>();

        mFavoritesExhibitorRecyclerViewAdapter = new FavoritesExhibitorRecyclerViewAdapter(mContext, mExhibitorLocals, FavoritesExhibitorsListFragment.this);
        PFABase.setDefaultRecyclerView(mContext, mRvFavorites, mFavoritesExhibitorRecyclerViewAdapter);

        getData();
    }

    private void getData() {

        mExhibitorLocals = DataUtil.getExhibitorLocal(mExhibitor_local_tblDao, mUser.id, mUser.role);
        mFavoritesExhibitorRecyclerViewAdapter.add(mExhibitorLocals);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void intentToDetails(int id) {
        getActivity().startActivity(ExhibitorDetailsActivity.newIntent(mContext, id, true));
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }
}
