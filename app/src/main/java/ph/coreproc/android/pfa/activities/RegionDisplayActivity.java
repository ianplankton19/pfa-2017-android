package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.RegionAdapter;
import ph.coreproc.android.pfa.models.Region;

/**
 * Created by IanBlanco on 4/4/2017.
 */

public class RegionDisplayActivity extends BaseActivity implements RegionAdapter.Callback{

    public static Intent newIntent(Context context){
        Intent intent = new Intent(context, RegionDisplayActivity.class);
        return intent;
    }

    @Bind(R.id.rvRegions)
    RecyclerView mRvRegions;


    RegionAdapter mRegionAdapter;
    List<Region> mRegions;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_region_dialog;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        initialize();
    }


    private void initialize(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRvRegions.setLayoutManager(linearLayoutManager);

        mRegions = new ArrayList<>();

        mRegions = PFABase.getRegions(mContext);

        mRegionAdapter = new RegionAdapter(mRegions, mContext, RegionDisplayActivity.this);
        mRvRegions.setAdapter(mRegionAdapter);


    }

    @Override
    public void getPosition(Region region) {
        EventBus.getDefault().postSticky(region);
        finish();
    }
}
