package ph.coreproc.android.pfa.rest;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.models.NxtGen;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import ph.coreproc.android.pfa.rest.responses.MetaNxtGenWrapper;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by IanBlanco on 5/10/2017.
 */

public interface NxtGenInterface {

    @GET
    Call<MetaNxtGenWrapper<ArrayList<NxtGen>>> getNxtGen(@Url String url);

    @GET("/api/v1/nxtgen/{id}")
    Call<DataWrapper<NxtGen>> getNxtGenDetails(@Path("id") String id);

    @POST("/api/v1/nxtgen/{id}/votes")
    Call<ResponseBody> voteNxtGen(@Path("id") String id);
}
