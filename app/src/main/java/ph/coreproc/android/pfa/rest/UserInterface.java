package ph.coreproc.android.pfa.rest;

import com.google.gson.JsonObject;

import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.Registration;
import ph.coreproc.android.pfa.models.Version;
import ph.coreproc.android.pfa.models.requests.ForgotPasswordBody;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by willm on 3/2/2017.
 */

public interface UserInterface {

    @GET("/api/v1/users/{id}")
    Call<DataWrapper<User>> getUser(@Path("id") String id,
                                    @Query("qr_code") String code);

    @POST
    Call<DataWrapper<User>> loginUser(@Url String url,
                                      @Body JsonObject jsonObject);

    @POST
    Call<DataWrapper<User>> registerUser(@Url String url,
                                         @Body Registration registration);


    @POST
    Call<ResponseBody> forgotPasswordUser(@Url String url,
                                          @Body ForgotPasswordBody forgotPasswordBody);



    @GET
    Call<Version> getVersion(@Url String url);


}
