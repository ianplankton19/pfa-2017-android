package ph.coreproc.android.pfa.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by IanBlanco on 6/13/2017.
 */

public class ForgotPasswordBody {

    public ForgotPasswordBody(String email) {
        this.email = email;
    }

    @SerializedName("email")
    @Expose
    private String email;
}
