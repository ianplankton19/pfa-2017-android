package ph.coreproc.android.pfa.adapters.conference;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Session;
import ph.coreproc.android.pfa.models.SubSession;
import ph.coreproc.android.pfa.models.SubTopic;
import ph.coreproc.android.pfa.models.Topics;

/**
 * Created by willm on 3/28/2017.
 */

public class ConferenceListRecyclerViewAdapter extends RecyclerView.Adapter<ConferenceListRecyclerViewAdapter.ViewHolder> {

    public interface Callback extends DisplayNoDataInterface {
        void displayDetails(Topics topics);
    }

    private Callback mCallback;

    private ArrayList<Session> itemsSession = new ArrayList<>();
    private ArrayList<SubSession> itemsSubSession = new ArrayList<>();
    private ArrayList<Topics> itemsTopic = new ArrayList<>();
    //    private ArrayList<SubTopic.SubTopicField> itemsSubTopic = new ArrayList<>();
    private Context mContext;
    private String mDay;


    public ConferenceListRecyclerViewAdapter(Context context, ArrayList<Session> items, Callback callback, String day) {
        mContext = context;
        mCallback = callback;
        mDay = day;

        for (int i = 0; i < items.size(); i++) {

            boolean checker = false;

            ArrayList<Topics> topicArray = items.get(i).topicList.topicsList;
            if (topicArray.size() != 0) {
                for (int s = 0; s < topicArray.size(); s++) {
                    if (topicArray.get(s).day.equals(mDay)) {

                        checker = true;
                    }
                }


                if (checker) {
                    this.itemsSession.add(items.get(i));
                }

//                return;
            } else {

                ArrayList<SubSession> subSessionArray = items.get(i).subSession.subSessions;
                for (int d = 0; d < subSessionArray.size(); d++) {
                    ArrayList<Topics> topicsItem = subSessionArray.get(d).topicList.topicsList;
//                holder.mLlContainer.addView(itemsSubSession.get(i).getViewDisplay(mContext, onClickListener, itemsTopics, mDay));
                    for (Topics topics : topicsItem) {
                        if (topics.day.equals(mDay)) {
//                        itemsSession.add(items.get(i));
                            checker = true;
                        }
                    }
                }

                if (checker) {
                    this.itemsSession.add(items.get(i));
                }

                Log.i("check", "checker: " + checker);
            }
        }
//        for (Topics topics : items) {
//            if (topics.day.equals(mDay)) {
//                this.itemsSession.add(topics);
//            }
//        }

    }

    @Override
    public ConferenceListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_session_item, parent, false);
        ViewHolder conferenceViewHolder = new ViewHolder(view);
        return conferenceViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Session session = itemsSession.get(position);
        holder.mConferenceNameTextView.setText(session.sessionName);
        holder.mConferenceTimeTextView.setText(PFABase.getTime(session.startTime) + " - " + PFABase.getTime(session.endTime));

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mCallback.displayDetails(topics);
            }
        };


//        boolean subTopicCheck = topics.subTopic.subTopics.size() > 0;
//
//        if (!subTopicCheck) {
//            holder.mConferenceNameTextView.setTextColor(Color.parseColor("#2e3034"));
//            holder.mConferenceNameTextView.setOnClickListener(onClickListener);
//        }
//
//        itemsSubTopic = session.topicList.topicsList..subTopic.subTopics;
//
        itemsSubSession = session.subSession.subSessions;
        itemsTopic = session.topicList.topicsList;
        if (itemsTopic.size() != 0) {
            for (int i = 0; i < itemsTopic.size(); i++) {
                if (itemsTopic.get(i).day.equals(mDay)) {
                    ArrayList<SubTopic.SubTopicField> itemsSubtopic = itemsTopic.get(i).subTopic.subTopics;
                    holder.mLlTopicContainer.addView(itemsTopic.get(i).getViewDisplay(mContext, onClickListener, itemsSubtopic, mDay));
                }
            }


            return;
        }

        for (int i = 0; i < itemsSubSession.size(); i++) {
            ArrayList<Topics> itemsTopics = itemsSubSession.get(i).topicList.topicsList;
            holder.mLlContainer.addView(itemsSubSession.get(i).getViewDisplay(mContext, onClickListener, itemsTopics, mDay));
        }

    }


    public void add(ArrayList<Session> items) {

//        for (Topics topics : items) {
//            if (topics.day.equals(mDay)) {
//                this.itemsSession.add(topics);
//            }
//        }
        for (int i = 0; i < items.size(); i++) {

            boolean checker = false;

            ArrayList<Topics> topicArray = items.get(i).topicList.topicsList;
            if (topicArray.size() != 0) {
                for (int s = 0; s < topicArray.size(); s++) {
                    if (topicArray.get(s).day.equals(mDay)) {

                        checker = true;
                    }
                }


                if (checker) {
                    this.itemsSession.add(items.get(i));
                }

//                return;
            } else {

                ArrayList<SubSession> subSessionArray = items.get(i).subSession.subSessions;
                for (int d = 0; d < subSessionArray.size(); d++) {
                    ArrayList<Topics> topicsItem = subSessionArray.get(d).topicList.topicsList;
//                holder.mLlContainer.addView(itemsSubSession.get(i).getViewDisplay(mContext, onClickListener, itemsTopics, mDay));
                    for (Topics topics : topicsItem) {
                        if (topics.day.equals(mDay)) {
//                        itemsSession.add(items.get(i));
                            checker = true;
                        }
                    }
                }

                if (checker) {
                    this.itemsSession.add(items.get(i));
                }

                Log.i("check", "checker: " + checker);
                Log.i("check", "checker: " + this.itemsSession.size());
            }
        }
        displayNoData(this.itemsSession);
        notifyDataSetChanged();
    }

    private void displayNoData(ArrayList<Session> items) {

        if (items.size() > 0 == false) {
            mCallback.displayNoDataText();
            return;
        }
        mCallback.removeNoDataText();
    }

    @Override
    public int getItemCount() {
        return itemsSession.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.llContainer)
        LinearLayout mLlContainer;

        @Bind(R.id.conferenceNameTextView)
        TextView mConferenceNameTextView;

        @Bind(R.id.conferenceTimeTextView)
        TextView mConferenceTimeTextView;

        @Bind(R.id.llTopicContainer)
        LinearLayout mLlTopicContainer;

        @Bind(R.id.llSessionContainerMain)
        LinearLayout mLlSessionContainerMain;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

