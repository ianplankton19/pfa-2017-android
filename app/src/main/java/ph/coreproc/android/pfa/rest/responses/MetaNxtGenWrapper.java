package ph.coreproc.android.pfa.rest.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 5/24/2017.
 */

public class MetaNxtGenWrapper<T> extends DataWrapper<T> implements Serializable {

    @SerializedName("meta")
    @Expose
    private Meta meta;

    public Meta getMeta() {
        return meta;
    }

    public class Meta {
        @SerializedName("key")
        @Expose
        private boolean key;
        public boolean isKey() {
            return key;
        }

    }
}
