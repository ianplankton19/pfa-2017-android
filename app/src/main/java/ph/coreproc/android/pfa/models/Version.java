package ph.coreproc.android.pfa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 6/14/2017.
 */

public class Version implements Serializable {

    @SerializedName("android")
    @Expose
    public String current;



}
