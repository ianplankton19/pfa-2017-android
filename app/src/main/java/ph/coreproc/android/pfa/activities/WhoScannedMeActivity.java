package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import butterknife.Bind;
import ph.coreproc.android.pfa.R;

public class WhoScannedMeActivity extends BaseActivity {

    @Bind(R.id.scannedRecyclerView)
    RecyclerView scannedRecyclerView;

    @Bind(R.id.scannedDaysTabLayout)
    TabLayout scannedDaysTabLayout;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, WhoScannedMeActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupTabLayout();

    }

    private void setupTabLayout() {
        if (scannedDaysTabLayout != null) {
            TabLayout.Tab tab = scannedDaysTabLayout.newTab();
            tab.setText("Exhibitors");
            scannedDaysTabLayout.addTab(tab);
            tab = scannedDaysTabLayout.newTab();
            tab.setText("Visitors");
            scannedDaysTabLayout.addTab(tab);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_who_scanned_me;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
