package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.WebViewViewPagerAdapter;
import ph.coreproc.android.pfa.fragments.webviewfragment.WebViewFragmentOne;
import ph.coreproc.android.pfa.fragments.webviewfragment.WebViewFragmentTwo;

/**
 * Created by IanBlanco on 3/28/2017.
 */

public class ExpoMapActivity extends BaseActivity {

    public static Intent newIntent(Context context){
        Intent intent = new Intent(context, ExpoMapActivity.class);
      return intent;
    }

    @Bind(R.id.tlExpoMap)
    TabLayout mTlExpoMap;

    @Bind(R.id.vpExpoMap)
    ViewPager mVpExpoMap;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_webview_viewpager;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialize();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initialize(){
        setViewPager();
        mTlExpoMap.setupWithViewPager(mVpExpoMap);
    }

    private void setViewPager(){
        WebViewViewPagerAdapter  adapter = new WebViewViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(WebViewFragmentOne.newInstance(), "First Floor");
        adapter.addFragment(WebViewFragmentTwo.newInstance(), "Second Floor");

        mVpExpoMap.setAdapter(adapter);
        mVpExpoMap.setOffscreenPageLimit(adapter.getCount());
    }

}
