package ph.coreproc.android.pfa.adapters.favorites;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.DisplayNoDataInterface;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.Speaker;

/**
 * Created by IanBlanco on 4/15/2017.
 */

public class FavoriteSpeakerRecyclerViewAdapter extends RecyclerView.Adapter<FavoriteSpeakerRecyclerViewAdapter.ViewHolder> {



    public interface Callback extends DisplayNoDataInterface {
        void intentToDetails(int id);

    }

    private Context mContext;
    private ArrayList<Speaker> mSpeakerLocals;

    private Callback mCallback;

    public FavoriteSpeakerRecyclerViewAdapter(Context context, ArrayList<Speaker> speakerLocals, Callback callback) {
        mContext = context;
        mSpeakerLocals = speakerLocals;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_speaker_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Speaker speakerLocal = mSpeakerLocals.get(position);
        Glide.with(mContext)
                .load(speakerLocal.avatar)
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .into(holder.mSpeakerImageView);
        holder.mSpeakerNameTextView.setText(speakerLocal.speakerName);
        holder.mLlSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.intentToDetails(speakerLocal.id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mSpeakerLocals.size();
    }

    public void add(ArrayList<Speaker> items) {
        mSpeakerLocals.addAll(items);
        displayNoData(mSpeakerLocals);
        notifyDataSetChanged();
    }

    public void update(ArrayList<Speaker> items) {
        mSpeakerLocals = items;
        displayNoData(mSpeakerLocals);
        notifyDataSetChanged();
    }

    private void displayNoData(ArrayList<Speaker> items) {

        if (items.size() > 0 == false) {
            mCallback.displayNoDataText();
            return;
        }
        mCallback.removeNoDataText();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.speakerImageView)
        ImageView mSpeakerImageView;

        @Bind(R.id.speakerNameTextView)
        TextView mSpeakerNameTextView;

        @Bind(R.id.boothNumberTextView)
        TextView mBoothNumberTextView;

        @Bind(R.id.llSpeaker)
        LinearLayout mLlSpeaker;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
