package ph.coreproc.android.pfa.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MenuItem;

import butterknife.Bind;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.conference.ConferenceViewPagerAdapter;
import ph.coreproc.android.pfa.fragments.conference.ConferenceFirstDayNewFragment;
import ph.coreproc.android.pfa.fragments.conference.ConferenceSecondDayNewFragment;

public class ConferenceListActivity extends BaseActivity {

    @Bind(R.id.conferenceDaysTabLayout)
    TabLayout conferenceDaysTabLayout;

    @Bind(R.id.conferenceListViewPager)
    ViewPager conferenceListViewPager;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ConferenceListActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();

    }

    private void initialize() {
        setViewPager();
        conferenceDaysTabLayout.setupWithViewPager(conferenceListViewPager);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_conference_list;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setViewPager() {

        ConferenceViewPagerAdapter adapter = new ConferenceViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(ConferenceFirstDayNewFragment.newInstance(), "Day 1");
        adapter.addFragment(ConferenceSecondDayNewFragment.newInstance(), "Day 2");

        conferenceListViewPager.setAdapter(adapter);
        conferenceListViewPager.setOffscreenPageLimit(adapter.getCount());
    }
}
