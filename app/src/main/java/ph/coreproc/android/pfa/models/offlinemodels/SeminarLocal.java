package ph.coreproc.android.pfa.models.offlinemodels;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ph.coreproc.android.pfa.models.Seminar;

/**
 * Created by IanBlanco on 4/10/2017.
 */

public class SeminarLocal implements Serializable {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("location")
    @Expose
    public String location;

    @SerializedName("starts_at")
    @Expose
    public String startsAt;

    public JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", this.id);
        jsonObject.addProperty("name", this.name);
        jsonObject.addProperty("location", this.location);
        jsonObject.addProperty("starts_at", this.startsAt);

        return jsonObject;
    }

    public SeminarLocal(Seminar seminar) {
        this.id = seminar.id;
        this.name = seminar.name;
        this.location = seminar.location;
        this.startsAt = seminar.startsAt;
    }

    public SeminarLocal(){

    }


}
