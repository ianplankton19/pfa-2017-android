package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.requests.ForgotPasswordBody;
import ph.coreproc.android.pfa.rest.UserInterface;
import ph.coreproc.android.pfa.utils.UiUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 6/13/2017.
 */

public class ForgotPasswordActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.etUsername)
    EditText mEtUsername;

    @Bind(R.id.btnSubmit)
    Button mBtnSubmit;

    @Bind(R.id.forgotPassword)
    TextView mForgotPassword;

    public static Intent newIntent(Context context) {

        Intent intent = new Intent(context, ForgotPasswordActivity.class);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_forgot_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_dark);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnSubmit)
    public void onClick() {
        if (mEtUsername.getText().toString().trim().equals("")) {
            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up E-mail address.");
            return;
        }

        if (!PFABase.isEmailValid(mEtUsername.getText().toString().trim())) {
            UiUtil.showMessageDialog(getSupportFragmentManager(), "Wrong format of E-mail.");
            return;
        }

        ForgotPasswordBody forgotPasswordBody = new ForgotPasswordBody(mEtUsername.getText().toString().trim());

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();

        UserInterface userInterface = KitchenRestClient.create(mContext, UserInterface.class, false);

        Call<ResponseBody> forgotPasswordCall = userInterface.forgotPasswordUser(getString(R.string.forgot_password),
                forgotPasswordBody);

        forgotPasswordCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (!response.isSuccessful()) {
                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                PFABase.dialogWithOnClick(mContext, "New password has been sent to your E-mail.", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
            }
        });

    }
}
