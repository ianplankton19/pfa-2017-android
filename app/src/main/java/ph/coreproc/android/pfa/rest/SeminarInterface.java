package ph.coreproc.android.pfa.rest;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.models.Seminar;
import ph.coreproc.android.pfa.models.requests.CommentBody;
import ph.coreproc.android.pfa.models.requests.QuestionBody;
import ph.coreproc.android.pfa.models.requests.RatingBody;
import ph.coreproc.android.pfa.rest.responses.DataWrapper;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by IanBlanco on 4/5/2017.
 */

public interface SeminarInterface {

    @GET
    Call<DataWrapper<ArrayList<Seminar>>> getSeminar(@Url String url);

    @GET("/api/v1/seminars/{id}")
    Call<DataWrapper<Seminar>> getSeminarDetails(@Path("id") String id);

    @POST("/api/v1/seminars/{id}/comments")
    Call<ResponseBody> postSeminarsComment(@Path("id") String id, @Body CommentBody commentBody);

    @POST("/api/v1/seminars/{id}/ratings")
    Call<ResponseBody> postSeminarsRating(@Path("id") String id, @Body RatingBody ratingBody);

    @POST("/api/v1/seminars/{id}/questions")
    Call<ResponseBody> postSeminarQuestion(@Path("id") String id, @Body QuestionBody questionBody);
}
