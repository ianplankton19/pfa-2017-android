package ph.coreproc.android.pfa;

/**
 * Created by IanBlanco on 4/26/2017.
 */

public interface DisplayNoDataInterface {

    void removeNoDataText();
    void displayNoDataText();
}
