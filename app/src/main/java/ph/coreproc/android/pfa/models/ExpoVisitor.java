package ph.coreproc.android.pfa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 5/8/2017.
 */

public class ExpoVisitor implements Serializable{

    @SerializedName("data")
    @Expose
    public ExpoVisitorContent mExpoVisitorContent;

    public class ExpoVisitorContent implements Serializable {

        @SerializedName("expo_visit_id")
        @Expose
        public int expoVisitId;

        @SerializedName("full_name")
        @Expose
        public String fullName;

        @SerializedName("region")
        @Expose
        public String region;

        @SerializedName("gender")
        @Expose
        public String gender;

        @SerializedName("company")
        @Expose
        public String company;

        @SerializedName("mobile")
        @Expose
        public String mobile;

        @SerializedName("phone")
        @Expose
        public String phone;

        @SerializedName("age_group")
        @Expose
        public String ageGroup;

        @SerializedName("occupation")
        @Expose
        public String occupation;

        @SerializedName("position_title")
        @Expose
        public String positionTitle;

        @SerializedName("purpose_of_visit")
        @Expose
        public String purposeOfVisit;

        @SerializedName("area_of_interest")
        @Expose
        public String areaOfInterest;

        @SerializedName("franchise_package")
        @Expose
        public String franchisePackage;

        @SerializedName("existing")
        @Expose
        public String existing;

        @SerializedName("frequency_visit")
        @Expose
        public String frequencyVisit;

        @SerializedName("master_franchise")
        @Expose
        public String masterFranchise;

        @SerializedName("source_of_info")
        @Expose
        public String sourceOfInfo;
    }
 }
