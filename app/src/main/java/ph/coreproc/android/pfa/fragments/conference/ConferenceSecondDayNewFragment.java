package ph.coreproc.android.pfa.fragments.conference;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.ConferenceDetailsActivity;

/**
 * Created by willm on 3/24/2017.
 */

public class ConferenceSecondDayNewFragment extends Fragment {

    @Bind(R.id.tvCustomText)
    TextView mTvCustomText;


    private Context mContext;


    public static ConferenceSecondDayNewFragment newInstance() {
        ConferenceSecondDayNewFragment conferenceSecondDayNewFragment = new ConferenceSecondDayNewFragment();

        return conferenceSecondDayNewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_secondday_conference, container, false);
//        Bundle bundle = getArguments();
        ButterKnife.bind(this, view);
        mContext = getActivity();
        initialize();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser) {
                initialize();
            }

        }
    }

    private void initialize() {

        Log.d("iteems", "dumaan");

        mTvCustomText.setText(Html.fromHtml("<b>Part I. BUSINESS SOLUTION<br>ROUNDTABLES DISCUSSION<br>9:00AM-10:00AM:<br>Business Solution Rountables I<br>" +
                "</b><i>Break: 10:00AM-10:15AM</i><br><b>10:15AM-11:15AM:<br>Business Solution Rountables II</b>"));

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.llTopicSessionOneKeyNote,
            R.id.llTopicDevelop1, R.id.llTopicDevelop2, R.id.llInnovativeWays, R.id.llTopicTipsForBuilding,
            R.id.llEffectiveStrategies, R.id.llManagingAndSupporting, R.id.llHandlingDifficult, R.id.llTopicHowToGetFranchisees, R.id.llBestPracticesInIntlTwo,
            R.id.llImprovingEmployee, R.id.llInnovationInLocalStore, R.id.llToolsAndWays, R.id.llBuildingAWinWin, R.id.llTopicRetainingTopic,
            R.id.llBestPracticesInIntl, R.id.llHandlingDifficultAndStrugglingFranchisees,
            R.id.llMaximizingGoogle, R.id.llInnovationsInHR, R.id.llUsingOmniChannels, R.id.llBuildingASuccessful,
            R.id.llTopicSessionTwoAmazingTechnology,
            R.id.llTopicSessionThreeTheMarketingForum,
            R.id.llTopicHomeMobileMovesBusiness,
            R.id.llTopicUsingMCommerce,
            R.id.llTopicSessionFourAnInspiringJourney,
            R.id.llTopicSessionFiveInternationalForum,
            R.id.llTopicSessionSixTheZeeForum})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.llTopicDevelop1:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "26", false, false));
                break;
            case R.id.llTopicDevelop2:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "52", false, false));
                break;
            case R.id.llInnovativeWays:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "28", false, false));
                break;
            case R.id.llTopicTipsForBuilding:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "29", false, false));
                break;

            case R.id.llEffectiveStrategies:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "30", false, false));
                break;
            case R.id.llManagingAndSupporting:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "31", false, false));
                break;
            case R.id.llHandlingDifficult:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "32", false, false));
                break;
            case R.id.llTopicRetainingTopic:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "63", false, false));
                break;
            case R.id.llTopicHowToGetFranchisees:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "55", false, false));
                break;
            case R.id.llBestPracticesInIntlTwo:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "56", false, false));
                break;
            case R.id.llHandlingDifficultAndStrugglingFranchisees:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "32", false, false));
                break;


            case R.id.llImprovingEmployee:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "34", false, false));
                break;
            case R.id.llInnovationInLocalStore:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "35", false, false));
                break;
            case R.id.llToolsAndWays:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "36", false, false));
                break;
            case R.id.llBuildingAWinWin:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "37", false, false));
                break;

            case R.id.llBestPracticesInIntl:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "38", false, false));
                break;

            case R.id.llMaximizingGoogle:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "39", false, false));
                break;
            case R.id.llInnovationsInHR:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "40", false, false));
                break;
            case R.id.llUsingOmniChannels:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "41", false, false));
                break;
            case R.id.llBuildingASuccessful:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "42", false, false));
                break;

            case R.id.llTopicSessionOneKeyNote:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "43", false, true));
                break;

            case R.id.llTopicSessionTwoAmazingTechnology:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "44", false, true));
                break;

            case R.id.llTopicSessionThreeTheMarketingForum:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "45", false, true));
                break;

            case R.id.llTopicHomeMobileMovesBusiness:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "46", false, true));
                break;
            case R.id.llTopicUsingMCommerce:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "47", false , true));
                break;
            case R.id.llTopicSessionFourAnInspiringJourney:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "48", false , true));
                break;
            case R.id.llTopicSessionFiveInternationalForum:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "49", false , true));
                break;
            case R.id.llTopicSessionSixTheZeeForum:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "50", false , true));
                break;
        }
    }
}
