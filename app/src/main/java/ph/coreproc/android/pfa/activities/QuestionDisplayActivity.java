package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.requests.QuestionBody;
import ph.coreproc.android.pfa.rest.ConferenceInterface;
import ph.coreproc.android.pfa.rest.ExhibitorInterface;
import ph.coreproc.android.pfa.rest.SeminarInterface;
import ph.coreproc.android.pfa.rest.SpeakerInterface;
import ph.coreproc.android.pfa.utils.UiUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IanBlanco on 4/18/2017.
 */

public class QuestionDisplayActivity extends BaseActivity {

    @Bind(R.id.etQuestion)
    EditText mEtQuestion;

    @Bind(R.id.btnSend)
    Button mBtnSend;

    private Call<ResponseBody> mResponseBodyCall;

    private static String ARGS_ID = "ARGS_ID";
    private int mId;

    private static String ARGS_ACTIVITY = "ARGS_ACTIVITY";
    private String mActivityFrom = "";

    public static Intent newIntent(Context context, int id, String activity) {
        Intent intent = new Intent(context, QuestionDisplayActivity.class);
        intent.putExtra(ARGS_ID, id);
        intent.putExtra(ARGS_ACTIVITY, activity);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_dialog_question;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        getWindow().set
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        initialize();
    }

    @OnClick(R.id.btnSend)
    public void onClick() {
        if (mEtQuestion.getText().toString().equals("")) {
            UiUtil.showMessageDialog(getSupportFragmentManager(), "Please fill up first the Question box.");
            return;
        }
        sendQuestion(mEtQuestion.getText().toString());
//        finish();
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();
        mId = extras.getInt(ARGS_ID);
        mActivityFrom = extras.getString(ARGS_ACTIVITY);
    }

    private void sendQuestion(String question) {

        QuestionBody questionBody = new QuestionBody(question);
        switch (mActivityFrom) {
            case "Conference":
                ConferenceInterface conferenceInterface = new KitchenRestClient().create(mContext, ConferenceInterface.class, true);
                mResponseBodyCall = conferenceInterface.postConferenceQuestion(mId + "", questionBody);
                break;
            case "Seminar":
                SeminarInterface seminarInterface = new KitchenRestClient().create(mContext, SeminarInterface.class, true);
                mResponseBodyCall = seminarInterface.postSeminarQuestion(mId + "", questionBody);
                break;
            case "Exhibitor":
                ExhibitorInterface exhibitorInterface = new KitchenRestClient().create(mContext, ExhibitorInterface.class, true);
                mResponseBodyCall = exhibitorInterface.postExhibitorQuestion(mId + "", questionBody);
                break;
            case "Speaker":
                SpeakerInterface speakerInterface = new KitchenRestClient().create(mContext, SpeakerInterface.class, true);
                mResponseBodyCall = speakerInterface.postSpeakerQuestion(mId + "", questionBody);
                break;
        }

        final Dialog dialog = PFABase.loadDefaultProgressDialog(mContext);
        dialog.show();
        mResponseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();

                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    if (response.code() == 405) {
                        PFABase.dialogWithOnClickCancelable(mContext, "" + error.getError().getMessage(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        return;
                    }


                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
//                    PFABase.dialogWithOnClickCancelable(mContext, "" + error.getError().getMessage(), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            finish();
//                        }
//                    }).show();

//                    finish();
                    return;
                }

                PFABase.dialogWithOnClickCancelable(mContext, "Question Sent", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global));
//                finish();
            }
        });

    }
}
