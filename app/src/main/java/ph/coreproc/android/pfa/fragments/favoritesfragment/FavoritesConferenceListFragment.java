package ph.coreproc.android.pfa.fragments.favoritesfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.preferences.Preferences;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import database.db.conference_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.ConferenceDetailsActivity;
import ph.coreproc.android.pfa.adapters.favorites.FavoritesConferenceRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.Topics;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;


/**
 * Created by IanBlanco on 4/6/2017.
 */

public class FavoritesConferenceListFragment extends Fragment implements FavoritesConferenceRecyclerViewAdapter.Callback {

    @Bind(R.id.rvFavorites)
    RecyclerView mRvFavoritesConference;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    FavoritesConferenceRecyclerViewAdapter mFavoritesConferenceRecyclerViewAdapter;

    ArrayList<Topics> mTopicsArrayList;
    private Context mContext;
    private User mUser;


    private conference_local_tblDao mConference_local_tblDao;

    public static FavoritesConferenceListFragment newInstance() {

        Bundle args = new Bundle();

        FavoritesConferenceListFragment fragment = new FavoritesConferenceListFragment();
        //   fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        mTopicsArrayList = DataUtil.getTopicLocals(mConference_local_tblDao, mUser.id, mUser.role);
        mFavoritesConferenceRecyclerViewAdapter.update(mTopicsArrayList);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_favorites_conference_display, container, false);
        ButterKnife.bind(this, view);

        mContext = getActivity();

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        mConference_local_tblDao = DataUtil.setUpConferenceDB(getActivity(), Prefs.DB_NAME);
        initialize();

        return view;

    }

    private void initialize() {
        mTopicsArrayList = new ArrayList<>();
        mRvFavoritesConference.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFavoritesConferenceRecyclerViewAdapter = new FavoritesConferenceRecyclerViewAdapter(getContext(), mTopicsArrayList, FavoritesConferenceListFragment.this);

        mRvFavoritesConference.setAdapter(mFavoritesConferenceRecyclerViewAdapter);

        getData();


    }


    private void getData() {

//        String response = Prefs.getString(getActivity(), Prefs.FAVORITES_CONFERENCE);
//        Log.i("tag", "Response:" + response);
//        TopicLocalData topicLocalData = ModelUtil.fromJson(TopicLocalData.class, response);

        mTopicsArrayList = DataUtil.getTopicLocals(mConference_local_tblDao, mUser.id, mUser.role);

//        try {
//            mTopicsArrayList = topicLocalData.topicsData;
//        } catch (Exception e) {
//            //No Data;
//        }

        mFavoritesConferenceRecyclerViewAdapter.add(mTopicsArrayList);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void intentToDetails(String id) {
        startActivity(ConferenceDetailsActivity.newIntent(getActivity(), id, true, true));
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }
}
