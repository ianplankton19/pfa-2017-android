package ph.coreproc.android.pfa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.User;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ph.coreproc.android.pfa.R;

/**
 * Created by willm on 3/28/2017.
 */

public class ScannedMeRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<User> items;
    Context mContext;


    public ScannedMeRecyclerViewAdapter(Context context, ArrayList<User> items) {
        mContext = context;
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user_item, parent, false);
        UserViewHolder SeminarViewHolder = new UserViewHolder(view);
        return SeminarViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    public void add(ArrayList<User> items) {
        int previousDataSize = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.userNameTextView)
        TextView userNameTextView;

        @Bind(R.id.userTimeTextView)
        TextView userTimeTextView;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}