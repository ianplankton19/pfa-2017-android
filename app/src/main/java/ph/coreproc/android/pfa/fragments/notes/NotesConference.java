package ph.coreproc.android.pfa.fragments.notes;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coreproc.android.kitchen.preferences.Preferences;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import database.db.notes_local_tblDao;
import ph.coreproc.android.pfa.DataUtil;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.NotesActivity;
import ph.coreproc.android.pfa.adapters.notes.NotesConferenceListAdapter;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.models.offlinemodels.Notes;
import ph.coreproc.android.pfa.utils.ModelUtil;
import ph.coreproc.android.pfa.utils.Prefs;
import ph.coreproc.android.pfa.utils.UiUtil;

/**
 * Created by IanBlanco on 5/29/2017.
 */

public class NotesConference extends Fragment implements NotesConferenceListAdapter.Callback {


    @Bind(R.id.tvTitle)
    TextView mTvTitle;

    @Bind(R.id.ivDelete)
    ImageView mIvDelete;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.rvNotes)
    RecyclerView mRvNotes;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    @Bind(R.id.llNoDataContainer)
    LinearLayout mLlNoDataContainer;

    @Bind(R.id.activity_notes_list)
    LinearLayout mActivityNotesList;

    private NotesConferenceListAdapter mNotesConferenceListAdapter;
    private ArrayList<Notes> mNotesArrayList;
    private ArrayList<Long> mIds;
    private notes_local_tblDao mNotes_local_tblDao;

    public boolean mShowCheckBox = false;
    private boolean mMarkAll = false;

    private Context mContext;
    private User mUser;

    public static NotesConference newInstance() {

        NotesConference notesConference = new NotesConference();

        return notesConference;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_notes_list, container, false);
        ButterKnife.bind(this, view);
        mNotes_local_tblDao = DataUtil.setUpNotesLocal(getActivity(),
                Prefs.DB_NAME);
        initialize();
        return view;
    }



    private void initialize() {
        mNotesArrayList = new ArrayList<>();
        mIds = new ArrayList<>(DataUtil.getNoteIds(mNotes_local_tblDao));
        mRvNotes.setLayoutManager(new LinearLayoutManager(getActivity()));

        mContext = getActivity();

        String response = Preferences.getString(mContext, Prefs.USER);
        mUser = ModelUtil.fromJson(User.class, response);

        mNotesConferenceListAdapter = new NotesConferenceListAdapter(getActivity(), mNotesArrayList, this, mIds, mNotes_local_tblDao, NotesConference.this);
        mRvNotes.setAdapter(mNotesConferenceListAdapter);

        getData();

    }


    private void getData() {

        ArrayList<Notes> mNotesList = new ArrayList<>(DataUtil.getNotesLocal(mNotes_local_tblDao, mUser.id, mUser.role));
        ArrayList<Notes> mNoteToPass = new ArrayList<>();
        for (Notes notes : mNotesList) {

         if (notes.category.equals("Conference")) {
             mNoteToPass.add(notes);
         }

        }

        mNotesConferenceListAdapter.add(mNoteToPass);
//        initialize(mNotesList);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }

    @Override
    public void intentToSavedNote(int id, String noteName, String category) {
        startActivity(NotesActivity.newIntent(getActivity(), id, noteName, category));
    }

    @Override
    public void showCheckBox(int id) {
        mIvDelete.setVisibility(View.VISIBLE);
        mShowCheckBox = true;
        mNotesArrayList = mNotesConferenceListAdapter.removeMarkData();
        for (Notes notes : mNotesArrayList) {
            if (notes.id == id) {
                notes.isChecked = true;
            }
        }
        mNotesConferenceListAdapter.updateList(arrayToPass(mShowCheckBox));
    }

    @Override
    public void deleteMarkedNote(ArrayList<Integer> ids) {

    }

    @OnClick(R.id.ivDelete)
    public void onClick() {
        if (mNotesConferenceListAdapter.noMarkedData()) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
//                            mNotesConferenceListAdapter.deleteMarked();
                            mShowCheckBox = false;
                            mNotesArrayList = mNotesConferenceListAdapter.getLatestData();
                            mIvDelete.setVisibility(View.GONE);
                            mNotesConferenceListAdapter.updateList(arrayToPass(mShowCheckBox));
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            dialog.dismiss();
                            break;
                    }

                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Are you sure you want to delete marked notes?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
        UiUtil.showMessageDialog(getActivity().getSupportFragmentManager(), "No marked notes");

    }


    private ArrayList<Notes> arrayToPass(boolean isShow) {

        for (Notes notes : mNotesArrayList) {
            notes.isShowed = isShow;
        }
        return mNotesArrayList;
    }
}
