package ph.coreproc.android.pfa.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by IanBlanco on 6/2/2017.
 */
public class DownloadBody {

    public DownloadBody(String ids, String email) {
        this.ids = ids;
        this.email = email;
    }

    public DownloadBody(String email) {
        this.email = email;
    }

    @SerializedName("ids")
    @Expose
    private String ids;

    @SerializedName("email")
    @Expose
    private String email;
}
