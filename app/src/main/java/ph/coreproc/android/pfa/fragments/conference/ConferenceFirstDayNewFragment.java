package ph.coreproc.android.pfa.fragments.conference;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.activities.ConferenceDetailsActivity;
import ph.coreproc.android.pfa.activities.ConferenceListActivity;

/**
 * Created by IanBlanco on 3/24/2017.
 */

public class ConferenceFirstDayNewFragment extends Fragment {

    private Context mContext;


    public static ConferenceFirstDayNewFragment newInstance() {
        ConferenceFirstDayNewFragment conferenceFirstDayNewFragment = new ConferenceFirstDayNewFragment();

        return conferenceFirstDayNewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_firstday_conference, container, false);
//        Bundle bundle = getArguments();
        ButterKnife.bind(this, view);
        mContext = getActivity();
        initialize();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser) {
                initialize();
            }

        }
    }

    private void initialize() {

        Log.d("iteems", "dumaan");

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.llFranchiseeTrack, R.id.llTopicBrandTracking, R.id.llTopicDigitalMarketingTrack, R.id.llTopicDigitalMarketingTrack2, R.id.llSubMobileDigitalMarketing, R.id.llSubInnovationInECommerce, R.id.llTopicGeneralTrack, R.id.llSubTopicRecruitungAndRetrainingMillenialEmployees, R.id.llSubTopicRecruitungAndRetrainingMillenialFranchisees, R.id.llTopicFranchiseRelationsTrack,
            R.id.llTopicWelcome, R.id.llTopicFaphlOverall, R.id.llTopicConferenceOpeningCeremonies, R.id.llTopicKeyNoteAddress, R.id.llTopicTalentCompetencyDiversity, R.id.llSubTopicSourceOfFunds, R.id.llSubTopicEnablingAccessToFinance,
            R.id.llTopicSessionOneConference, R.id.llTopicSpecialSession, R.id.llTopicSessionTwoInnovation, R.id.llTopicMarketingInnovation, R.id.llTopicHowGoogleCanHelp, R.id.llTopicSessionThreeAsean, R.id.llTopicLegaAndRegulatory, R.id.llTopicEnhancingFinancial, R.id.llTopicFindingtheRightPartner,
            R.id.llTopicSessionFourTheCSuite, R.id.llTopicInnovatingTheTranspo, R.id.llTopicInnovatingTheCustomerShopping, R.id.llTopicETheCustomerFood, R.id.llTopicFinanceTrack})
    public void onClick(View view) {
        switch (view.getId()) {

            // PART I

            case R.id.llFranchiseeTrack:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "1", false, true));
                break;
//            case R.id.llTopicDigitalMarketingTrack:
//                startActivity(ConferenceDetailsActivity.newIntent(mContext, "2", false));
//                break;
            case R.id.llSubMobileDigitalMarketing:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "3", false, true));
                break;
            case R.id.llTopicDigitalMarketingTrack2:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "57", false, true));
                break;
            case R.id.llTopicTalentCompetencyDiversity:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "20", false, true));
                break;
            case R.id.llSubInnovationInECommerce:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "4", false, true));
                break;
            case R.id.llSubTopicSourceOfFunds:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "59", false, true));
                break;
            case R.id.llSubTopicEnablingAccessToFinance:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "60", false, true));
                break;
//            case R.id.llTopicGeneralTrack:
//                startActivity(ConferenceDetailsActivity.newIntent(mContext, "5", false));
//                break;
            case R.id.llSubTopicRecruitungAndRetrainingMillenialEmployees:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "6", false, true));
                break;
            case R.id.llSubTopicRecruitungAndRetrainingMillenialFranchisees:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "7", false, true));
                break;
            case R.id.llTopicFranchiseRelationsTrack:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "8", false, true));
                break;
            case R.id.llTopicBrandTracking:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "9", false, true));
                break;

//            case R.id.llTopicFinanceTrack:
//                startActivity(ConferenceDetailsActivity.newIntent(mContext, "54", false, true));
//                break;


            // PART II

//            case R.id.llTopicFaphlOverall:
//                startActivity(ConferenceDetailsActivity.newIntent(mContext, "11", false, true));
//                break;
//            case R.id.llTopicConferenceOpeningCeremonies:
//                startActivity(ConferenceDetailsActivity.newIntent(mContext, "12", false, true));
//                break;
            case R.id.llTopicKeyNoteAddress:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "13", false, true));
                break;



            //PART III

            case R.id.llTopicSessionOneConference:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "14", false, true));
                break;
//            case R.id.llTopicSpecialSession:
//                startActivity(ConferenceDetailsActivity.newIntent(mContext, "14", false));
//                break;
            case R.id.llTopicSessionTwoInnovation:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "15", false, true));
                break;
            case R.id.llTopicMarketingInnovation:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "16", false, true));
                break;
            case R.id.llTopicHowGoogleCanHelp:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "17", false, true));
                break;
            case R.id.llTopicSessionThreeAsean:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "18", false, true));
                break;
            case R.id.llTopicLegaAndRegulatory:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "19", false, true));
                break;
            case R.id.llTopicEnhancingFinancial:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "20", false, true));
                break;
            case R.id.llTopicFindingtheRightPartner:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "21", false, true));
                break;
            case R.id.llTopicSessionFourTheCSuite:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "22", false, true));
                break;
            case R.id.llTopicInnovatingTheCustomerShopping:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "23", false, true));
                break;
            case R.id.llTopicInnovatingTheTranspo:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "24", false, true));
                break;
            case R.id.llTopicETheCustomerFood:
                startActivity(ConferenceDetailsActivity.newIntent(mContext, "25", false, true));
                break;
        }
    }
}
