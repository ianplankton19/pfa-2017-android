package ph.coreproc.android.pfa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.coreproc.android.kitchen.models.APIError;
import com.coreproc.android.kitchen.utils.ErrorUtil;
import com.coreproc.android.kitchen.utils.KitchenRestClient;
import com.coreproc.android.kitchen.utils.KitchenUiUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;
import ph.coreproc.android.pfa.PFABase;
import ph.coreproc.android.pfa.R;
import ph.coreproc.android.pfa.adapters.ExhibitorsRecyclerViewAdapter;
import ph.coreproc.android.pfa.models.Exhibitor;
import ph.coreproc.android.pfa.models.User;
import ph.coreproc.android.pfa.rest.ExhibitorInterface;
import ph.coreproc.android.pfa.rest.responses.MetaDataWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExhibitorsListActivity extends BaseActivity implements ExhibitorsRecyclerViewAdapter.Callback {

    @Bind(R.id.exhibitorsListRecyclerView)
    RecyclerView exhibitorsListRecyclerView;

    @Bind(R.id.ivChoices)
    ImageView mIvChoices;

    @Bind(R.id.ivSearch)
    ImageView mIvSearch;

    @Bind(R.id.tvTitle)
    TextView mTvTitle;

    @Bind(R.id.tvNoDataToDisplay)
    TextView mTvNoDataToDisplay;

    SearchView searchView;
    SearchView.OnQueryTextListener queryTextListener;


    private int mPage = 1;
    private String mSector = "";
    private String mInvestment = "";

    private ExhibitorsRecyclerViewAdapter mExhibitorsRecyclerViewAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<Exhibitor> mExhibitorArrayList;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private boolean isCurrentlyLoadingNextPage = false;

    private MetaDataWrapper.Pagination mPagination;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ExhibitorsListActivity.class);
        return intent;
    }


    private final String[] investmentPackageArr = {"5M AND ABOVE", "3M – 5M", "1.5M – 3M"
            , "500K – 1.5M", "500K AND BELOW"};

   private final String[] investmentPackageArrValue = {"5 M and Above", "3 M - 5 M", "1.5 M - 3 M", "500 K - 1.5 M", "500 K - Below"};

    private final String[] exhibitorCategoriesArr = { "FOOD", "FOOD PARK",
            "INTERNATIONAL", "NON-FOOD", "REGIONAL", "SUPPLIERS AND ALLIED SERVICE", "PARTNERS"};



    private final String[] choicesArr = {"Investment Packages / Sectors", "Exhibitor's Categories"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        createSpinner();
        initialize();

    }

    private void initialize() {

        mExhibitorArrayList = new ArrayList<>(PFABase.getExhibitorLocals(mContext, "", "", 0));
        mLinearLayoutManager = new LinearLayoutManager(mContext.getApplicationContext(),
                LinearLayoutManager.VERTICAL, false);
        exhibitorsListRecyclerView.setLayoutManager(mLinearLayoutManager);
        mExhibitorsRecyclerViewAdapter = new ExhibitorsRecyclerViewAdapter(mContext, mExhibitorArrayList, ExhibitorsListActivity.this);

        exhibitorsListRecyclerView.setAdapter(mExhibitorsRecyclerViewAdapter);

//        exhibitorsListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                totalItemCount = mLinearLayoutManager.getItemCount();
//                lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
//                if (!isLoading && totalItemCount <= lastVisibleItem + 1) {
//                    isCurrentlyLoadingNextPage = true;
//                    isLoading = true;
////                    getExhibitor("" + mPage, mSector, mInvestment, false);
//
//                }
//            }
//        });


//        getExhibitor("", mSector, mInvestment, false);

    }

    private void getExhibitor(String page, String sector, String investment, final boolean changeData) {

        if (!page.equals("")) {
            if (!isCurrentlyLoadingNextPage) {
                return;
            }

            if (mPagination.getCurrentPage() == mPagination.getTotalPages()) {
                return;
            }

            mPage = mPage + 1;
        }


        final Dialog dialog = PFABase.loadDefaultProgressDialogCancellable(mContext, this);
        dialog.show();
        ExhibitorInterface exhibitorInterface = KitchenRestClient.create(mContext, ExhibitorInterface.class, true);
        Call<MetaDataWrapper<ArrayList<Exhibitor>>> exhibitorsResponseCall = exhibitorInterface.getExhibitors(getString(R.string.get_exhibitors_url), mPage + "", "", mSector, mInvestment);
        exhibitorsResponseCall.enqueue(new Callback<MetaDataWrapper<ArrayList<Exhibitor>>>() {
            @Override
            public void onResponse(Call<MetaDataWrapper<ArrayList<Exhibitor>>> call, Response<MetaDataWrapper<ArrayList<Exhibitor>>> response) {
                dialog.dismiss();
                isCurrentlyLoadingNextPage = false;
                isLoading = false;
                if (!response.isSuccessful()) {

                    if (response.code() == 401) {
                        User.logout(mContext);
                        return;
                    }

                    APIError error = ErrorUtil.parsingError(response);
                    KitchenUiUtils.showAlertDialog(mContext, "" + error.getError().getMessage());
                    return;
                }

                if (!changeData) {
                    mExhibitorsRecyclerViewAdapter.add(response.body().getData());
                } else {
                    mExhibitorsRecyclerViewAdapter.changeData(response.body().getData());
                }
                mPagination = response.body().getMeta().getPagination();

            }

            @Override
            public void onFailure(Call<MetaDataWrapper<ArrayList<Exhibitor>>> call, Throwable t) {
                isCurrentlyLoadingNextPage = false;
                isLoading = false;
                dialog.dismiss();
                KitchenUiUtils.showAlertDialog(mContext, getString(R.string.dialog_error_global) + "\n" + t.getMessage());
            }
        });

    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_exhibitors_list;
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater menuInflater = getMenuInflater();
//        menuInflater.inflate(R.menu.exhibitor, menu);
//        MenuItem searchItem = menu.findItem(R.id.action_search);
//        SearchManager searchManager = (SearchManager) ExhibitorsListActivity.this.getSystemService(Context.SEARCH_SERVICE);
//        searchView = (SearchView) searchItem.getActionView();
////        if (searchItem != null) {
////            KitchenUiUtils.showAlertDialog(mContext, "dumaan");
////        }
//        if (searchView != null) {
//            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//
//            queryTextListener = new SearchView.OnQueryTextListener() {
//                @Override
//                public boolean onQueryTextChange(String newText) {
//                    // Toast like print
////                    mSpeakersRecyclerViewAdapter.getFilter().filter(newText);
//                    return true;
//                }
//
//                @Override
//                public boolean onQueryTextSubmit(String query) {
//                    return false;
//                }
//            };
//            searchView.setOnQueryTextListener(queryTextListener);
//        }
////        getMenuInflater().inflate(R.menu.search_item, menu);
//        MenuItem spinnerItem = menu.findItem(R.id.spinner);
//        final Spinner spinner = (Spinner) MenuItemCompat.getActionView(spinnerItem);
//        spinner.setPopupBackgroundResource(R.color.white);
//        spinner.setMinimumWidth(0);
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.random, android.R.layout.simple_spinner_dropdown_item);
//
//        spinner.setAdapter(adapter);
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                spinner.setPrompt("");
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        return super.onCreateOptionsMenu(menu);
//    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
//                searchView.setOnQueryTextListener(queryTextListener);
                // Not implemented here
                return false;
            case android.R.id.home:
                finish();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void intentToDetails(int id) {
        startActivity(ExhibitorDetailsActivity.newIntent(mContext, id, false));
    }

    @Override
    public void removeNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.GONE);
    }

    @Override
    public void displayNoDataText() {
        mTvNoDataToDisplay.setVisibility(View.VISIBLE);
    }


    @OnClick({R.id.ivChoices, R.id.ivSearch})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivChoices:

                PFABase.dialogPicker(mContext, "Sort by", choicesArr, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                PFABase.dialogPicker(mContext, "Investment Packages / Sectors", investmentPackageArr, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (!mInvestment.equals(investmentPackageArr[which])) {
                                            mPage = 1;
                                            mSector = "";
                                            mInvestment = investmentPackageArrValue[which];
//                                            getExhibitor("", mSector, mInvestment, true);
                                            mExhibitorArrayList = new ArrayList<>(PFABase.getExhibitorLocals(mContext, mInvestment, mSector, 1));
                                            mExhibitorsRecyclerViewAdapter.changeData(mExhibitorArrayList);
                                        }
                                    }
                                }).show();
                                break;
                            case 1:
                                PFABase.dialogPicker(mContext, "Exhibitor's Category", exhibitorCategoriesArr, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (!mSector.equals(exhibitorCategoriesArr[which])) {
                                            mPage = 1;
                                            mSector = exhibitorCategoriesArr[which];
                                            mInvestment = "";
                                            mExhibitorArrayList = new ArrayList<>(PFABase.getExhibitorLocals(mContext, mInvestment, mSector, 2));
                                            mExhibitorsRecyclerViewAdapter.changeData(mExhibitorArrayList);
//                                            getExhibitor("", mSector, mInvestment, true);
                                        }

                                    }
                                }).show();
                                break;
                        }

                    }
                }).show();

                break;
            case R.id.ivSearch:

                startActivity(SearchExhibitorListActivity.newIntent(mContext));

                break;
        }
    }
}
