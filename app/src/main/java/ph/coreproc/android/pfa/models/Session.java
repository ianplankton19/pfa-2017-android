package ph.coreproc.android.pfa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IanBlanco on 5/3/2017.
 */

public class Session implements Serializable {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("session_name")
    @Expose
    public String sessionName;

    @SerializedName("start_time")
    @Expose
    public String startTime;

    @SerializedName("end_time")
    @Expose
    public String endTime;

    @SerializedName("topics")
    @Expose
    public TopicList topicList;

    @SerializedName("subsession")
    @Expose
    public SubSessionList subSession;

}
