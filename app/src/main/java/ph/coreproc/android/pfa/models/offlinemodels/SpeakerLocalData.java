package ph.coreproc.android.pfa.models.offlinemodels;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by IanBlanco on 4/10/2017.
 */

public class SpeakerLocalData implements Serializable {

    @SerializedName("data")
    @Expose
    public ArrayList<SpeakerLocal> speakers;



    private JsonObject data;

    public void setData(JsonObject data) {
        this.data = data;
    }


    public SpeakerLocalData(JsonArray jsonArray) {
        data = new JsonObject();
        data.add("data", jsonArray);
    }

    public JsonObject getData() {
        return data;
    }


}
