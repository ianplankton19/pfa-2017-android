package ph.coreproc.android.pfa.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by IanBlanco on 4/17/2017.
 */

public class CommentBody {

    public CommentBody(String comment) {
        this.comment = comment;
    }

    @SerializedName("comment")
    @Expose
    public String comment;
}
