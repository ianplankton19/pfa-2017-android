package ph.coreproc.android.pfa.models.offlinemodels;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import ph.coreproc.android.pfa.models.Speaker;
import ph.coreproc.android.pfa.models.Topics;

/**
 * Created by willm on 3/29/2017.
 */

public class SpeakerLocal implements Serializable {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String speakerName;

    @SerializedName("title")
    @Expose
    public String speakerTitle;

    @SerializedName("company")
    @Expose
    public String speakerCompany;

    @SerializedName("website")
    @Expose
    public String website;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("avatar")
    @Expose
    public String avatar;

    @SerializedName("role")
    @Expose
    public String role;

    @SerializedName("topics")
    @Expose
    public SpeakerTopicLocal speakerTopic;

    public JsonObject getData() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", this.id);
        jsonObject.addProperty("name", this.speakerName);
        jsonObject.addProperty("company", this.speakerCompany);
        jsonObject.addProperty("website", this.website);
        jsonObject.addProperty("description", this.description);
        jsonObject.addProperty("role", this.role);
        jsonObject.addProperty("avatar", this.avatar);
//        jsonObject.add("topics", speakerTopic);
        return jsonObject;
    }

    public JsonObject getData(JsonObject speakerTopic) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", this.id);
        jsonObject.addProperty("name", this.speakerName);
        jsonObject.addProperty("company", this.speakerCompany);
        jsonObject.addProperty("website", this.website);
        jsonObject.addProperty("description", this.description);
        jsonObject.addProperty("role", this.role);
        jsonObject.addProperty("avatar", this.avatar);
        jsonObject.add("topics", speakerTopic);

        return jsonObject;
    }

    public SpeakerLocal(Speaker speaker) {
        this.id = speaker.id;
        this.speakerName = speaker.speakerName;
        this.speakerCompany = speaker.speakerCompany;
        this.website = speaker.website;
        this.role = speaker.role;
        this.description = speaker.description;

        JsonArray jsonArray = new JsonArray();
        ArrayList<TopicLocal> topicLocals = new ArrayList<>();
        try {
            for (Topics topic : speaker.speakerTopic.speakerTopics) {
                TopicLocal topicLocal = new TopicLocal(topic);
                topicLocals.add(topicLocal);
                jsonArray.add(topicLocal.getJsonObject());
            }
        }catch (Exception e){

        }
        this.speakerTopic = new SpeakerTopicLocal(jsonArray);
        this.speakerTopic.speakerTopics = topicLocals;

    }



}