package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MainGenerator {
    public static void main(String[] args)  throws Exception {

        //place where db folder will be created inside the project folder
        Schema schema = new Schema(1,"database.db");

        //Entity i.e. Class to be stored in the database // ie table LOG
        Entity topic_entity= schema.addEntity("conference_local_tbl");

        topic_entity.addIdProperty(); //It is the primary key for uniquely identifying a row
        topic_entity.addStringProperty("save_string").notNull();  //Not null is SQL constrain




        Entity seminar_entity = schema.addEntity("seminar_local_tbl");

        seminar_entity.addIdProperty();
        seminar_entity.addStringProperty("save_string").notNull();



        Entity speaker_entity = schema.addEntity("speaker_local_tbl");

        speaker_entity.addIdProperty();
        speaker_entity.addStringProperty("save_string").notNull();



        Entity exhibitor_entity = schema.addEntity("exhibitor_local_tbl");

        exhibitor_entity.addIdProperty();
        exhibitor_entity.addStringProperty("save_string").notNull();



        Entity notes_entity = schema.addEntity("notes_local_tbl");

        notes_entity.addIdProperty();
        notes_entity.addStringProperty("save_string").notNull();


        Entity exhibitor_scan_entity = schema.addEntity("exhibitor_scan_local_tbl");

        exhibitor_scan_entity.addIdProperty();
        exhibitor_scan_entity.addStringProperty("save_string").notNull();


        Entity user_scan_entity = schema.addEntity("user_scan_local_tbl");

        user_scan_entity.addIdProperty();
        user_scan_entity.addStringProperty("save_string").notNull();



        //  ./app/src/main/java/   ----   com/codekrypt/greendao/db is the full path
        new DaoGenerator().generateAll(schema, "./app/src/main/java");




    }
}
